<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoToItemsAndUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('items', function(Blueprint $table)
		{
		
			$table->string('photo');					//UB-27 Item can have a Photo [2015-07-11]
			
		});
		
		
		Schema::table('users', function(Blueprint $table)
		{
		
			$table->string('photo');					//UB-27 Item can have a Photo [2015-07-11]
				
		});
	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('items', function(Blueprint $table)
		{
			$table->dropColumn('photo');			//UB-27 Item can have a Photo [2015-07-11]
		});
		
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('photo');			//UB-27 Item can have a Photo [2015-07-11]
		});
	}

}
