<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactPreferencesToUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->boolean('contact_email')->default(TRUE);
			$table->boolean('contact_phone')->default(TRUE);
			$table->boolean('contact_sms')->default(TRUE);
				
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('contact_email');
			$table->dropColumn('contact_phone');
			$table->dropColumn('contact_sms');
		});
	}

}
