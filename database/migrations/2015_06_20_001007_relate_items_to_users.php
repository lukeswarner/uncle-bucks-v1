<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelateItemsToUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('items', function(Blueprint $table)
		{
			//
			$table->integer('user_id')->unsigned();
			
			/*
			$table->foreign('user_id', 'items_user_id_foreign') 	//UB-19 - Foreign Key constraint breaks Item creation, since user_id cannot be null
				->references('id')
				->on('users')
				->onDelete('cascade');
			*/
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('items', function(Blueprint $table)
		{
			//
			//$table->dropForeign('items_user_id_foreign');		//UB-19 - Foreign Key constraint breaks Item creation, since user_id cannot be null
			$table->dropColumn('user_id');
		});
	}

}
