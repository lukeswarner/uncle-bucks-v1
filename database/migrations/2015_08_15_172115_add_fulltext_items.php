<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFulltextItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		DB::statement('ALTER TABLE items ADD FULLTEXT fulltext_title_description(title, description)');
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::table('items', function($table) {
	            $table->dropIndex('fulltext_title_description');
	        });
	}

}
