<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelateLocationToItemsUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('locations', function(Blueprint $table)
		{
			//
			/* allows Polymorphic Relationships between a Location and either a User or an Item */ 
			$table->integer('locatable_id')->unsigned();				//UB-25 Item can have a Location [2015-07-02]
			$table->string('locatable_type');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('locations', function(Blueprint $table)
		{
			$table->dropColumn('locatable_id');
			$table->dropColumn('locatable_type');
		});
	}

}
