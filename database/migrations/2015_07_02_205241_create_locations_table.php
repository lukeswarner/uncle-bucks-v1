<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('address');
			$table->string('street');
			$table->string('city');
			$table->string('state');
			$table->string('postal_code');
			$table->string('region');
			$table->float('lat', 8, 4);	// 4 decimal places of precision is 11m of accuracy- more than enough for our purposes (ie, privacy)
			$table->float('lng', 8, 4);	// see: http://gis.stackexchange.com/questions/8650/how-to-measure-the-accuracy-of-latitude-and-longitude
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
