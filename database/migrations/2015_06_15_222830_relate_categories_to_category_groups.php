<?php
/**
 *
 * @author Luke
 * @issues
 * UB-16 Categories Model
 *
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelateCategoriesToCategoryGroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function(Blueprint $table)
		{
			
			//add foreign key denoting a Category belongsTo a CategoryGroup
			$table->integer('category_group_id')->unsigned();
			
			//foreign key cascade- when a CategoryGroup is deleted, delete all of its' Categories
			$table->foreign('category_group_id')
				->references('id')
				->on('category_groups')
				->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories', function(Blueprint $table)
		{
			//
		});
	}

}
