<?php
/**
 * @issues
 * UB-20 User Migration (sub-table of UB-1 User CRUD) [2015-06-13]
 *
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->string('username')->unique();  	//UB-20 : replace "name" with "username"
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('phone');				//UB-20 : add "phone" field
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
