<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder {
	
	protected $bend_lat= 44.0581753;
	protected $bend_lng= -121.3166054;
	protected $sq= 0.01;
	protected $sq2= 0.02;
	
	
	public function randomDelta(){
		$a= mt_rand(0,10);
		if($a <= 8 ){
	
			$delta= mt_rand(0,200) / 10000;
		}
		else {
			$delta= mt_rand(200,800) / 10000;
		}
	
		if( mt_rand(0,1) == 1){
			$delta *= -1;
		}
	
		return $delta;
	}
	
	
	
	public function randomLocation($user){
	
		$new_lat= $this->bend_lat + $this->randomDelta();
		$new_lng= $this->bend_lng + $this->randomDelta();
	
		
		DB::table('locations')->insert([
			'address' => 'random',
			'lat' => $new_lat,
			'lng' => $new_lng,
			'locatable_id' => $user->id,
			'locatable_type' => 'App\User'
			
			]);
		
	}
	
    public function run()
    {
    	
    	DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
    	
    	
    	\App\Location::truncate();
    	
    	\App\User::truncate();
    	
        DB::table('roles')->truncate();
    	DB::table('role_user')->truncate();
    	
    	
    	
    	/*
    	 * 
    	 * CREATE ROLES
         * */
       	$admin_role= DB::table('roles')->insert([
	       	'name' => 'admin',
	       	'display_name' => 'Admin',
	       	'description' => 'An administrative user manages the content for the site and can edit any item.',
	       	]);
       	
       	$individual_role= DB::table('roles')->insert([
	       	'name' => 'individual',
	       	'display_name' => 'Individual',
	       	'description' => 'An individual is allowed to create and post their own items for rent, and edit only their own items',
	       	]);
       	//*/
    	
    	/*  CREATE DEFAULT USERS */
    	$admin_role= App\Role::where('name', '=', 'admin')->first();
    	$individual_role= App\Role::where('name', '=', 'individual')->first();
       	

       	$admin_id= DB::table('users')->insert([
	       	'email' => 'admin@unclebucks.co',
	       	'password' => bcrypt('secret'),
       	]);
       	$admin= App\User::where('email', '=', 'admin@unclebucks.co')->first();
       	$admin->attachRole($admin_role);
       	
       	
       	$luke_id= DB::table('users')->insert([
	       	'email' => 'lukeswarner@gmail.com',
	       	'password' => bcrypt('secret'),
       		'phone' => '619-753-8583'
       	]);
       	
       	$luke= App\User::where('email', '=', 'lukeswarner@gmail.com')->first();
       	$luke->attachRole($individual_role);
       	//*/
    	
       	 
    	$individual_role= App\Role::where('name', '=', 'individual')->first();
    	
       	for( $i=0; $i<50; $i++ ){
       		
       		$email= str_random( 8 ).'@ashaman.net';
       		
       		try {
	       		$u= DB::table('users')->insert([
		       		'email' => $email,
		       		'password' => bcrypt('secret'),
	       			'phone' => '123-456-7890',
	       			'contact_email' => rand(0,1),
	       			'contact_phone' => rand(0,1),
	       			'contact_sms' => rand(0,1),
	       		]);
	       		
	       		$u= App\User::where('email', '=',$email )->first();
	       		$u->attachRole($individual_role);
	       		
	       		
	       		$loc= \App\Location::create( $this->randomLocation($u) );
	       		
	       		$u->location()->save($loc);
       		}
       		catch (Exception $e){
       			
       		}
       		
       		
       		
       		
       		
       	}
       	
       	DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
       	
       	
    }

}