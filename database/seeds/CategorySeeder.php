<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategorySeeder extends Seeder {

	
    public function run()
    {
        DB::table('categories')->truncate();
        
		
    	$groups= App\CategoryGroup::all();
    	/*
    	 * CategoryGroups
    	 * 
    	 * 		Recreation
    	 * 		Home
    	 * 		Tech
    	 * 		Garage
    	 * 		Miscellaneous
    	 * 
    	 */
    	
	/*
    	DB::table('categories')->insert([
    		'id' => '0',
	    	'name' => 'All For Rent',
	    	'slug' => 'all',
	    	'description' => "Rather than filtering to only items that match a single category, show all available items.",
	    	'category_group_id' => $groups->where('name', 'All / None')->first()->id
	    	]);
    	*/

    	//RECREATION
    	DB::table('categories')->insert([
    	'name' => 'Bicycles',
    	'slug' => 'bike',
    	'description' => DatabaseSeeder::ipsum(),
    	'category_group_id' => $groups->where('name', 'Recreation')->first()->id
    	]);
    	

    	DB::table('categories')->insert([
    	'name' => 'Boats',
    	'slug' => 'boat',
    	'description' => DatabaseSeeder::ipsum(),
    	'category_group_id' => $groups->where('name', 'Recreation')->first()->id
    	]);
    	
    	
        DB::table('categories')->insert([
	        'name' => 'Motorsports',
	        'slug' => 'motor',
	        'description' => DatabaseSeeder::ipsum(),
	        'category_group_id' => $groups->where('name', 'Recreation')->first()->id
	        ]);
        
        DB::table('categories')->insert([
        'name' => 'RVs & Camping',
        'slug' => 'rv',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Recreation')->first()->id
        ]);
        
        
        DB::table('categories')->insert([
        'name' => 'Sporting Goods',
        'slug' => 'sports',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Recreation')->first()->id
        ]);
        
        
        
        //HOME
        DB::table('categories')->insert([
	        'name' => 'Kitchen',
	        'slug' => 'kit',
	        'description' => DatabaseSeeder::ipsum(),
	        'category_group_id' => $groups->where('name', 'Home')->first()->id
	        ]);
	        
        
        DB::table('categories')->insert([
        'name' => 'Baby & Kids',
        'slug' => 'baby',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Home')->first()->id
        ]);
        
        
        
        //TECH
        DB::table('categories')->insert([
        'name' => 'Musical Instruments',
        'slug' => 'inst',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Tech')->first()->id
        ]);
        
        DB::table('categories')->insert([
        'name' => 'Photo & Video',
        'slug' => 'photo',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Tech')->first()->id
        ]);
        
        DB::table('categories')->insert([
        'name' => 'Electronics',
        'slug' => 'el',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Tech')->first()->id
        ]);
        
        
        
        //GARAGE
        DB::table('categories')->insert([
        'name' => 'Cars & Trucks',
        'slug' => 'cars',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Garage')->first()->id
        ]);
        
        DB::table('categories')->insert([
	        'name' => 'Tools',
	        'slug' => 'tools',
	        'description' => DatabaseSeeder::ipsum(),
	        'category_group_id' => $groups->where('name', 'Garage')->first()->id
	        ]);
        
        DB::table('categories')->insert([
        'name' => 'Farm & Garden',
        'slug' => 'garden',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Garage')->first()->id
        ]);
        
        DB::table('categories')->insert([
	        'name' => 'Trailers',
	        'slug' => 'trailer',
	        'description' => DatabaseSeeder::ipsum(),
	        'category_group_id' => $groups->where('name', 'Garage')->first()->id
	        ]);
        
        
        
        //MISC
        DB::table('categories')->insert([
        'name' => 'General',
        'slug' => 'gen',
        'description' => DatabaseSeeder::ipsum(),
        'category_group_id' => $groups->where('name', 'Miscellaneous')->first()->id
        ]);
 
       
         
    }

}
