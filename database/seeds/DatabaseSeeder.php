<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {

	
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CategoryGroupSeeder');
		$this->call('CategorySeeder');
		$this->call('UserSeeder');
		$this->call('ItemSeeder');
		//$this->call('LocationSeeder');
		
		
	}
	
	
	/**
	 * UB-3 User CRUD [2015-06-24]
	 *
	 * return three words of hipster ipsum text    --     http://hipsum.co
	 * @return string
	 */
	public static function lorem(){
	
		$texts= [
		"Austin single-origin coffee",
		"banjo stumptown distillery",
		"ugh bitters trust fund pop-up",
		"YOLO crucifix tote bag cray",
		"fashion axe chillwave",
		"Pinterest lomo",
		"gluten-free vinyl",
		"Bitters trust fund",
		"kitsch food truck",
		"irony farm-to-table cardigan",
		"Echo Park locavore semiotics",
		"chambray tote bag",
		"Bespoke roof party",
		"chillwave bitters",
		"Etsy sriracha fap wayfarers",
		"brunch pop-up",
		"skateboard Street art meditation",
		"Blue Bottle, Godard",
		"you probably haven't heard of them",
		"literally sustainable",
		"messenger bag",
		"Photo booth",
		"authentic 8-bit",
		"normcore trust fund",
		"Cray irony freegan",
		"actually locavore",
		"Vice PBR&B",
		"biodiesel hella synth",
		"occupy DIY Truffaut Carles",
		"dreamcatcher Kickstarter",
		"kitsch cornhole",
		"cray asymmetrical",
		"tofu 8-bit photo booth",
		"leggings vinyl",
		"fashion axe kitsch",
		"flexitarian plaid roof party.",
		"8-bit street art",
		"distillery pour-over",
		"paleo try-hard semiotics",
		"tote bag sriracha",
		"plaid bitters",
		"tilde polaroid",
		"Lomo irony cronut",
		"occupy letterpress",
		"gastropub cornhole ",
		"taxidermy McSweeney's.",
		"Next level quinoa ",
		"whatever XOXO",
		"hoodie authentic Brooklyn.",
		"Direct trade normcore asymmetrical",
		"cornhole cold-pressed four ",
		"loko disrupt Wes Anderson ",
		"90's wolf tofu",
		"XOXO you probably",
		"haven't heard of them",
		"mixtape Pitchfork.",
		"Aesthetic bicycle rights",
		"keffiyeh Intelligentsia",
		"Wes Anderson four dollar toast",
		"organic messenger bag",
		"keytar kogi wolf chia.",
		"Lomo ethical crucifix",
		"gluten-free messenger bag",
		"four dollar toast raw denim.",
		"Banh mi cliche wayfarers",
		"squid Neutra semiotics",
		"Echo Park PBR fixie typewriter",
		"Portland deep v normcore ugh. ",
		"Synth Portland cardigan ",
		"disrupt irony. ",
		"Pork belly ",
		"selvage PBR listicle irony.",
		"Chambray lomo artisan",
		"bicycle rights",
		"narwhal",
		"four dollar toast",
		"Neutra, wolf butcher",
		"cray Pitchfork craft beer",
		"gastropub selvage.",
		"Kogi health goth ",
		"Kickstarter occupy",
		"single-origin coffee.",
		"Biodiesel twee ",
		"plaid meditation skateboard ",
		"polaroid, ",
		"gluten-free fingerstache ",
		"butcher pork belly kogi.",
		"Quinoa chillwave chambray",
		"raw denim Austin yr ",
		"High Life."
				];
		
		
		$num= count($texts)- 1;
		
			
		return $texts[rand(0, $num)];
	}
	
	
	/**
	 * UB-12 Category CRUD [2015-06-16]
	 * 
	 * return a string of hipster ipsum text    --     http://hipsum.co
	 * @return string 
	 */
	public static function ipsum(){
		$texts= [
				"Austin single-origin coffee banjo stumptown distillery, ugh bitters trust fund pop-up. YOLO crucifix tote bag cray, fashion axe chillwave Pinterest lomo gluten-free vinyl.",
				"Bitters trust fund kitsch food truck, irony farm-to-table cardigan Echo Park locavore semiotics chambray tote bag. Bespoke roof party chillwave bitters.",
				"Etsy sriracha fap wayfarers brunch pop-up skateboardStreet art meditation Blue Bottle, Godard you probably haven't heard of them literally sustainable messenger bag.",
				"Photo booth authentic 8-bit actually normcore trust fund. Cray irony freegan, actually locavore Vice PBR&B biodiesel hella synth occupy DIY Truffaut Carles.",
				"Actually dreamcatcher Kickstarter kitsch cornhole cray asymmetrical, tofu 8-bit photo booth leggings vinyl. Ugh fashion axe kitsch flexitarian plaid roof party.",
				"8-bit street art distillery pour-over, paleo try-hard semiotics tote bag sriracha plaid bitters tilde polaroid Austin. Lomo irony cronut, dreamcatcher occupy letterpress 8-bit gastropub cornhole Carles Etsy tilde taxidermy McSweeney's.",
				"Next level quinoa whatever XOXO, hoodie authentic Brooklyn.",
				"Direct trade normcore asymmetrical, cornhole cold-pressed four loko disrupt Wes Anderson 90's wolf tofu XOXO you probably haven't heard of them mixtape Pitchfork.",
				"Aesthetic bicycle rights Carles, keffiyeh Intelligentsia Wes Anderson four dollar toast organic messenger bag keytar kogi wolf chia. Lomo ethical crucifix, gluten-free messenger bag four dollar toast raw denim.",
				"Banh mi cliche wayfarers, squid Neutra semiotics Echo Park PBR fixie typewriter Portland deep v normcore ugh. Synth Portland cardigan disrupt irony. Pork belly selvage PBR listicle irony.",
				"Chambray lomo artisan bicycle rights narwhal four dollar toast Neutra, wolf butcher cray Pitchfork craft beer gastropub selvage. Kogi health goth Kickstarter occupy single-origin coffee.",
				"Biodiesel twee plaid meditation skateboard polaroid, gluten-free fingerstache butcher pork belly kogi. Quinoa chillwave chambray, raw denim Austin yr High Life.",
				"McSweeney's fap fanny pack, butcher skateboard Carles kale chips cliche blog meggings forage kitsch sustainable pickled typewriter.",
				"Distillery Williamsburg Bushwick mumblecore slow-carb. McSweeney's fanny pack twee, sartorial bespoke bicycle rights drinking vinegar wayfarers quinoa. Dreamcatcher 90's irony food truck meh, vinyl pour-over 8-bit.",
				"Food truck fap put a bird on it you probably haven't heard of them, pork belly hella crucifix readymade blog cronut vegan.",
				"IPhone leggings four dollar toast mlkshk, try-hard church-key flexitarian. Cred fashion axe Portland semiotics Blue Bottle."
				];
		
		
		$num= count($texts)- 1;
		
			
		return $texts[rand(0, $num)];
	}

}
