<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Item;


class ItemSeeder extends Seeder {

	
    public function run()
    {
       //\App\Item::truncate();
   
        $users= \App\User::all();
        $categories= \App\Category::all();
        
       //$storage_path= "C:\Users\Luke\Pictures\ub_items";

        //$a=0;
        //$e=0;
        
        for( $i=0; $i<500; $i++ ){
        	
        	$price= rand(1, 40);


        	
			if( $i % 3 == 0){
        		$d= rand(0,30);
        		$expiration = Carbon::today()->addDays($d);
        		$auto_renews= rand(0,1);
        		//$a++;
        	
			}
			else {
				$m= rand(0,18);
				$d= rand(0,30);
				$expiration = Carbon::today()->subMonths($m)->subDays($d);
        		$auto_renews=0;
				//$e++;
			}
			
			$created= clone $expiration;
			$created->subMonth( rand(1,6) );


			
			$category= $categories->random(1);
			$photos= collect(Storage::files('ub_items/'.$category->slug));
			
			//if(env('APP_ENV') == "production"){
				$photo= str_ireplace("ub_items/".$category->slug."/", "uploads/item/", $photos->random(1) );	
				//echo $photo."\r\n";	
			//}
			//else {
			//	$photo= str_ireplace("ub_items/".$category->slug."\\", "uploads/item/", $photos->random(1) );
			//}	
			
			$rental_period= rand(0,1) ? "day" : "hour";

			 
        	$item_data = [
		        'title' => DatabaseSeeder::lorem(),
		        'price' => $price,
		        'description' => DatabaseSeeder::ipsum(),
		        'category_id' => $category->id,
		        'user_id' => $users->random(1)->id,
        		'photo' => $photo,
		        'status' => 0,
		        'rental_period' => $rental_period,
		        'auto_renew' => $auto_renews,
		        'expires_at' => $expiration,
		        'created_at' => $created,
		        'updated_at' => $created,
	       ];
        	
        	$item= Item::create($item_data);
        	

        }
        
        

       	 
    }

}