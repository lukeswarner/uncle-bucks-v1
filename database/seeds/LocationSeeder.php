<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Location;
use App\User;


class LocationSeeder extends Seeder {

	protected $bend_lat= 44.0612384;
	protected $bend_lng= -121.384684;
	protected $sq= 0.01;
	protected $sq2= 0.02;
	
	
	public function randomDelta(){
		$a= mt_rand(0,10);
		if($a <= 7 ){
				
			$delta= mt_rand(100,1000) / 10000;
			
		}
		else {
			$delta= mt_rand(1000,3500) / 10000;
		}
		
		if( mt_rand(0,1) == 1){
			$delta *= -1;
		}
		
		return $delta;
	}
	
	
	
	public function randomLocation($user){
		
		$new_lat= $this->bend_lat + $this->randomDelta();
		$new_lng= $this->bend_lng + $this->randomDelta();
		
		echo $new_lat.','.$new_lng."\r\n
				";
		return [
			'address'=>'random',
			'lat' => $new_lat,
			'lng' => $new_lng,
		];
		
	}
	
    public function run()
    {
       \App\Location::truncate();
   
        $users= User::all();
        
       
        
        foreach( $users as $u ){
        	
        	$loc= Item::create( $this->randomLocation( $u ) );
        	
        	$u->location()->save($loc);


        }

       	 
    }

}