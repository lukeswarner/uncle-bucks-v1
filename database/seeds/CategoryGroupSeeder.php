<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoryGroupSeeder extends Seeder {

	
    public function run()
    {
        DB::table('category_groups')->delete();
        
       /*
    	DB::table('category_groups')->insert([
	    	'id' => 0,
	    	'name' => 'All / None',
	    	'slug' => 'all',
	    	'description' => "",
    	]);
    	*/
        
        DB::table('category_groups')->insert([
	        'name' => 'Recreation',
	        'slug' => 'rec',
	        'description' => DatabaseSeeder::ipsum(),
	        ]);
        
        DB::table('category_groups')->insert([
	        'name' => 'Home',
	        'slug' => 'home',
	        'description' => DatabaseSeeder::ipsum(),
        	]);
        
        DB::table('category_groups')->insert([
	        'name' => 'Tech',
	        'slug' => 'tech',
	        'description' => DatabaseSeeder::ipsum(),
        	]);
        
        DB::table('category_groups')->insert([
	        'name' => 'Garage',
	        'slug' => 'garade',
	        'description' => DatabaseSeeder::ipsum(),
        	]);
        
        DB::table('category_groups')->insert([
        'name' => 'Miscellaneous',
        'slug' => 'misc',
        'description' => DatabaseSeeder::ipsum(),
        ]);
         
    }

}