		<div clas="row">
			<h2>How do you want to pay?</h2>
			
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-ub">
				    <div class="panel-heading" role="tab" id="headingOne">
				      	<h3 class="panel-title">
					        
					          Pay with a new card
					        
				      	</h3>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				      	<div class="panel-body">
				        	
				      			
							{!! Form::open(['url'=>'payment/'.$type.'/'.$plan, 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'payment-form']) !!} 
				
								<div class="form-group">
									<div class="col-md-8 col-md-offset-3">
										
										<div id="error-wrapper" class="alert alert-danger" style="display:none;">
											<strong>Whoops!</strong> There were some problems with your credit card information.<br><br>
											<span id="payment-errors"></span>
										</div>
									</div>
								</div>
									
								<input type="hidden" name="source" value="captured"/>		
								<input type="hidden" name="amount" value="{{ $amount }}"/>
								<input type="hidden" name="title" value="{{ $title }}"/>
								
								
								@include('payment.capture', ['submitButtonText' => 'Complete Payment', 'processingText'=>"Processing Payment...", "save_option"=>TRUE]) 
								
							{!! Form::close() !!}
							
							
				      	</div>
			    	</div>
			  	</div>
			</div>