@extends('app')


@section('css')
	<!--  toggle switch css -->
	<link href="{{ asset('/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
@endsection


@section('scripts')
	<!--  <script type="text/javascript" src="https://js.stripe.com/v2/"></script> -->
	<script type="text/javascript" src="https://js.stripe.com/v2/stripe-debug.js"></script>
	<script type="text/javascript" src="{{ asset( 'js/stripe-capture-card.js' ) }}<?= "?d=".date('Ymd');?>"></script>
	@include('scripts.bootstrap-toggle')
@endsection

<?php 
/**
 *
 * @issues
 * UB-130 User Can Purchase 
 */

?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5 col-md-offset-3">	
			<h2>{{$title}}</h2>
			<h3>Amount: {{$amount_duration}}</h3>
			<p>
				{!! $description !!}
			</p>
			@if( $type == "charge" )
			<h3>The item you will post to Uncle Bucks:</h3>
			<div class="row">
				<div class="col-md-4">	
					<div id="item-photo">	 
						<img src="{{ url($item->thumbnail) }}" class="item-photo"/>
					</div>
				</div>
				<div class="col-md-8">
					<h2 class="item-heading">{{$item->title}}</h2>
					<h3 style="margin-top: 0px;">{{$item->category->name}}</h3>
					<h3 style="margin-top: 0px;">${{$item->price}} / {{$item->rental_period}}</h3>
					
				</div>
			</div>
			@endif
		</div>
	</div>
	
	
	<div class= "row">
		<div class="col-md-5 col-md-offset-3">	
			@if( !empty($saved_card) )
				@include('payment.saved-or-new-card')
			@else
				@include('payment.new-card')
			@endif
		</div>
	</div>
	
	
</div>
@endsection
		  