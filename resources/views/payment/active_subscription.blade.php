@extends('app')


<?php 
/**
 *
 * @issues
 * UB-130 User Can Purchase 
 */

?>

@section('content')
<div class="container-fluid">
	<div class="row">
		
		{!! Form::open(['url'=>'payment/subscription/'.$user->id.'/cancel', 'method' => 'post', 'class'=>'form-horizontal']) !!}
		
			<div class="form-group">
				<div class="col-md-6 col-md-offset-3">
					<h2>Your Current Subscription</h2>
				</div>
			</div>			
			

			<div class="form-group">
				<label class="col-md-4 control-label">Plan</label>
				<div class="col-md-3"><h3 class="subscription_details">{{ $plan->name }} {{$plan->interval}}ly</h3></div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Amount</label>
				<div class="col-md-3"><h3 class="subscription_details">${{$amount}}/{{$plan->interval}}</h3></div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Started</label>
				<div class="col-md-3"><h3 class="subscription_details">{{$subscription->start->format('F d, Y')}}</h3></div>
			</div>
	
			
			<div class="form-group">
				<label class="col-md-4 control-label">Renews</label>
				<div class="col-md-3"><h3 class="subscription_details">{{$subscription->current_period_end->format('F d, Y')}}</h3></div>
			</div>
			
			
			<div class="form-group">
				<div class="col-md-4 col-md-offset-3">
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#myModal">
					  Cancel Subscription
					</button>
					
			
				</div>
			</div>
			
		
		
	</div>
		    
	
			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Are you sure you want to cancel your subscription?</h4>
			      </div>
			      
			      
			      <div class ="modal-body">
			      	<p>Cancelling your subscription will prevent your card from being charged again. Your subscription will stay active through {{$subscription->current_period_end->format('F d, Y')}}. Any items which expire after this date will be removed from Uncle Bucks, but you can always renew your items individually or reactivate your monthly subscription.</p>
			      </div>
			      
			      
			      <div class="modal-footer">
			      	
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Never Mind</button>
			        	
			         	{!! Form::submit('Cancel Subscription', ['class' => 'btn btn-danger']) !!}
			        
			      </div>
			    </div>
			  </div>
			</div>
	 {!! Form::close() !!}
</div>
@endsection
		  