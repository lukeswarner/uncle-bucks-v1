		<div clas="row">
			<h2>How do you want to pay?</h2>
			
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-ub">
				    <div class="panel-heading" role="tab" id="headingOne">
				      	<h3 class="panel-title">
					        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          Pay with my saved card
					        </a>
				      	</h3>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				      	<div class="panel-body">
				        	
				      			
							{!! Form::open(['url'=>'payment/'.$type.'/'.$plan, 'method' => 'post', 'class'=>'form-horizontal']) !!} 
							
								
								<input type="hidden" name="source" value="saved"/>
								<input type="hidden" name="amount" value="{{ $amount }}"/>
								<input type="hidden" name="title" value="{{ $title }}"/>
								
								
								@include('payment.saved-card', ['submitButtonText' => 'Complete Payment']) 
							{!! Form::close() !!}
							
							
				      	</div>
			    	</div>
			  	</div>
			  	
			  	
			  	
			  <div class="panel panel-ub">
    				<div class="panel-heading" role="tab" id="headingTwo">
      					<h3 class="panel-title">
        					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          					Pay with a new card
        					</a>
				      </h3>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				      	<div class="panel-body">
				      	
				        	{!! Form::open(['url'=>'payment/'.$type.'/'.$plan, 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'payment-form']) !!} 
				
								<div class="form-group">
									<div class="col-md-8 col-md-offset-3">
										
										<div id="error-wrapper" class="alert alert-danger" style="display:none;">
											<strong>Whoops!</strong> There were some problems with your credit card information.<br><br>
											<span id="payment-errors"></span>
										</div>
									</div>
								</div>
									
								<input type="hidden" name="source" value="captured"/>		
								<input type="hidden" name="amount" value="{{ $amount }}"/>
								<input type="hidden" name="title" value="{{ $title }}"/>
								
								
								@include('payment.capture', ['submitButtonText' => 'Complete Payment', 'processingText'=>"Processing Payment...", "save_option"=>TRUE]) 
								
							{!! Form::close() !!}
							
				      	</div>
			    	</div>
				</div>
			</div>
			  
			
		</div>


<?php 

/*
	<div class="row">
		<div class="col-md-4 col-md-offset-3 choose-payment">
			@if( !empty($saved_card) )
			<h2>How do you want to pay?</h2>
			@else
			<h2>Enter your payment information</h2>
			@endif
		</div>
	</div>
	
	<div clas="row">
		@if( !empty($saved_card) )
		<div class="col-md-4 col-md-offset-1"  style="border-right: 2px solid;">	
			{!! Form::open(['url'=>'payment/'.$type.'/'.$plan, 'method' => 'post', 'class'=>'form-horizontal']) !!} 
			
				<div class="form-group">
					<div class="col-md-6">
						<h3>Use my saved card</h3>
					</div>
				</div>
				
				<input type="hidden" name="source" value="saved"/>
				<input type="hidden" name="amount" value="{{ $amount }}"/>
				<input type="hidden" name="title" value="{{ $title }}"/>
				
				
				@include('payment.saved-card', ['submitButtonText' => 'Pay With Saved Card']) 
				<br />
				<br />
				<br />
				<br />
				<br />
			{!! Form::close() !!}
		</div>
		@else
		<div class="col-md-3"> &nbsp; </div>
		@endif
		
		
		
		<div class="col-md-5">
		
			{!! Form::open(['url'=>'payment/'.$type.'/'.$plan, 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'payment-form']) !!} 
				
				<div class="form-group">
					<div class="col-md-8 col-md-offset-3">
						@if( !empty($saved_card) )
							<h3>Use a new card</h3>
						@endif
						
						<div id="error-wrapper" class="alert alert-danger" style="display:none;">
							<strong>Whoops!</strong> There were some problems with your credit card information.<br><br>
							<span id="payment-errors"></span>
						</div>
					</div>
				</div>
					
				<input type="hidden" name="source" value="captured"/>		
				<input type="hidden" name="amount" value="{{ $amount }}"/>
				<input type="hidden" name="title" value="{{ $title }}"/>
				
				
				@include('payment.capture', ['submitButtonText' => 'Pay with New Card']) 
				
			{!! Form::close() !!}
		</div>
		
	</div>