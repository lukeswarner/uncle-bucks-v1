			<div class="form-group">
				<div class="col-md-3"><b>{{$saved_card->brand}}</b></div>
				<div class="col-md-6">XXXX-XXXX-XXXX-{{$saved_card->last4}}</div>
				<div class="col-md-3" style="text-align:right;">Exp: {{ str_pad($saved_card->exp_month, 2, "0", STR_PAD_LEFT ) }} / {{$saved_card->exp_year}}</div>
			</div>
			
			
		    <div class="form-group">
				<div class="col-md-12">
					<button type="submit" class="btn btn-ub btn-block payment-btn" data-submit-text="Processing Payment..."  data-default-text="{{ $submitButtonText }}">{{ $submitButtonText }}</button>
	
				</div>
			</div>	
				