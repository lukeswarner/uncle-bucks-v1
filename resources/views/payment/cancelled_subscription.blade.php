@extends('app')

<?php 
/**
 *
 * @issues
 * UB-130 User Can Purchase 
 */

?>

@section('content')
<div class="container-fluid">
	<div class="row">
		
		{!! Form::open(['url'=>'payment/subscription/'.$user->id.'/reactivate', 'method' => 'post', 'class'=>'form-horizontal']) !!} 

	
			<div class="form-group">
				<div class="col-md-6 col-md-offset-3">
					<h2>Your Cancelled Subscription</h2>
				</div>
			</div>			
			
			<div class="form-group">
				<div class="col-md-4 col-md-offset-3">
					<p>
					Reactivate your subscription to make sure that your items never expire from Uncle Bucks. 
					Your card will not be charged until the current subscription period ends on {{$subscription->current_period_end->format('F d, Y')}}. 
					</p>
				</div>
			</div>
				

			<div class="form-group">
				<label class="col-md-4 control-label">Plan</label>
				<div class="col-md-3"><h3 class="subscription_details">{{ $plan->name }} {{$plan->interval}}ly</h3></div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Amount</label>
				<div class="col-md-3"><h3 class="subscription_details">${{$amount}}/{{$plan->interval}}</h3></div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Started</label>
				<div class="col-md-3"><h3 class="subscription_details">{{$subscription->start->format('F d, Y')}}</h3></div>
			</div>
	
			
			<div class="form-group">
				<label class="col-md-4 control-label text-danger">Cancelled</label>
				<div class="col-md-3"><h3 class="subscription_details text-danger">{{$subscription->canceled_at->format('F d, Y')}}</h3></div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label text-danger">Ends</label>
				<div class="col-md-3"><h3 class="subscription_details text-danger">{{$subscription->current_period_end->format('F d, Y')}}</h3></div>
			</div>
			
			
				
			<div class="form-group">
				
				<div class="col-md-4 col-md-offset-3">
				
					{!! Form::submit('Reactivate Subscription', ['class' => 'btn btn-ub btn-block']) !!}
				</div>
			</div>
		
		
			
			
		{!! Form::close() !!}
	</div>
		    
	
	
</div>
@endsection
		  