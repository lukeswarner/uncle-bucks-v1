			<div class="form-group">
					<label class="col-md-3 control-label">Card Number</label>
					<div class="col-md-8">
						<input type="text" size="20" class="form-control" data-stripe="number"/>
					</div>
				</div>
				
			
			    <div class="form-group">
					<label class="col-md-3 control-label">CVC</label>
					<div class="col-md-8">
						<input type="text" size="4" class="form-control" data-stripe="cvc"/>
					</div>
				</div>
			
			    <div class="form-group">
			        <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
			        <div class="col-sm-8">
				     	<div class="row">
				          	<div class="col-xs-6">
					        	<select class="form-control col-sm-2" data-stripe="exp-month">
					                <option value="01">January (01)</option>
					                <option value="02">February (02)</option>
					                <option value="03">March (03)</option>
					                <option value="04">April (04)</option>
					                <option value="05">May (05)</option>
					                <option value="06">June (06)</option>
					                <option value="07">July (07)</option>
					                <option value="08">August (08)</option>
					                <option value="09">September (09)</option>
					                <option value="10">October (10)</option>
					                <option value="11">November (11)</option>
					                <option value="12">December (12)</option>
					        	</select>
				            </div>
				            <div class="col-xs-6">
				              	<select class="form-control" data-stripe="exp-year">
				              		<?php 
			              		$y= \Carbon\Carbon::today()->format('Y');
			              		for( $i=0; $i<10; $i++){
								?>
				              			<option value="{{$y}}">{{$y}}</option>
				              		<?php 
			              			$y++;
			              		}
			              		?>
					                
				              	</select>
				            </div>
			          	</div>
			        </div>
		      	</div>
		      	
		      	@if($save_option)
		      	<div class="form-group">
		      		<label class="col-md-3 control-label">Save this card for later?</label>
					<div class="col-md-8">
						<input checked name="save_card" value="true" data-toggle="toggle" data-on="Save Card" data-off="Don't Save Card" data-onstyle="success" data-offstyle="danger" type="checkbox">
					</div>
		      	</div>
				@endif
				
			    <div class="form-group">
					<div class="col-md-8 col-md-offset-3">
						<button type="submit" class="btn btn-ub btn-block payment-btn" id="process_payment_button" data-submit-text="{{ $processingText }}"  data-default-text="{{ $submitButtonText }}" autocomplete="off">{{ $submitButtonText }}</button>
		
					</div>
				</div>	