@extends('app')

@section('css')
	<!--  toggle switch css -->
	<link href="{{ asset('/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
@endsection


@section('scripts')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript" src="{{ asset('js/stripe-capture-card.js') }}"></script>
	@include('scripts.bootstrap-toggle')
@endsection

<?php 
/**
 *
 * @issues
 * UB-130 User Can Purchase 
 */

?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5 col-md-offset-3">
			<h2>Update Your Payment Details</h2>
			
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-ub">
				    <div class="panel-heading" role="tab" id="headingOne">
				      	<h3 class="panel-title">
					        Your Current Payment Details
				      	</h3>
				      	
				      	
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
				      	<div class="panel-body">
				    		<div class="row">
								<div class="col-md-3"><b>{{$saved_card->brand}}</b></div>
								<div class="col-md-6">XXXX-XXXX-XXXX-{{$saved_card->last4}}</div>
								<div class="col-md-3" style="text-align:right;">Exp: {{ str_pad($saved_card->exp_month, 2, "0", STR_PAD_LEFT ) }} / {{$saved_card->exp_year}}</div>
							</div>
						</div>
					</div>
			  	</div>
			  	
			  	
			  	
			  <div class="panel panel-ub">
    				<div class="panel-heading" role="tab" id="headingTwo">
      					<h3 class="panel-title">
        					Change Payment Details
				      </h3>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
				      	<div class="panel-body">
				      	
				        	{!! Form::open(['url'=>'payment/card/update', 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'payment-form']) !!} 
				
								<div class="form-group">
									<div class="col-md-8 col-md-offset-3">
										
										<div id="error-wrapper" class="alert alert-danger" style="display:none;">
											<strong>Whoops!</strong> There were some problems with your credit card information.<br><br>
											<span id="payment-errors"></span>
										</div>
									</div>
								</div>
									
									
								
								@include('payment.capture', ['submitButtonText' => 'Update Payment Details', 'processingText'=>"Saving New Payment Details...", "save_option"=>FALSE]) 
								
							{!! Form::close() !!}
							
				      	</div>
			    	</div>
				</div>
			</div>
		</div>	  
			
	</div>

</div>
@endsection
		  