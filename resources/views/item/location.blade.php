@extends('app')


<?php 
/**
 * @issues
 * UB-25 Item can have a Location [2015-07-02]
 */


if($item->location) {
	$lat_init= $item->location->lat;
	$lng_init= $item->location->lng;
}
else {
	$lat_init= 44.0610;
	$lng_init= -121.3053;
}
	

?>
@section('scripts')
	@include('scripts.google-maps-api')
	@include('scripts.google-geocode', ['lat'=> $lat_init, 'lng'=> $lng_init, 'id' => $item->id, 'type'=>'item', 'url' => url('location/geocode')])
@endsection


@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5">
			
			@if( count($item->location) )
				<h2 id="current-address">{{$item->location->address}}</h2>
				
			@else
				<h2>You haven't set a location yet!</h2>
			@endif
			
			
			<div class="embed-responsive embed-responsive-16by9">
			    <div id="map-canvas" class="embed-responsive-item"></div>
			</div>
		</div>
	
		<div class="col-md-6">
			
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<h2>Choose a new location</h2>
			<p>
				Picking a location helps you to rent to neighbors close-by. You can describe your location in any of these ways:
				<ul>
					<li>street address - 1044 NW Bond St Bend, OR 97701</li>
					<li>intersection - NW Idaho & NW Wall, Bend, OR</li>
					<li>landmark - Juniper Park, Bend, OR</li>
					<li>neighborhood - Old Mill District, Bend, OR</li>
					<li>zipcode - 97701</li>
				</ul>
				
			</p>

			
      		<div class="form-group">
				{!! Form::text('address', "Old Mill District, Bend, OR", ['id' => 'address', 'class' => 'form-control']) !!}
			</div>
			<div class="form-group">
      			<input type="button" class="btn btn-ub btn-block" value="Set Location" onclick="codeAddress()">
      			
      			<a class="btn btn-ub btn-block" href=" {{ url('item/'.$item->id.'/edit') }}">Save & Finish</a>
			</div>	
			
			
		</div>
	</div>
</div>


@endsection



