@extends('app')


<?php 
/**
 * @issues
 * UB-3 User can create Item [2015-06-23]
 */
?>

<?php 
	$user= $item->user;
?>

 
	@section('scripts')
		@include('scripts.google-maps-api')
        
    @if($user->location)
		@include('scripts.map-display', ['lat'=> $user->location->lat, 'lng'=> $user->location->lng ])
    @endif
        
        @include('scripts.favorites-js')
	@endsection


@section('content')

@include('search.filter-menu')


<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container-fluid">


	<div class="row" id="show-item">
	
		<div class="col-md-7 col-lg-8">
			<div class="col-md-12 col-lg-7">
				<img src="{{ $item->photo }}" class="item-photo img-responsive">
			</div>
			
			<div class="col-md-12 col-lg-5">
				
				<div class="item-heading">
					@if($editable) 
					<a href="{{ action('ItemController@edit', $item->id) }}">{{ $item->title }} [edit item]</a> 
					@else  
					{{ $item->title }} 
					@endif	
				</div>
				
				<div class="item-price">{!! $item->displayRentalDetails(false, true); !!}</span></div>
						
				<div class="item-category">{!! $item->print_category() !!}</div>
				
				<div class="item-description">{!! nl2br(e($item->description))  !!}</div>			
			
			</div>
		
		</div>
		
		<div class=" col-md-5 col-lg-4">
			<div id="item-contact">
				<div class="dropdown">
				  <button class="btn btn-ub btn-lg btn-block dropdown-toggle" type="button" id="contact-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    Contact Owner
				    <?php $style= ($user->contact_email) ? "" : "disabled"; ?><i class="glyphicon glyphicon-envelope contact-icon-sm contact-icon-left {{ $style}}" aria-hidden="true"></i>
				    <?php $style= ($user->contact_phone) ? "" : "disabled"; ?><i class="glyphicon glyphicon-earphone contact-icon-sm {{ $style}}" aria-hidden="true"></i>
				    <?php $style= ($user->contact_sms) ? "" : "disabled"; ?><i class="glyphicon glyphicon-comment contact-icon-sm {{ $style}}" aria-hidden="true"></i> 
				  </button>
				  <ul class="dropdown-menu dropdown-block" aria-labelledby="contact-dropdown">
				  	@if( $user->contact_email)
				    <li><a href="mailto:{{ $user->email }}" class="btn btn-category btn-block">Email: {{$user->email}}</a></li>
				    @else
				    <li><span class="disabled">Please don't email the owner</span></li>
				    @endif
				    
				    @if( $user->contact_phone)
				    <li><a href="tel:{{ $user->phone }}" class="btn btn-category btn-block">Call: {{ $user->phone }}</a></li>
				     @else
				    <li><span class="disabled">Please don't call the owner</span></li>
				    @endif
				    
				    @if( $user->contact_sms)
				    <li><a href="sms:{{ $user->phone }}" class="btn btn-category btn-block">Text: {{ $user->phone }}</a></li>
				     @else
				    <li><span class="disabled">Please don't text the owner</span></li>
				    @endif
				    
				  </ul>
				</div>
			</div>
			<div id="item-location">
				@if ( count($user->location) )	
					<br />			
					<h3>{{$user->location->address}}</h3>
					<div class="embed-responsive embed-responsive-4by3">
					    <div id="map-canvas" class="embed-responsive-item"></div>
					</div>
				@endif
			</div>
            
            
            <?php /* UB-50 Flag Item As Inappropriate [2016-02-23] */ 
            
                $star_class= "glyphicon-star";
                $tooltip_text= "Add to Favorites";
                $session_user_id= '';
                
                if ( Auth::check() ){ //the user is signed-in
                    $session_user= Auth::user();
                    $session_user_id= $session_user->id;
                    
                    if( $session_user->favorite_items->contains( $item ) ){
                        $star_class.= " favorite";
                        $tooltip_text= "Remove from Favorites";
                    }
                    
                }
                
            ?>
            <div id="item-response">   
                
                @if ( Auth::check() )
                <h3 class="itemfav"><i id="itemfav{{ $item->id }}" class="glyphicon {{ $star_class }} " data-toggle="tooltip" title="{{$tooltip_text}}" onclick="toggleFavorite('{{ $item->id }}', '{{ $session_user_id }}' )"></i></h3>
                @endif
                
                <a href="{{ url('flag/'.$item->id) }}"><h3 class="itemflag"><i class="glyphicon glyphicon-flag" data-toggle="tooltip" title="Flag Item As Inappropriate"></i></h3></a>
                    
                <h3 class="itemshare"><i id="flag{{ $item->id }}" class="glyphicon glyphicon-share " data-toggle="tooltip" title="Share Item" onclick="flagItem('{{ $item->id }}', '{{ $session_user_id }}')"></i></h3>
            </div>
            
            
		</div>
			
        
		
	</div>
</div>
@endsection
