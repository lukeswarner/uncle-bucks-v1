@extends('app')


<?php 
/**
 * @issues
 * UB-3 User can create Item [2015-06-23]
 */
?>


<?php 
	$item= "";		// UB-134 - Item Rental Period 
					// item.basic form uses $item to decide which rental_period to default to, since we want to display the current period
					//		for users who are editing an item via item.edit.
					//		In item.create blade, we don't yet have an item, so pass in an empty and item.basic will use the default
					//		rental_period
?>


@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10">		
					@include('errors.list')

					{!! Form::open(['url'=>'item', 'method' => 'post', 'class'=>'form-horizontal']) !!}
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h2>List an item for rent</h2>
							</div>
						</div>

						@include('item.basic', ['submitButtonText' => 'Create Item'])
							
						
					{!! Form::close() !!}
		</div>	
		
	</div>
</div>
@endsection
