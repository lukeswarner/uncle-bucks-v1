@extends('app')

@section('content')
<div class="container">
	<h1>Items</h1>
	
	@foreach( $items as $item )
		<div class="row">
			<div class="col-md-8">
				<div class="item-heading">
					<a href="{{ action('ItemController@show', $item->id) }}">{{ $item->title }}</a> 
					
					&nbsp;&nbsp;-&nbsp;&nbsp;
					
					${{ $item->price }} / day
				</div>
			</div>
			<div class="col-md-4">
				{!! $item->print_category() !!}
			</div>
		</div>
		<hr>
	@endforeach
	
	<div>{!! $items->render() !!}</div>
</div>
@stop
