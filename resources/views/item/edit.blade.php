@extends('app')


<?php 
/**
 * @issues
 * UB-3 User can create Item [2015-06-23]
 */
?>

<?php 
	$user= $item->user;
?>

 
	@section('scripts')
		@include('scripts.google-maps-api')
		@include('scripts.map-display', ['lat'=> $user->location->lat, 'lng'=> $user->location->lng ])
	@endsection


@section('content')
<div class="container-fluid">

	<div class="row">
		<div class="col-lg-3 col-lg-offset-6 pull-right">
			<button class="btn btn-ub-inverse btn-block set_filter" id="back-to-my-items" name="user">Back to My Items</button>
		</div>
		<div class="col-lg-3"><h2 class="page-title">Edit Item</h2></div>
		
	</div>
    
    @if($item->is_flagged())
        <div class="row section-flags border-danger ">
            <h2 class="text-danger">This item has been reported as inappropriate and has been removed</h2>
            <p>Review why your item was flagged, make the appropriate changes, and publish your item.</p>
            
            
            <div class="row">
                <div class="col-sm-2"><h3>Date Flagged</h3></div>
                <div class="col-sm-2"><h3>Reason</h3></div>
                <div class="col-sm-8"><h3>Detailed Explanation</h3></div>
            </div>
            
            <?php $i=0; ?>
            @foreach($item->getFlags() as $flag)
                <?php 
                    $bg;
                    if($i % 2 == 0 ){ 
                        $bg= "bg-zebra";
                    }
                    else {
                        $bg= "bg-white";
                    }
                    $i++;
                ?>
                <div class="row row-flag {{$bg}}">
                    <div class="col-sm-2">{{ $flag->created_at->format('M d, Y') }}</div>
                    <div class="col-sm-2">{{ $flag->reason_pretty() }}</div>
                    <div class="col-sm-8">{{ $flag->explanation }}</div>
                </div>
            
            @endforeach
                
            </div> <!-- table-responsive -->
        </div>
    @endif 
	
	<div class="row">
		<div class="col-lg-3">
			<div id="item-photo">
				@if ( !empty($item->photo) )	 
					<img src="{{ url($item->photo) }}" class="item-photo"/>
					<a href="{{ action('ItemController@edit_photo', $item->id) }}" class="btn btn-ub btn-block">Change Photo</a>
					
				@else
				
					<a href="{{ action('ItemController@edit_photo', $item->id) }}" class="btn btn-ub btn-block">Choose Photo</a>
					
				@endif
			</div>
		</div>
		<div class="col-lg-6">
			<div id="item-details">
						
				@include('errors.list')
	
				{!! Form::model( $item, ['action'=>['ItemController@update', $item->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
	
	
					@include('item.basic', ['submitButtonText' => 'Save Item Details'])
						
					
				{!! Form::close() !!}
			
			</div>	
		</div>
		<div class="col-lg-3">
			
			<div id="item-status">
				@if( is_null($item->expires_at) )
					
					<div class="item-edit-row">
						<a href="{{ action('ItemController@publish', $item->id) }}" class="btn btn-ub btn-block">Publish Item</a>
					</div>
					
                @elseif( $item->is_flagged() )
                    <div class="item-edit-row">
						<a href="{{ action('ItemController@publish_flagged', $item->id) }}" class="btn btn-ub btn-block">Republish Item</a>
					</div>
                    
                    
				@elseif( $user->hasSubscription() )
					
					<div class="item-edit-row">
						<b class="item-edit-label">Subscription: </b>
						<h3 class="item-edit-header">{{ str_replace(' ', '&nbsp;', $user->getSubscription()->plan->name)}}</h3>
					</div>
					
					<div class="item-edit-row">
						<b class="item-edit-label">Item Renews On: </b>
						<h3 class=item-edit-header">{{ str_replace(' ', '&nbsp;', $item->expires_at->toFormattedDateString() ) }}</h3>
					</div>
					
					<div class="item-edit-row no-margin-top">
						<a href="{{ action('PaymentController@manage_subscription') }}" class="btn btn-ub btn-block">Subscription</a>
					</div>
					
				@elseif( $item->expires_at->gte(\Carbon\Carbon::today()) )
					
					<div class="item-edit-row">
						<b class="item-edit-label">Expires On: </b>
						<h3 class="item-edit-header">{{ str_replace(' ', '&nbsp;', $item->expires_at->toFormattedDateString() ) }}</h3>
					</div>
					
					<div class="item-edit-row">
						<a href="{{ action('ItemController@renew', $item->id) }}" class="btn btn-ub btn-block">Renew Item</a>
					</div>
					
				@else
					
					<div class="item-edit-row">
						<b class="item-edit-label text-danger">Expired on:  </b>
						<h3 class="item-edit-header text-danger">{{ str_replace(' ', '&nbsp;', \Carbon\Carbon::now()->diffForHumans( $item->expires_at, TRUE ) ) }} ago!</h3>
					</div>
					
					<div class="item-edit-row">	
						<a href="{{ action('ItemController@renew', $item->id) }}" class="btn btn-danger btn-lg btn-block">Renew Item</a>
					</div>
					
				@endif
			</div>
		
			<div id="item-category">
				<div class="item-edit-row">
						<b class="item-edit-label">Category: </b>
						<h3 class="item-edit-header">{{ str_replace(' ', '&nbsp;', $item->category->name ) }}</h3>
				</div>
				
				@if ( count($item->category) )	 
				<div class="item-edit-row">
					<a href="{{ action('ItemController@edit_category', $item->id) }}" class="btn btn-ub btn-block">Change Category</a>
				</div>	
				@else
				<div class="item-edit-row">
					<p>It's important to decide the right category to list your item under.</p> 
					<a href="{{ action('ItemController@edit_category', $item->id) }}" class="btn btn-ub btn-block">Choose a Category</a>
				</div>	
				@endif
				
			</div>
			
			<div id="item-location">
				
				@if ( $user->location )	 
					<div class="item-edit-row">
						<b class="item-edit-label">Location: </b>
						<h3 class="item-edit-header">{{ str_replace(' ', '&nbsp;', $user->location->address) }}</h3>
					</div>
					
					<div class="item-edit-row">
						<div class="embed-responsive embed-responsive-4by3">
						    <div id="map-canvas" class="embed-responsive-item"></div>
						</div>
					</div>
					
				@else
				
					<p>It looks like you haven't specified where your item will be located. This helps renters find items available nearby. Don't worry, you don't have to provide your home address- use a nearby cross intersection or neighborhood if you prefer.</p> 
	
				@endif
				
				<?php  /*  UB-131 User Can Save Location
				<a href="{{ action('ItemController@edit_location', $item->id) }}" class="btn btn-ub btn-block" style="margin-top: 10px;">Choose a Location</a>
				*/   ?>
			</div>
		
			
		</div>
		
	</div>
	
	
	
</div>
@endsection
