<?php




?>
<div class="form-group">
<div class="input-group">
<span class="input-group-addon">@</span>
{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Your email address']) !!}
</div>
</div>

<div class="form-group">
<div class="input-group">
<span class="input-group-addon">#</span>
{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Your phone number']) !!}
</div>
</div>

		
		<div class="form-group">
			<input value="contact-phone" checked data-toggle="toggle" data-on="Contact me via phone" data-off="Don't show my phone number" data-onstyle="success" data-offstyle="danger" type="checkbox">
		</div>
		
		<div class="form-group">
			<input value="contact-email" checked data-toggle="toggle" data-on="Send email message" data-off="Don't send me email messages" data-onstyle="success" data-offstyle="danger" type="checkbox">
		</div>
		
		
