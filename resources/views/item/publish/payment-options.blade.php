				<div class="form-group">
					<div class="col-md-6">
						<h3>One Item for One Buck</h3>
						<ul>
							<li>$1 / item</li>
							<li>Item expires after one month</li>
							<li>Manually renew each item</li>
						</ul>
					</div>
					
					<div class="col-md-6">
						<h3>Unlimited Items Subscription</h3>
						<ul>
							<li>$10 / month</li>
							<li>Unlimited Items</li>
							<li>Items never expire!</li>
							<li>All items automatically renew</li>
						</ul>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-6">
						<a href="{{ url('payment/charge/one') }}" class="btn btn-ub btn-block">One Item for One Buck</a>
					</div>
					
					<div class="col-md-6">
						<a href="{{ url('payment/subscription/monthly') }}" class="btn btn-ub btn-block">Unlimited Items Subscription</a>
					</div>
				</div>