@extends('app')

@section('css')
	<!--  toggle switch css -->
	<link href="{{ asset('/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
	@include('scripts.bootstrap-toggle')
@endsection

<?php 
/**
 * @issues
 * UB-124 User Can Publish or Renew Item
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 col-md-offset-1">	
			<div id="item-photo">	 
				<img src="{{ url($item->photo) }}" class="item-photo"/>
			</div>
		</div>
		<div class="col-md-6">	
			<div class="row">
				<div class="col-md-7"><h2>{{$item->title}}</h2></div>
				<div class="col-md-5">
					@if( $item->expires_at->gte(\Carbon\Carbon::today()) )
						<h4 class="item-expires-at">{{ $item->print_expiration() }}</h4>
					@else
						<h4 class="text-danger item-expires-at">{{ $item->print_expiration() }}</h4>
					@endif
				</div>
			
			</div>
			
			
			
			@if( $item->is_expired() == FALSE )
				<h3>Two ways to keep your item listed on Uncle Bucks...</h3>
				<p>
					Your item will be listed on Uncle Bucks until it expires. To keep your item listed, choose to either add another month to the current expiration date or subscribe to the Unlimited Items  plan.
				</p>
				
			@else
				<h3>Two ways to get your item back on Uncle Bucks...</h3>
				<p>
					Because your item is past its expiration date it has been removed from the Uncle Bucks search page. 
					That means that even if you have exactly what someone is looking for, they won't be able to find your item.<br /><br />
					To relist your item, choose to either renew your item for one month or subscribe to the Unlimited Items plan.
				</p>
			@endif
			
			@include('errors.list')
			
			@include('item.publish.payment-options')
			
		</div>
	</div>
	
</div>
@endsection
