			{!! Form::model( $item, ['action'=>['ItemController@renew', $item->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
				
					<div class="form-group">
						<div class="col-md-6">
							<button type="submit" name="manual_renew" value="true" class="btn btn-ub btn-block">{!! $monthlyButton !!}</button>
						</div>
						<div class="col-md-6">
							<button type="submit" name="auto_renew" value="enable" class="btn btn-ub btn-block">{!! $autoRenewButton !!}</button>
						</div>
					</div>
	
			{!! Form::close() !!}