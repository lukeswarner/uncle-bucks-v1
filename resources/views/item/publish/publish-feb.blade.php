@extends('app')

<?php 
/**
 * @issues
 * UB-130 User Can Pay
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 col-md-offset-1">	
			<div id="item-photo">	 
				<img src="{{ url($item->photo) }}" class="item-photo"/>
			</div>
		</div>
		<div class="col-md-6">	
			<h2 class="no-margin-top">The last step: publish your {{$item->title}}</h2>

			<p>
				And just because we think you are so gosh darn nice, we decided to let you in on a little secret. 
				We ain't chargin' for posts right now, so post as much as you can while it's FREE. <br /><br/>
			</p>
			<p>
				<h4>All items posted before February 1st will set to auto renew without charge FOREVER!</h4>
			</p>
			<h2>&nbsp;</h2>
			
			@include('errors.list')
				
			
			
			@if($item->user->test)
				@include('item.publish.payment-options')
				
			@else 
				@include('item.publish.promo-publish-form', ['autoRenewButton'=>'Publish Forever, for FREE!'])
			@endif
			
		</div>
	</div>
	
</div>
@endsection