@extends('app')

<?php 
/**
 * @issues
 * UB-130 User Can Pay
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 col-md-offset-1">	
			<div id="item-photo">	 
				<img src="{{ url($item->photo) }}" class="item-photo"/>
			</div>
		</div>
		<div class="col-md-6">	
			<h2 class="no-margin-top">The last step: publish your {{$item->title}}</h2>
			<p>
				You can post this item for one month or purchase an Unlimited Items monthly subscription.
			</p>

			
			@include('errors.list')
				
			
			@include('item.publish.payment-options')
			
		</div>
	</div>
	
</div>
@endsection