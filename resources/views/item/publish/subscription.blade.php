@extends('app')

<?php 
/**
 * @issues
 * UB-130 User Can Pay
 */
?>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3 col-md-offset-1">	
			<div id="item-photo">	 
				<img src="{{ url($item->photo) }}" class="item-photo"/>
			</div>
		</div>
		<div class="col-md-5">	
			<h2>{{$item->title}}</h2>
			<h4>Your {{$plan->name}} subscription is active!</h4>
			<p>
				@if( $user->cancelled() )
					Subscription Ends: {{ $item->user->subscription_ends_at->format('F m, Y') }}
				@else
					Subscription Renews: {{ $subscription->current_period_end->format('F m, Y') }}
				@endif
			</p>

			
			<h2>&nbsp;</h2>
			
			@include('errors.list')
				
			
			{!! Form::open(['url'=>'payment/subscribed/active', 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'payment-form']) !!} 
			
					<div class="form-group">
						
						<div class="col-md-12">
							<button type="submit" name="subscripton_publish" class="btn btn-ub btn-block">Publish My Item</button>
						</div>
					</div>
	
			{!! Form::close() !!}
			
		</div>
	</div>
	
</div>
@endsection