@extends('app')

<?php 
/**
 * @issues
 * UB-130 User Can Pay
 */
?>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3 col-md-offset-1">	
			<div id="item-photo">	 
				<img src="{{ url($item->photo) }}" class="item-photo"/>
			</div>
		</div>
		<div class="col-md-5">	
			<h2>The last step: publish your {{$item->title}}</h2>
			<p>
				After publishing, your item will be listed on Uncle Bucks for 1 month. 
				You can choose to renew your posting at any time, and we'll add another month to the expiration date. 
				
			</p>
			<p>
				<h4 style="line-height: 1.4;">And since you're new to the neighborhood, we have a welcome gift for you:<br /> <b>Your first item is FREE.</b> After that, it's only $1 per month to renew.</h4>
				
			</p>
			
			<h2>&nbsp;</h2>
			
			@include('errors.list')
				
			{!! Form::model( $item, ['action'=>['ItemController@publish', $item->id], 'class'=>'form-horizontal']) !!}
			
				{!! Form::hidden('payment', 'first') !!}
			
				<div class="form-group">
					
					<div class="col-md-12">
						<button type="submit" class="btn btn-ub btn-block">Publish My Item For FREE!</button>
					</div>
				</div>
	
			{!! Form::close() !!}
			
		</div>
	</div>
	
</div>
@endsection
