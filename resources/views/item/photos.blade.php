@extends('app')


<?php 
/**
 * @issues
 * UB-27 User can create Item [2015-07-11]
 */
?>

@section('scripts')
	@include('scripts.file-upload')
@endsection


@section('content')
<div class="container-fluid">
	<div class="row">
	
		
		
		@if ( !empty($item->photo) )	
		<div class="col-md-5 col-xs-12 col-sm-12">
			
			<img src="{{ url($item->photo) }}" class="img-responsive current-photo" />	
		</div>
		@else
		
		<div  class="col-md-2">
			&nbsp;
		</div>
		
		@endif
		
		<div class="col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-0">
				<h2 class="hidden-xs hidden-sm" >&nbsp;</h2>
		
				<h4>Choose a photo:</h4>
				<h2 class="no-margin-top">{{$item->title}}</h2>
		
					{!! Form::model( $item, ['action'=>['ItemController@upload_photo', $item->id], 'files'=>true, 'class'=>'form-horizontal']) !!}
					
						
						
						<div class="col-lg-12">
							@include('errors.list')
				            <div class="input-group input-group-ub">
				                <span class="input-group-btn">
				                    <span class="btn btn-ub btn-file">
				                        Browse&hellip; <input type="file" name="photo"></input>
				                    </span>
				                </span>
				                <input type="text" class="form-control" readonly>
				            </div>
				            
				        </div>
						
					
						
						<div class="form-group">
							<div class="col-lg-12" style="margin-top:20px;">
								{!! Form::submit('Upload Photo', ['class' => 'btn btn-ub btn-block']) !!}
							</div>
						</div>
							
					
					{!! Form::close() !!}
				
		</div>
	</div>
</div>
@endsection
