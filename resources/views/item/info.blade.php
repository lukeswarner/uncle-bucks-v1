@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
	
	<!-- start of row  -->
        <div class="col-md-10 col-md-offset-1 col-lg-offset-2">
			<h2>Did you want to post an item to our rental listings page?</h2>
        </div>
        
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<p>
				Before you can do that, you'll need to login to your Uncle Bucks account.
            </p>
            <p>    
                If you don't have an account with us, you can register now. Don't worry: it's easy, and will let you keep track of the items you post so you can quickly renew your them before they expire.
            </p>
            <p>
				If you have questions about how the whole process works, why don't you take a look at our <a href="{{ url('resources/faq') }}" class="underline">FAQ page</a>.
			</p>
		</div>
	</div>
    
    <div><h4>&nbsp;</h4></div>
    <div class="row">
		<div class="col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-2">
            <a href="{{ url('auth/login')}}" class="btn btn-ub btn-block">Login</a>
			
        </div>
        <div class="col-md-5 col-lg-3 col-lg-offset-1">
            <a href="{{ url('auth/register')}}" class="btn btn-ub btn-block">Register</a>
        </div>
	</div>
    
    <!--
	<div class="row">
		<div class="col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-2">
			<div style="width:100%; margin:auto;">
				<a href="{{ url('auth/register')}}" class="btn btn-ub btn-block">Register</a>
				<a href="{{ url('auth/login')}}" class="btn btn-ub btn-block">Login</a>
				<a href="{{ url('resources/faq')}}" class="btn btn-ub btn-block">Read the FAQs</a>	
			</div>
		</div>
	</div>
    -->
	<!--  end of row -->
	
	
</div>
@endsection
