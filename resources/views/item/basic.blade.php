<?php 
/**
 * @issues
 * UB-12 Category CRUD) [2015-06-16]
 */

	/*
	 * required variables
	 * 		$submitButtonText	- used to name the submit button
	 */

?>

		<div class="form-group">
			<label class="col-lg-3 control-label">Item Title</label>
			<div class="col-lg-8">
				{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Item title']) !!}
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-lg-3 control-label">Rental Period</label>
			<div class="col-lg-8">
				{!! Form::select('rental_period', ['day'=>'Daily', 'hour'=>'Hourly'], empty($item) ? 'day': $item->rental_period , ['class'=>'form-control']); !!}
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label">Rental Price</label>
			<div class="col-lg-8">
				<div class="input-group">
					<span class="input-group-addon">$</span>
					{!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Rental Price']) !!}
					<span class="input-group-addon">.00</span>
				</div>
			</div>
		</div>


		
		<div class="form-group">
			<label class="col-lg-3 control-label">Description</label>
			<div class="col-lg-8">
				{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'A detailed description - make sure to include keywords that will help users find your item when searching!']) !!}
			</div>
		</div>
		
		
		<div class="form-group">
			<div class="col-lg-8 col-lg-offset-3">
			
				{!! Form::submit($submitButtonText, ['class' => 'btn btn-ub btn-block']) !!}
			</div>
		</div>