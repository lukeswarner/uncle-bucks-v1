
<div class="container-fluid container-masonry">
	<h1>{{ $header_text }}</h1>
	<div class="row row-masonry">
	@foreach( $items as $item ) 
		<div class="item item-masonry">
      		<a href="{{ url('item', $item->id) }}">
	      		<div class="well well-masonry"> 
	      			@if ( $item->photo ) 
	      				<div class="photo-container">
	      					<img src="{{ $item->photo }}" class="photo-masonry grow">
	      				</div>
					@endif
					
					<h3><a href="{{ url('item', $item->id) }}">${{ $item->price }}/DAY</a> </h3>
				</div>
			</a>
		</div>
		
	@endforeach
	</div>
	<div>{!! $items->render() !!}</div>
</div>
