			{!! Form::model( $item, ['action'=>['ItemController@renew', $item->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
				
					<div class="form-group">
						<div class="col-md-8">
							<button type="submit" name="auto_renew" value="disable" class="btn btn-danger btn-lg btn-block">{!! $autoRenewButton !!}</button>
						</div>
						<div class="col-md-4">
							<a href="{{ action('ItemController@edit',$item->id)}}" class="btn btn-ub btn-block">Return to My Item</a>
						</div>
					</div>
	
			{!! Form::close() !!}