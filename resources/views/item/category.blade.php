@extends('app')


<?php 
/**
 * @issues
 * UB-3 User can create Item [2015-06-23]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-2">
			<h4>Choose a category:</h4>
			<h2 class="no-margin-top">{{$item->title}}</h2>
		</div>
		<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-2">
					@include('errors.list')
					
					@if( count($item->category) )
						<div style="margin-bottom: 20px;">Current Category: {!! $item->print_category("span") !!}</div>
					@endif
					
					
					{!! Form::model( $item, ['action'=>['ItemController@update_category', $item->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
					
						@foreach ( $category_groups as $group )
							<div class="category-group-box">
								<h3>{{ $group->name}} </h3>
						
								
							
								@forelse( $group->categories as $category)
									
										<div class="checkbox">
  											<label>
  											
												<?php 
													if($item->category && ($category->id == $item->category->id)) {
														echo '<input name="category" type="radio" value="'.$category->id.'" checked>';
														echo "<!-- yes -->\r\n";
														//echo Form::radio('category', "".$category->id, 1);
													}
													else {
														echo "<!-- no -->\r\n";
														echo '<input name="category" type="radio" value="'.$category->id.'">';
														//echo Form::radio('category', "".$category->id, 0);
													}
												?>
												
												{{ $category->name }}
											</label>
										</div>
										
								@empty
									<div></div>
								
								@endforelse
							
							</div>
						
						@endforeach
						
						
						
						{!! Form::submit('Choose Category', ['class' => 'btn btn-ub btn-block']) !!}
							
							
						
					{!! Form::close() !!}
				
		</div>
	</div>
</div>
@endsection
