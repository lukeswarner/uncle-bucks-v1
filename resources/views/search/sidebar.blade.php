
<?php 

	if( !isset($category_id) ){
		$category_id = 0;
	}
?>
	
<div id="sidebar" class="hidden-sm hidden-xs">
	{!! Form::open(['id' => 'itemform', 'url' => ' ', 'method' => 'post']) !!}		
		{!! Form::hidden('category_id', $category_id, ['id'=>'category_id']) !!}
		{!! Form::hidden('filter', $filter, ['id'=>'filter']) !!}
		{!! Form::hidden('keyword', $keyword, ['id'=>'keyword']) !!}	
	{!! Form::close() !!}
		
		
	<?php // UB-135 Move Keyword Search to top of Sidebar ?>
	<div class="input-group keyword-spacer">
	 	<input type="text" placeholder="Keyword" value="{{ $keyword }}" class="set_keyword form-control"></input>
    	
		<span class="input-group-btn">
			<button class="call_search btn btn-default btn-search">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</button>
      	</span>
    </div><!-- /input-group -->

	<?php 
		$extra_class="";
		if ( $category_id  == 0) {
			$extra_class= "btn-category-active";
		}
	?>
	
			<button class="set_category btn btn-large btn-block btn-category {{ $extra_class }}" name="0">All For Rent</button>
	
	<?php 
	/*
	foreach( $groups as $cg){
		$categories= $cg->categories->sortBy('name');
	*/	
		foreach( $categories as $category ) {
			$extra_class="";
			if ( $category->id == $category_id ) {
				$extra_class= "btn-category-active";
			}
			?>
			<button class="set_category btn btn-large btn-block btn-category {{ $extra_class }}" name="{{ $category->id }}">{{ $category->name }}</button>	
			<?php 
		}
	//}
	?>		
	

</div>





<div id="sidebar-mobile" class="hidden-md hidden-lg hidden-xl">

	
			<div class="row">
				
				<div class="collapse" id="filter-collapse">
					<div class="filter-content">
						<h2 class="h-filter">Filter items</h2>
						{!! Form::open(['url'=>' ', 'method' => 'post', 'class'=>'']) !!}
							{!! Form::select('category_id', $category_list, $category_id, ['class'=>'form-control', 'id' => 'category-dropdown']); !!}
								
							<div class="input-group">
      							{!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Keyword']) !!}
					      		<span class="input-group-btn">
					        		<button type="submit" class="btn btn-default btn-filter">
									  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
									</button>
					      		</span>
					    	</div><!-- /input-group -->
									
						{!! Form::close() !!}
					</div>
					
					
				</div>
			</div>
	

</div>