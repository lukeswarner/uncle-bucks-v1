							
							<div class="container-fluid">
								<div class="filter-options">
									<?php 
										$class= "";
										if( $filter == "recent"){ $class=" chosen";}
									?>
									<button class="set_filter btn btn-filter-options{{$class}}" name="recent">Most Recent</button> 
									
									<?php 
									/*
										$class= "";
										if( $filter == "location"){ $class=" chosen";}
									?>	
									| <button class="set_filter btn btn-filter-options{{$class}}" name="location">Near Me</button> 
									*/?>
									
									<?php if( ! Auth::guest() ):
										$class= "";
										if( $filter == "favorite"){ $class=" chosen";}
									?>		
									| <button class="set_filter btn btn-filter-options{{$class}}" name="favorite">Favorites</button>
									<?php endif; 
									
									if( ! Auth::guest() ):
										$class= "";
										if( $filter == "user"){ $class=" chosen";}
									?>	
									| <button class="set_filter btn btn-filter-options{{$class}}" name="user">My Items</button> 
									<?php endif; ?>
								
									<?php 
									/*		REMOVED - No longer offering "expired" filter. UB-122 Display Expiration Details for My Items
										$class= "";
										if( $filter == "expired"){ $class=" chosen";}
									?>	
									| <button class="set_filter btn btn-filter-options{{$class}}" name="expired">Expired</button> 
									<? */ 
									?>
								</div>
							</div>