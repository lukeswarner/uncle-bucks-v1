@extends('app')


<?php 
/**
 * @issues
 * UB-22 CategoryGroup CRUD [2015-06-17]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Create New Category Group</div>
				<div class="panel-body">
					
					@include('errors.list')

					{!! Form::open(['url'=>'categorygroup', 'method' => 'post', 'class'=>'form-horizontal']) !!}
					

						@include('categorygroup.form', ['submitButtonText' => 'Create Category Group'])
							
						
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
