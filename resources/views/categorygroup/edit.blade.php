@extends('app')


<?php 
/**
 * @issues
 * UB-22 CategoryGroup CRUD [2015-06-17]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Update <b>{{ $category_group->name }}</b> Category Group</div>
				<div class="panel-body">
					<div class="col-md-9">
					@include('errors.list')
				
					{!! Form::model( $category_group, ['action'=>['CategoryGroupController@update', $category_group->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
					

						@include('categorygroup.form', ['submitButtonText' => 'Update Category Group'])
							
						
					{!! Form::close() !!}
					</div>
					
					@include('partials.delete-modal', [ 'actionURL' => 'categorygroup/'.$category_group->id, 'resource' => 'Category Group', 'message' =>'Warning! This will also delete all the categories currently assigned to this Category Group!' ])
	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
