<?php 
/**
 * @issues
 * UB-22 CategoryGroup CRUD) [2015-06-17]
 */

	/*
	 * required variables
	 * 		$submitButtonText	- used to name the submit button
	 */

?>

		<div class="form-group">
			{!! Form::label('name', 'Name:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('slug', 'URL slug:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::text('slug', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		
		<div class="form-group">
			{!! Form::label('description', 'Description:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				{!! Form::submit($submitButtonText, ['class' => 'btn btn-ub btn-block']) !!}
			</div>
		</div>