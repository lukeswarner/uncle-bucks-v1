@extends('app')


@section('css')
	<!--  toggle switch css -->
	<link href="{{ asset('/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
	@include('scripts.bootstrap-toggle')
@endsection
	
<?php 
/**
 * @issues
 * UB-7 Update User Account (sub-table of UB-1 User CRUD) [2015-06-13]
 */
?>

@section('content')
<div class="container-fluid">

	<div class="row">
		
		@if($contact_set == FALSE)
		<div class="alert alert-danger col-md-10 col-md-offset-1">
			<strong>Warning!</strong> You have disabled all contact methods. Unless you make at least one contact method visible, renters will not be able to contact you to rent your item!<br><br>
		</div>
		@endif
	
		
		@include('errors.list')

		{!! Form::model( $user, ['action'=>['UserController@update', $user->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
					
		
					
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h3>Contact Details</h3>
								<p>Update your contact information using the fields below.</p>
							</div>
						</div>
						
						@include('user.form.required-fields')
						
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h3>Contact Preferences</h3>
								<p>Use the toggles to choose how you would like to be contacted by neighbors who want to rent your item.</p>
							</div>
						</div>
					
						
						@include('user.form.extra-fields')
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								@include('user.form.submit-button', ['submitButtonText' => 'Save Changes'])
							</div>
						</div>
			
			
		{!! Form::close() !!}
		
	</div>
</div>
@endsection
