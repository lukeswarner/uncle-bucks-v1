<?php 
/**
 * @issues
 * UB-7 Update User Account (sub-table of UB-1 User CRUD) [2015-06-13]
 */
?>

					<!-- 
						<h3 class="text-center">Contact Preferences</h3>
						<p>Use the toggles to choose how you would like to be contacted by neighbors who want to rent your item.</p>
					-->
						
						<div class="form-group">
							<label class="col-md-3 control-label">Email Me</label>
							<div class="col-md-6">
								<input name="contact_email" value="true" <?php if($user->contact_email == TRUE) {echo "checked";}?> data-toggle="toggle" data-on="Email me" data-off="Don't email me" data-onstyle="success" data-offstyle="danger" type="checkbox">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Call Me</label>
							<div class="col-md-6">
								<input name="contact_phone" value="true" <?php if($user->contact_phone == TRUE) {echo "checked";}?>  data-toggle="toggle" data-on="Call me" data-off="Don't call me" data-onstyle="success" data-offstyle="danger" type="checkbox">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Text Me</label>
							<div class="col-md-6">
								<input name="contact_sms" value="true" <?php if($user->contact_sms == TRUE) {echo "checked";}?>  data-toggle="toggle" data-on="Text me" data-off="Don't text me" data-onstyle="success" data-offstyle="danger" type="checkbox">
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h3>Email Notifications</h3>
								<p>Do you want Uncle Bucks to send you an email notification when your items are expiring?</p>
							</div>
						</div>
					
						<div class="form-group">
							<label class="col-md-3 control-label">Notify Me</label>
							<div class="col-md-6">
								<input name="contact_notification" value="true" <?php if($user->contact_notification == TRUE) {echo "checked";}?> data-toggle="toggle" data-on="Notify me about item expiration" data-off="Don't notify me about item expiration" data-onstyle="success" data-offstyle="danger" type="checkbox">
							</div>
						</div>
						