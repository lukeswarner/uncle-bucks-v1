<?php 
/**
 * @issues
 * UB-7 Update User Account (sub-table of UB-1 User CRUD) 	[2015-06-13]
 * UB-139 Remove username									[2015-11-02]
 */
?>
		<?php /* 									//UB-139 Remove username
		<div class="form-group">
			<label class="col-md-3 control-label">Username</label>
			<div class="col-md-6">
				{!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'enter a unique name - this will be publicly visible']) !!}
			</div>
		</div>
		*/
		?>

		<div class="form-group">
			<label class="col-md-3 control-label">Email Address</label>
			<div class="col-md-6">
				{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => '']) !!}
			</div>
			
		</div>
		
		<div class="form-group">
			<label class="col-md-3 control-label">Phone (optional)</label>
			<div class="col-md-6">
				{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '']) !!}
			</div>
		</div>

		