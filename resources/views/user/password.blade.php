@extends('app')


<?php 
/**
 * @issues
 * UB-20 User Migration (sub-table of UB-1 User CRUD) [2015-06-13]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
			
					
					@include('errors.list')

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/password') }}">
					
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h2>Update Password</h2>
								<p>Enter a new password below.</p>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-3 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								@include('user.form.submit-button', ['submitButtonText' => 'Update My Password'])
							</div>
						</div>
					</form>
		
	</div>
</div>
@endsection
