@extends('app')


<?php 
/**
 * @issues
 * UB-7 Update User Account (sub-table of UB-1 User CRUD) 	[2015-06-13]
 * UB-139 Remove username									[2015-11-02]
 */
?>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<!--  <h2>{{ $user->username }}</h2>	UB-139 Remove Username  -->		
			<p><b>email:</b> {{ $user->email }}</p>
			<p><b>phone:</b> {{ $user->phone }}</p>
			<p><b>registration date:</b> {{ $user->created_at->format('F d, Y') }}</p>
		</div>
		<div class="col-md-4">
			<a href="{{ url('user/'.$user->id.'/edit') }}">edit account</a>		<!-- UB-139 Remove Username -->
		</div>
		
	</div>
</div>
@endsection