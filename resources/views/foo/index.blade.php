@extends('app')


<?php 
/**
 * @issues
 * UB-3 User can create Item [2015-06-23]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
				<h2>Go foo!</h2>
					@include('errors.list')

					{!! Form::open(['action'=>'FooController@filter', 'method' => 'post', 'class'=>'form-horizontal']) !!}
					

					{!! Form::submit('go', ['class' => 'btn btn-primary btn-block']) !!}
							
						
					{!! Form::close() !!}
				
		</div>
	</div>
</div>
@endsection