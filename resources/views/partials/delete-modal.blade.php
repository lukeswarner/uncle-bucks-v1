<?php 
/**
 * 
 * @issues
 * UB-12 CRUD Category
 * 
 * 
 * required variables
 * 		actionURL
 * 		resource
 * 		message
 */

?>


<!-- Button trigger modal -->
<button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">
  Delete {{ $resource }}
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete this {{ $resource }}?</h4>
      </div>
      
      @if ( $message )
      <div class ="modal-body">
      	<p>{{ $message }}</p>
      </div>
      @endif
      
      <div class="modal-footer">
      	{!! Form::open(['url'=> $actionURL, 'method' => 'delete', 'class'=>'form-horizontal']) !!}
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	
         	{!! Form::submit('Delete '.$resource, ['class' => 'btn btn-danger']) !!}
         {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>