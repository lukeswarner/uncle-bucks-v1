<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>Uncle Bucks</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/ub.css?'.date('YmdHi')) }}" rel="stylesheet">
	<link href="{{ asset('/css/masonry.css') }}" rel="stylesheet">
	
	<!-- Fonts -->
	<link href="{{ asset('/css/fonts.css') }}" rel='stylesheet' type='text/css'>
	
	@yield('css')
	
	
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body>
	<div class="container-fluid" id="ub-header">
		<div class="row"  >
				<a href="{{ url() }}"><img class="img-responsive logo" src="{{ asset('img/logo-web-black.png') }}" /></a>
			
				<a class="header-button" role="button" data-toggle="collapse" href="#menu-collapse" aria-expanded="false" aria-controls="menu-collapse">
					<img class="navbtn-menu" src="{{ asset('img/menu-black-nb.png' )}}" />
				</a>
			
			@if(Request::path() === '/')
				<a class="header-button" role="button" data-toggle="collapse" href="#filter-collapse" aria-expanded="false" aria-controls="filter-collapse">
					<img class="navbtn-filter" src="{{ asset('img/filter-black-nb.png') }}" />
				</a>
			@endif
				
		</div>
			

			<div class="row">
				
				<div class="collapse" id="menu-collapse">
					<ul class="menu-content">
						@if (Auth::guest())
							<li><a class="btn btn-ub btn-menu" href="{{ url('/auth/login') }}">Login</a></li>
							<li><a class="btn btn-ub btn-menu" href="{{ url('/auth/register') }}">Register</a></li>
							
						@else
							<!-- <li><a class="btn btn-ub btn-menu" href="{{ url('user/'.Auth::user()->username.'/edit')}}">My Account</a></li> -->
							<li><a class="btn btn-ub btn-menu" href="{{ url('/home')}}">My Account</a></li>
							<li><a class="btn btn-ub btn-menu" href="{{ url('/auth/logout') }}">Logout</a></li>
							
						@endif
					</ul>
				</div>
			</div>
			
			
		
		<div id="header-border"></div>
		
	</div>

	<div class="container-fluid container-masonry">
		<div id="sidebar" class="col-md-2 container-sidebar">
			@include('sidebar')
		</div>
		
		<div class="col-md-10 container-content">
			@yield('content')
		</div>
	</div>
	
	<footer class="footer">
    	<div class="container">
	      	<div class="col-md-4 col-xs-12 pull-left">
	      		<ul class="list-unstyled">
	      			<li><a href="#">About Uncle Bucks</a></li>
	      			<li><a href="#">How it works</a></li>
	      			<li><a href="#">FAQs</a></li>
	      		</ul>
	        </div>
	        
	        <div class="col-md-4 col-xs-12 pull-right">
	        	<p class="text-muted">Other Links Here</p>
	        </div>
	        
	        <div class="col-md-4 col-xs-12">
	        	<p class="text-muted text-center">&copy; Uncle Bucks {{ date('Y') }}</p>
	        </div>
        
      	</div>
    </footer>
	
	
	<!-- Scripts -->
	<script src="{{ asset('/js/jquery-2.1.4.min.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-3.3.1.min.js') }}"></script>
	
	@yield('scripts')


</body>
</html>
