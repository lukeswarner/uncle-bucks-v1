<script>

		function flagItem(item, user, ip) {

			$i= "#flag"+item;
			
            console.log('flagging item ' + item + ' from user ' + user + ' at ip: ' + ip);
			
            $.ajax({
					type: "POST",
					url: "{{ url('item/flag') }}",
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
					data: {
						'item' : item,
						'user' : user,
                        'ip' : ip
					},
				})
				.success( function( response_data) {
					
					$($i)
						//.addClass('glyphicon-star')
						.addClass('favorite');
					console.log(item+" favorited");
				})
				.error( function( response_data ){
					console.log("FAILURE during "+item+" favorited");
				});
		
            }
            
			 
	}
	
</script>