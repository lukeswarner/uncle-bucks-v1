<script>

	var map;
	var markers= [];
	
	function initialize( lat, lng) {
		geocoder = new google.maps.Geocoder();

	  	var latlng= new google.maps.LatLng( lat, lng );
	  	
		var map= new google.maps.Map(document.getElementById('map-canvas'), {
	    	zoom: 13,
	    	center: latlng
	  	});
		
	  	var marker = new google.maps.Marker({
	      	map: map,
	      	position: latlng
	  	});
	
	  	markers.push(marker);
	
	}

	
	google.maps.event.addDomListener(window, 'load', initialize( {{ $lat }}, {{ $lng }}) );

</script>
