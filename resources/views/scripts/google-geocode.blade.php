    <script>

	    var map;
	    var markers= [];
	    
		function initialize() {
			geocoder = new google.maps.Geocoder();
		  	var latlng = new google.maps.LatLng({{ $lat }}, {{ $lng }});
		  	var mapOptions = {
		    	zoom: 13,
		    	center: latlng
		  	}
		 	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			
		  	var marker = new google.maps.Marker({
	          	map: map,
	          	position: latlng
	      	});

	      	markers.push(marker);

		}

		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
    
    <script>

		var geocoder;
		

		function codeAddress() {
		  	var address = document.getElementById('address').value;
		  
		 	geocoder.geocode( { 'address': address}, function(results, status) {
			  	if (status == google.maps.GeocoderStatus.OK) {
			
					var location= results[0].geometry.location;

					/*
					var components= results[0].address_components;
					for ( obj in components){
						console.log( obj.types );
					}
					
					console.log( results[0] );
					*/

					
					$.ajax({
						  type: "POST",
						  url: "{{ url('location/geocode') }}",
						  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
						  data: {
							  'address' : address,
							  'lat' : location.lat(),
							  'lng' : location.lng(),
							  'id': {{ $id }},
							  'type' : "{{ $type }}",
							  },
						})
						.success( function( response_data) {
							//alert( "success" + response_data );
						
							$("#current-address").text(address);
							
							map.setCenter(results[0].geometry.location);

							if( markers.length > 0 ) {
								markers[0].setMap(null);
								markers.pop();
							}
							
					      	marker = new google.maps.Marker({
					          	map: map,
					          	position: results[0].geometry.location
					      	});

					      	markers.push(marker);
					      	markers[0].setMap(map);

						})
						.fail(
							function(xhr, status, error) {
								  alert(xhr.responseText);
							}
						);
					  
			      	
			    } else {
			      	alert('Geocode was not successful for the following reason: ' + status);
			    }
		  	});
		}
		
		

    </script>
