<?php 
	if (App::isLocal()) { ?>
			<script src="{{ asset('/js/isotope.pkgd.js')}}"></script>
		<?php 
	}
	else { ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js"></script>
		<?php 
	}
?>

<script>
/*
$(document).ready( function() {
	
	var $grid = $('.grid').isotope({
		  // options
		  itemSelector: '.grid-item',
		  percentPosition: true,
		  masonry: {
		    // set to the element
		    columnWidth: '.grid-sizer',
		    gutter: '.gutter-sizer'
		  }	
		
	});


	$('.append-button').on( 'click', function() {

		console.log('append clicked');

		$filter= $("#filter").val();
		$cat= $("#category_id").val();
		$keyword= $("#keyword").val();
		$page= 1;
		
		getItems($grid, $cat, $keyword, $filter, $page);
		
	});
	
});


function getItems($grid, $cat, $keyword, $filter, $page){

	

	console.log('calling ajax with c: '+$cat+' k:'+$keyword+' f:'+$filter+' p:'+$page);
	$.ajax({
		  type: "POST",
		  url: "{{ url('more_items') }}",
		  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
		  data: {
			  'filter' : $filter,
			  'category_id' : $cat,
			  'keyword' : $keyword,
			  'page': $page,
			  },
		})
		.success( function( response_data) {
			
			console.log(response_data);

			$grid.append( response_data ) 
	    		.isotope( 'appended', response_data );
			
			return;

		})
		.fail(
			function(xhr, status, error) {
				  alert('ajax failed');
				  return false;
			}
		);
}
*/

var $item_count= 0;
var $item_total= 0;

$(function(){

	$item_count = {{ $item_count}};
	$item_total = {{ $item_total }};

	$("#no_more_items").hide();
	
	if( $item_count == $item_total ){
		$("#spinner").hide();
	}
	else if( $item_count == 0){		//NO results were found, so hide both the spinner and the "no more items" - we use different text in unclebucks.blade 
		$("#spinner").hide();		//to inform the user where there search match ZERO results
		$("#no_more_items").hide();

	}
	else {
		$("#no_more_items").hide();
	}

    var $container = $('.grid ');
  
    $container.isotope({
      	itemSelector : '.grid-item',
    	percentPosition: true,
		masonry: {
			// set to the element
		    columnWidth: '.grid-sizer',
		    gutter: '.gutter-sizer'
		}	
    });


    
    

    /*    WORKING INFINIE SCROLL  */
    $container.infinitescroll({
    
      	navSelector  : '#page_nav',    // selector for the paged navigation 
      	nextSelector : '#page_nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.grid-item',     // selector for all items you'll retrieve
      },
     
      	// call Isotope as a callback
      	function ( newElements ) {
			$item_count += newElements.length;
    	  	
          
      		var $newElems = jQuery( newElements ).hide(); // hide to begin with

        	// ensure that images load before adding to masonry layout
        	$newElems.imagesLoaded(function(){
          		$newElems.fadeIn(); // fade in when ready
          		$container.isotope( 'appended', $newElems );

          		if( $item_count == $item_total ){
    				$("#spinner").hide();
    				$("#no_more_items").fadeIn(3000);
        	  	}
        	});

        	
    	  	
        	initialize_overlay();
      	}
    );

  });


</script>