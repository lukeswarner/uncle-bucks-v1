<script>

	var map;
	var markers= [];
	
	function initMap( lat, lng) {
		geocoder = new google.maps.Geocoder();
	
	  	var latlng= new google.maps.LatLng( lat, lng );
	  	
		var map= new google.maps.Map(document.getElementById('map-canvas'), {
	    	zoom: 13,
	    	center: latlng
	  	});
		
	  	var marker = new google.maps.Marker({
	      	map: map,
	      	position: latlng
	  	});
	
	  	markers.push(marker);
	
	}
	

	//check if new location is within 50 miles
	function verifyRegion(lat, lng){
		//console.log('verifying Region');
		
		var bend_lat= 44.0581753;
		var bend_lng= -121.3166054;
		
		$d= distance(bend_lat, bend_lng, lat, lng);
		//console.log('distance is ' + $d);

		return ($d <= 50);
	}

	//get the distance in mi between two coordinates
	function distance(lat1, lon1, lat2, lon2) {
		var p = 0.017453292519943295;    // Math.PI / 180
	  	var c = Math.cos;
	  	var a = 0.5 - c((lat2 - lat1) * p)/2 + 
	          c(lat1 * p) * c(lat2 * p) * 
	          (1 - c((lon2 - lon1) * p))/2;

	  	//return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
	  	return 7918 * Math.asin(Math.sqrt(a)); // 2 * R; R = 3959 mi
	}
	
   
	
	/**
	 * Use browser to detect the user's location
	 *
	 * If sucessful, pass the lat / lng to saveLocation
	 */
	function detectLocation() {

		clearError();
		
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(function(position){
		        //on success
	        	showResults(position.coords.latitude, position.coords.longitude, "Detected Location");
		        }, 
				//on failure
	       	 	handleFailure
		        //saveLocation("47.6542", "-122.3500", "detected by browser" )
	        );
	    } else {
	    	errorMessage("Your browser is too old to determine your location. You'll have to upgrade to a newer version or use the other method to save your location.");
	    }
	}
	
	
	
			
	/**
	 * Use Google Maps API to lookup address 
	 * 
	 * if successful, send the lat / lng to saveLocation()
	 */
	function codeAddress() {

		clearError();
		 
		var geocoder = new google.maps.Geocoder();

		
	  	var address = document.getElementById('address').value;
	  
	 	geocoder.geocode( { 'address': address}, function(results, status) {
	 		//on success
		  	if (status == google.maps.GeocoderStatus.OK) {
			  	
				var location= results[0].geometry.location;

				if( verifyRegion(location.lat(), location.lng() ) ) {
					showResults(location.lat(), location.lng(), address);
				}
				else {
					msg= "It looks like we haven't made it out to your neck of the woods. Uncle Bucks is currently only available in the Bend, Oregon area.";
					errorMessage(msg);
				}
		      
		    } 
		  	//on failure
		    else {
			    if( status == "ZERO_RESULTS" ){
				    msg= "Sorry, we couldn't find anywhere that matched '"+address+"'. Could you be a bit more specific? It might help to include the city and state.";
			    }
			    else {
					msg= "We couldn't track down that location: " + status;
			    }
			    errorMessage(msg);
		    }
	  	});
	  	
	}
		
		

	
	
	/**
	 * try to save the location by using AJAX to POST to location/store
	 *
	 *		@param float 	lat
	 *		@param float	lng
	 * 		@param string 	address 
	 *
	 *		ajax post send the above params as detected from browser or Google Maps Geolocate, 
	 *			along with the location's ower's type (user or item) and id
	 *
	 */
	function showResults(lat, lng, address){

		 $("#address-results").text(address);

		initMap( lat, lng);

		$("#new-lat").val(lat);
		$("#new-lng").val(lng);
		$("#new-address").val(address);
		
		$("#detect-row").hide();
		$("#map-row").removeClass("hidden");
			
		
		/*
		 $.ajax({
			  type: "POST",			  
			  url: "{{ url('location/store') }}",
			  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
			  data: {
				  'address' : address,
				  'lat' : lat,
				  'lng' : lng,
				  'id': "{{ $id }}",
				  'type' : "{{ $type }}",
				  }
		 })
			.success( function( response_data) {
				$("#address-results").text(address);

				initMap( lat, lng);

				$("#lat").val(lat);
				$("#lng").val(lng);
				
				$("#detect-row").hide();
				$("#map-row").removeClass("hidden");
				
			})
			.fail(
				function(xhr, status, error) {
					  alert(xhr.responseText);
				}
			);
			*/
	}


	/**
	 *	write any error messages to the location create page
	 */
	function handleFailure(error) {
		
	    switch(error.code) {
	        case error.PERMISSION_DENIED:
	            msg= "Okay. If you don't want us to detect your location that way, we respect your privacy.<br/>Why don't you enter a location and let us search for it, instead.";
	            break;
	        case error.POSITION_UNAVAILABLE:
	        	msg= "We couldn't connect to the internet. Make sure you are still connected and try again.";
	            break;
	        case error.TIMEOUT:
	        	msg= "It looks like its taking too long to detect your location. Why don't you try it again?";
	            break;
	        case error.UNKNOWN_ERROR:
	        	msg= "Uh oh, we weren't able to figure out your location. You can try it again or use the other method to save your location.";
	            break;
	    }

	    
	    errorMessage(msg);
		
	}



	function resetView(){
		$("#detect-row").show();
		$("#map-row").addClass("hidden");
	}




	function errorMessage(message){
		$("#message-row").removeClass('hidden').addClass('alert-danger');
	    $("#message").html(message);
	}

	function clearError(){

		if( ! $("#message-row").hasClass("hidden") ) {
			$("#message-row").addClass('hidden');
		    $("#message").html("");
		}
	}
</script>
