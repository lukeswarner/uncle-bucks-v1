<script>
function presentItems($items){

	var $pretty_items = [];
	for( $item of $items) {
	
						<?php $user= $item->user; ?>
						
						<div class="grid-item">
						
				     
				      		<div class="well well-masonry">
				      		
				      			@if ( $item->thumbnail ) 
				      			
				      				<div class="photo-container img">
				      				
				      					<img src="{{ $item->thumbnail }}" class="photo-masonry">
				      					
				      					
				      					<!--  UB-122 Display Expiration Details for My Items -->
				      					@if($filter == "user" && $item->auto_renew)							
				      						<div class="expiration_overlay bg-success">
					      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
					      					</div>
					      					
				      					@elseif($filter == "user" && $item->is_expired())							
				      						<div class="expiration_overlay bg-danger">
					      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
					      					</div>
					      					
			      						@elseif ($filter == "user" && $item->is_expiring())
			      							<div class="expiration_overlay bg-warning">
					      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
					      					</div>
					      					
					      				@elseif ($filter == "user")														<!-- /* UB-132 My Item Design Changes [2015-11-09] */ -->
			      							<div class="expiration_overlay bg-white">
					      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
					      					</div>
			      						@endif
			      						
			      						
				      					
				      					
				      					<div class="overlay">
						                    <div class="overlay-top">
						                    	<a class="close-overlay hidden pull-right">x</a>
						                    	<h3 id="item-title{{$item->id}}" class="item-title">{{ str_limit($item->title, 40) }}</h3>
						                    </div>
						                    
						                    <div id="overlay-content{{$item->id}}" class="overlay-content hidden-sm hidden-xs">
						                    	{{ str_limit($item->description, 128) }}
						                    </div>
						                    
						                    <div id="overlay-contact{{$item->id}}" class="overlay-contact" >
						                    
						                    </div>
						                    
						                    <div class="overlay-bottom">
						                    
						                    <!-- overlay for owners -->
						                    @if ( $filter == "user" )													<!-- UB-132 My Item Design Changes [2015-11-09] -->
						                    	 
						                    	 <div class="contact-icon ">
						                    		<a href="{{ url('item/'.$item->id.'/edit') }}" class="glyphicon glyphicon-edit glyphicon-contact" aria-hidden="true"></a><br />
						                    		Edit
						                    	</div>
						                    	 
						                    @else 
						                    
						                    <!-- overlay for everyone else  -->
						                    	<div class="contact-icon">
						                    		<a href="{{ url('item', $item->id) }}" class="glyphicon glyphicon-th-list glyphicon-contact" aria-hidden="true"></a><br />
						                    		Details
						                    	</div>
						                    	
							                    @if( $user->contact_email)
							                    <div class="contact-icon ">
							                    	<i onclick="showContact('{{ $item->id }}', '{{ $user->split_email() }}', 'email')" class="glyphicon glyphicon-envelope glyphicon-contact" aria-hidden="true"></i><br />
							                    	Email
							                    </div>
							                    @else
							                    <div class="contact-icon contact-disabled">
							                    	<i class="glyphicon glyphicon-envelope glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
							                    	Email
							                    </div>
							                    @endif
							                    
							                    
							                    
							                    	
							                    @if( $user->contact_phone )
							                    <div class="contact-icon">
							                    	<i onclick="showContact('{{ $item->id }}', '{{ $user->phone }}', 'phone')" class="glyphicon glyphicon-earphone glyphicon-contact" aria-hidden="true"></i><br />
							                    	Call
							                    </div>
							                    @else
							                    <div class="contact-icon contact-disabled">
							                    	<i class="glyphicon glyphicon-earphone glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
							                    	Call
							                    </div>
							                    @endif
							                    
							                    
							                    
							                    	
							                    @if( $user->contact_sms )
							                    <div class="contact-icon">
							                    	<i onclick="showContact('{{ $item->id }}', '{{ $user->phone }}', 'sms')" class="glyphicon glyphicon-comment glyphicon-contact" aria-hidden="true"></i><br />
							                    	Text
							                    </div>
							                    @else
							                    <div class="contact-icon contact-disabled">
							                    	<i class=" glyphicon glyphicon-comment  glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
							                    	Text
							                    </div>
							                    @endif
							                    
							                    <!-- end bottom for users -->
							                @endif 
						                    
						                    </div>
						                </div>
						                
				      				</div>
				      				
								@endif
								
								<?php 																				/* UB-132 My Item Design Changes [2015-11-09] */
								if ( $filter != "user" && Auth::check() ):
									$session_user= Auth::user();
									if( $session_user->favorite_items->contains( $item ) ){
										$star_class= "glyphicon-star favorite";
									}
									else {
										//$star_class= "glyphicon-star-empty";										/** removed per UB-111 "Dull" non-favorited star **/
										$star_class= "glyphicon-star";
									}
								
									?>
								<h3 class="itemfav"><i id="itemfav{{ $item->id }}" class="glyphicon {{ $star_class }}" onclick="toggleFavorite('{{ $item->id }}', '{{ $session_user->id }}' )"></i></h3>
								<?php endif; ?>
								
								<h3>{!! $item->displayRentalDetails() !!}</h3>
							
							</div>  <!--  / .well .well-masonry -->
							
						</div>  <!--  / .item .item-masonry -->
	
	
	}	

	return $items;
}




































	