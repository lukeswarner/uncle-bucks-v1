<script>

		

		function toggleFavorite(item, user) {

			$i= "#itemfav"+item;
			
			if( $($i).hasClass("favorite") ){

				$.ajax({
					  type: "POST",
					  url: "{{ url('item/favorite/remove') }}",
					  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
					  data: {
						  'item' : item,
						  'user' : user,
						  },
					})
					.success( function( response_data) {
						$($i).removeClass("favorite");
							//.removeClass("glyphicon-star");		/** removed per UB-111 "Dull" non-favorited star **/
						//$($i).addClass("glyphicon-star-empty");
						console.log(item+" removed");
					})
					.error( function( response_data ){
						console.log("FAILURE during "+item+" removed");
					});
			}
			else {
				$.ajax({
					type: "POST",
					url: "{{ url('item/favorite/set') }}",
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
					data: {
						'item' : item,
						'user' : user,
					},
				})
				.success( function( response_data) {
					//$($i).removeClass('glyphicon-star-empty');  /** removed per UB-111 "Dull" non-favorited star **/
					$($i)
						//.addClass('glyphicon-star')
						.addClass('favorite');
					console.log(item+" favorited");
				})
				.error( function( response_data ){
					console.log("FAILURE during "+item+" favorited");
				});
		
			}

			 
	}
	
</script>