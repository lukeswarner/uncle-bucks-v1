@extends('app')

@section('css')
	<link href="{{ asset('/css/overlay-effects.css' ) }}" rel="stylesheet">
	<link href="{{ asset('/css/masonry.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/isotope.css' ) }}" rel="stylesheet">
@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/2.1.0/jquery.infinitescroll.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
	@include('scripts.overlay-js')
	@include('scripts.favorites-js')
	@include('scripts.isotope-js', ['item_count' => count($items), 'item_total' => $total_items])
	
	<?php if( Auth::guest() && ! Session::has('hide_modal') ){ ?>
	<script>
		$('#welcomeModal').delay(6000).modal('show')
	</script>
	<?php 
		Session::put('hide_modal', TRUE);
	} 
	?>
@endsection

@section('content')


@include('search.filter-menu')


<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container-fluid">
	<div class="row">
		@if( count( $items ) == 0)
			
			<div class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
				<h2>Dagnabbit. It seems like nobody's got what your looking for.</h2>
				<p>Try changing the category you are searching within or the keyword you are looking for - you'll probably have better luck!</p>
			</div>
			
		@else 
		<div class="col-md-12">
			
			<div id="effect-1" class="row grid effects">
			
				<div class="grid-sizer"></div>
				<div class="gutter-sizer"></div>
			
				@foreach( $items as $item ) 
				<?php
					$user= $item->user;
					
					if( empty($item) || empty($item->expires_at) || empty($user) ){
						continue;
					}
				?>
					
					<div class="grid-item">
					
			     
			      		<div class="well well-masonry">
			      		
			      			@if ( $item->thumbnail ) 
			      			
			      				<div class="photo-container img">
			      				
			      					<img src="{{ $item->thumbnail }}" class="photo-masonry">
			      					
			      					
			      					<?php if($filter == "user" ) { ?>
				      					<!--  UB-122 Display Expiration Details for My Items -->
				      					<?php 
				      					if( $item->is_flagged() ){?>
				      						<div class="expiration_overlay bg-danger">
					      						<h4 class="expiration-hdr" style="margin-top: 20px; margin-bottom: 20px;">REMOVED DUE TO INAPROPRIATE CONTENT</h4>
					      					</div>
				      					<?php 
				      					}
				      					else if( $user->hasSubscription() && $user->cancelled() == FALSE ) {?> <!--  user has an active subscription that is not expired or cancelled   -->
				      					
				      						@if ($item->is_expiring())							
					      						<div class="expiration_overlay bg-warning">
						      						<h4 class="expiration-hdr">AutoRenews {{ $item->expires_at->format('M d, Y') }}</h4>
						      					</div>
					      					@else
					      						<div class="expiration_overlay bg-success">
						      						<h4 class="expiration-hdr">AutoRenews {{ $item->expires_at->format('M d, Y') }}</h4>
						      					</div>
						      				@endif
				      					
				      					<?php 
										}								
										else if( $user->hasSubscription() && $user->cancelled() )	{ ?> <!--  user has a subscription that is still current, but has been cancelled  -->
				      					
				      						@if( $item->is_renewing_before_cancellation() )	
				      							<div class="expiration_overlay bg-success">
						      						<h4 class="expiration-hdr">AutoRenews {{ $item->expires_at->format('M d, Y') }}</h4>
						      					</div>
						      				@else
						      					<div class="expiration_overlay bg-white">
						      						<h4 class="expiration-hdr">Expires {{ $item->expires_at->format('M d, Y') }}</h4>
						      					</div>
						      				@endif
				      			
				      					
				      					<?php 
				      					}
				      					else { ?>		<!--  user doesn't have a subscription   -->
					      					@if($item->is_expired())							
					      						<div class="expiration_overlay bg-danger">
						      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
						      					</div>
						      					
				      						@elseif ($item->is_expiring())
				      							<div class="expiration_overlay bg-warning">
						      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
						      					</div>
						      					
						      				@else													<!-- /* UB-132 My Item Design Changes [2015-11-09] */ -->
				      							<div class="expiration_overlay bg-white">
						      						<h4 class="expiration-hdr">{{ $item->print_expiration() }}</h4>
						      					</div>
				      						@endif
			      						
			      						<?php } ?>		<!-- end subscription options -->
			      						
			      					<?php } ?> <!-- end if filter = user -->
			      					
			      					
			      					<div class="overlay">
					                    <div class="overlay-top">
					                    	<a class="close-overlay hidden pull-right">x</a>
					                    	<h3 id="item-title{{$item->id}}" class="item-title">{{ str_limit($item->title, 40) }}</h3>
					                    </div>
					                    
					                    <div id="overlay-content{{$item->id}}" class="overlay-content  hidden-sm hidden-xs">
					                    	{{ str_limit($item->description, 128) }}
					                    </div>
					                    
					                    <div id="overlay-contact{{$item->id}}" class="overlay-contact" >
					                    
					                    </div>
					                    
					                    <div class="overlay-bottom">
					                    
					                    <!-- overlay for owners -->
					                    @if ( $filter == "user" )													<!-- UB-132 My Item Design Changes [2015-11-09] -->
					                    	 
					                    	 <div class="contact-icon ">
					                    		<a href="{{ url('item/'.$item->id.'/edit') }}" class="glyphicon glyphicon-edit glyphicon-contact" aria-hidden="true"></a><br />
					                    		Edit
					                    	</div>
					                    	 
					                    @else 
					                    
					                    <!-- overlay for everyone else  -->
					                    	<div class="contact-icon">
					                    		<a href="{{ url('item', $item->id) }}" class="glyphicon glyphicon-th-list glyphicon-contact" aria-hidden="true"></a><br />
					                    		Details
					                    	</div>
					                    	
						                    @if( $user->contact_email)
						                    <div class="contact-icon ">
						                    	<i onclick="showContact('{{ $item->id }}', '{{ $user->split_email() }}', 'email')" class="glyphicon glyphicon-envelope glyphicon-contact" aria-hidden="true"></i><br />
						                    	Email
						                    </div>
						                    @else
						                    <div class="contact-icon contact-disabled">
						                    	<i class="glyphicon glyphicon-envelope glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
						                    	Email
						                    </div>
						                    @endif
						                    
						                    
						                    
						                    	
						                    @if( $user->contact_phone )
						                    <div class="contact-icon">
						                    	<i onclick="showContact('{{ $item->id }}', '{{ $user->phone }}', 'phone')" class="glyphicon glyphicon-earphone glyphicon-contact" aria-hidden="true"></i><br />
						                    	Call
						                    </div>
						                    @else
						                    <div class="contact-icon contact-disabled">
						                    	<i class="glyphicon glyphicon-earphone glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
						                    	Call
						                    </div>
						                    @endif
						                    
						                    
						                    
						                    	
						                    @if( $user->contact_sms )
						                    <div class="contact-icon">
						                    	<i onclick="showContact('{{ $item->id }}', '{{ $user->phone }}', 'sms')" class="glyphicon glyphicon-comment glyphicon-contact" aria-hidden="true"></i><br />
						                    	Text
						                    </div>
						                    @else
						                    <div class="contact-icon contact-disabled">
						                    	<i class=" glyphicon glyphicon-comment  glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
						                    	Text
						                    </div>
						                    @endif
						                    
						                    <!-- end bottom for users -->
						                @endif 
					                    
					                    </div>
					                </div>
					                
			      				</div>
			      				
							@endif
							
							<?php 																				/* UB-132 My Item Design Changes [2015-11-09] */
							if ( $filter != "user" && Auth::check() ):
								$session_user= Auth::user();
								if( $session_user->favorite_items->contains( $item ) ){
									$star_class= "glyphicon-star favorite";
								}
								else {
									//$star_class= "glyphicon-star-empty";										/** removed per UB-111 "Dull" non-favorited star **/
									$star_class= "glyphicon-star";
								}
							
								?>
							<h3 class="itemfav"><i id="itemfav{{ $item->id }}" class="glyphicon {{ $star_class }}" onclick="toggleFavorite('{{ $item->id }}', '{{ $session_user->id }}' )"></i></h3>
							<?php endif; ?>
							
							<h3>{!! $item->displayRentalDetails() !!}</h3>
						
						</div>  <!--  / .well .well-masonry -->
						
					</div>  <!--  / .item .item-masonry -->
					
					
					
					
					
					
					
					
					
				@endforeach
			</div> <!--  / #effect-1 -->
			@endif
			
			
		</div>
	</div>
	
	@if( $filter != "favorite" )
	<div class="row">
		<img class="center-block" id="spinner" src="http://i.imgur.com/qkKy8.gif"/>
		<div id="no_more_items"><h3 class="center-text">Well crud. You've reached the end of the road. We don't have any more items that match what you are looking for.</h3></div>
		<nav id="page_nav">
		    <a href="{!! $items->nextPageUrl() !!}"></a>
		 </nav>
	</div>
	@endif
	
</div>


 	<div class="modal fade bs-example-modal-lg" id="welcomeModal" tabindex="-1" role="dialog" aria-labelledby="welcomeModalLabel">
			<div class="modal-dialog modal-lg">
		    	<div class="modal-content">
		    		      				
		      		<div class="modal-body">
       					<img class="img-responsive" src="{{ asset('img/about_comic.png') }}" />
       					<button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i></button>
       					<div class="row">
       					<div class="col-md-10 col-md-offset-1">
       						<h3 class="">Uncle Bucks: Helping Neighbors Rent From Neighbors</h3>
       						<p>
       							To start posting items for rent, just click "Register" to create an account. <br/>
       							And <i>here's a little secret</i>: We ain't chargin' for posts right now, 
       							and we'll even auto renew your items, without charge, FOREVER.
       							<h4>Post as much as you can while it's FREE.</h4>
       						</p>
       					</div>
       					</div>
       					
       					
      				</div>
		    	</div>
		  	</div>
		</div>
@endsection
