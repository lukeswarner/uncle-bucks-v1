<?php 
/**
 * @issues
 * UB-139 Remove username									[2015-11-02]
 */
?>
@extends('app')

@if ($user->location )
@section('scripts')
	@include('scripts.google-maps-api')
	@include('scripts.map-display', ['lat'=> $user->location->lat, 'lng'=> $user->location->lng])
@endsection
@endif

@section('content')
<div class="container-fluid">
	
	<div class="row">																									<!-- 	UB-139 Remove Username 		-->
		<div class="col-md-6 col-md-offset-6"><h3 class="welcome text-right"><a href="{{ url("user/".$user->id."/edit")}}">welcome {{ $user->email }}</a></h3></div>
	</div>
	
	<!-- start of row  -->
	<div class="row">
				
		<div class="col-md-6 col-lg-5 col-lg-offset-1">
			<h2 class="border-bottom dashboard-title">Items</h2>
			
			<div class="row">
				<div class="col-md-3 col-xs-2 mobile-stretch"><h2 class="dashboard-splash">{{ $user->items->count() }}</h2></div>
				<div class="col-md-9 col-xs-10 mobile-shrink dashboard-label">total items</div>
			</div>
				
			@if( $user->hasSubscription() && $user->cancelled() == FALSE ) <!--  user has an active subscription that is not expired or cancelled   -->
			
			<div class="row">
				<div class="col-md-3 col-xs-2 mobile-stretch"><h2 class="dashboard-splash">{{ $user->items->count() }}</h2></div>
				<div class="col-md-9 col-xs-10 mobile-shrink dashboard-label text-success">set to AutoRenew</div>
			</div>
			
			<div class="row">
				<div class="col-md-3 col-xs-2 mobile-stretch"><h2 class="dashboard-splash">{{ $user->items("expiring")->count() }}</h2></div>
				<div class="col-md-9 col-xs-10 mobile-shrink dashboard-label text-warning"> will Auto Renew soon</div>
			</div>
			
			@elseif( $user->hasSubscription() && $user->cancelled() )	<!--  user has a subscription that is still current, but has been cancelled  -->
			
			<div class="row">
				<div class="col-md-3 col-xs-2 mobile-stretch"><h2 class="dashboard-splash">{{ $user->items("renews_before_cancellation")->count() }}</h2></div>
				<div class="col-md-9 col-xs-10 mobile-shrink dashboard-label text-success"> will Auto Renew before subscription ends</div>
			</div>	
			
			
			@else		<!--  user doesn't have a subscription   -->
			
			
			<div class="row">
				<div class="col-md-3 col-xs-2 mobile-stretch"><h2 class="dashboard-splash">{{ $user->items("expiring")->count() }}</h2></div>
				<div class="col-md-9 col-xs-10 mobile-shrink dashboard-label text-warning"> expiring soon</div>
			</div>	
			
			@endif
			
			<div class="row">
				<div class="col-md-3 col-xs-2 mobile-stretch"><h2 class="dashboard-splash">{{ $user->items("expired")->count() }}</h2></div>
				<div class="col-md-9 col-xs-10 mobile-shrink dashboard-label text-danger"> expired</div>
			</div>	
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><button class="set_filter btn btn-dashboard" name="user">Manage Items</button></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><button class="set_filter btn btn-ub btn-block" name="user">Manage Items</button></div>
			</div>
		
		</div>
		
		
		
		<div class="row hidden-md hidden-lg">&nbsp;</div>
		
		
		
		<div class="col-md-6 col-lg-5 col-lg-offset-1">
			<h2 class="border-bottom dashboard-title">Subscription</h2>
		
			@if( $user->hasSubscription() && $user->cancelled() == FALSE ) <!--  user has an active subscription that is not expired or cancelled   -->
			<div class="row">
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-certificate" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label">{{ $plan->name }} {{ $plan->interval }}ly subscription</div>
			</div>
			
			<div class="row">
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-usd" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label">{{ $plan->amount/100 }} / {{ $plan->interval }}</div>
			</div>	
			
			<div class="row">
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-calendar" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label">Renews: {{$subscription->current_period_end->format('F d, Y')}}</div>
			</div>
			
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{action('PaymentController@manage_subscription')}}" class="btn btn-dashboard">Manage Subscription</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ action('PaymentController@manage_subscription') }}" class="btn btn-ub btn-block">Manage Subscription</a></div>
			</div>
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{ url('payment/card/update') }}" class="btn btn-dashboard">Update Payment Details</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ url('payment/card/update') }}" class="btn btn-ub btn-block">Update Payment Details</a></div>
			</div>
			
			@elseif( $user->hasSubscription() && $user->cancelled() )	<!--  user has a subscription that is still current, but has been cancelled  -->
			
			<div class="row">
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash text-danger"><b class="glyphicon glyphicon-ban-circle" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label"><span class="text-danger">CANCELLED:</span> {{ $plan->name }} {{ $plan->interval }}ly subscription</div>
			</div>
			
			<div class="row">
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-calendar" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label">Ends: {{$user->subscription_ends_at->format('F d, Y')}}</div>
			</div>
			
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{action('PaymentController@manage_subscription') }}" class="btn btn-dashboard">Reactivate Your Subscription</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ action('PaymentController@manage_subscription') }}" class="btn btn-ub btn-block">Reactivate Your Subscription</a></div>
			</div>
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{ url('payment/card/update') }}" class="btn btn-dashboard">Update Payment Details</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ url('payment/card/update') }}" class="btn btn-ub btn-block">Update Payment Details</a></div>
			</div>
			
			@else		<!--  user doesn't have a subscription   -->
			
			
			<div class="row">
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-remove-sign" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label">You don't have an active subscription</div>
			</div>
			
			@if($user->test)
			<div class="row">
				<div class="col-xs-1"><h2 class="dashboard-splash">&nbsp;</h2></div>
				<div class="col-xs-11 ">
					<p class="subscription_description">With an "Unlimited Items" monthly subscription, you can post as many items as you want for a whole month and all of your items will automatically renew while your subscription is active.<br /> 
						You'll never have to worry about expiration dates again - well, except for that jug of milk in the back of your fridge... <br />
						<b>Unlimited items AND peace of mind - just for $10/month.</b>
					</p>
				</div>
			</div>
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{ url('payment/subscription/monthly') }}" class="btn btn-dashboard">Purchase A Subscription</a></div>
				
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ url('payment/subscription/monthly') }}" class="btn btn-ub btn-block">Purchase A Subscription</a></div>
			</div>
			@endif
			
			
			@endif
			
		
		
		</div>
		
		
		
		
	</div>
	<!--  end of row -->
	
	<div class="row">&nbsp;</div>
	<div class="row hidden-xs hidden-sm">&nbsp;</div>
	
	<!-- start of row  -->
	<div class="row">
				
		
		
		
		
		<div class="col-md-6 col-lg-5 col-lg-offset-1">
			<h2 class="border-bottom dashboard-title">Location</h2>
			
			<div class="row">
				<div class="col-xs-2"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-map-marker" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 dashboard-label">{{ $user->location->address or "You haven't set a location yet"}}</div>
				
				@if($user->location )
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 pad-map">
					<div class="embed-responsive embed-responsive-16by9">
						
					    <div id="map-canvas" class="embed-responsive-item"></div>
					   
					</div>
				</div>
				
				@endif
			</div>
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{ url('location') }}" class="btn btn-dashboard">Manage Location</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ url('location') }}" class="btn btn-ub btn-block">Manage Location</a></div>
			</div>
		
		</div>
		
		
		
		<div class="row hidden-md hidden-lg">&nbsp;</div>
		
		
		
		<div class="col-md-6 col-lg-5 col-lg-offset-1">
			<h2 class="border-bottom dashboard-title">Contact Preferences</h2>
			
			@if( !$user->contact_email && !$user->contact_phone && !$user->contact_sms)
			<div class="alert alert-danger">
				<strong>Warning!</strong> You have disabled all contact methods. Unless you make at least one contact method visible, renters will not be able to contact you to rent your item!<br>
			</div>
			@endif
			
			<div class="row">
				<?php $color= ($user->contact_email) ? "" : "text-muted";?>
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-envelope {{$color}}" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label {{$color}}">{{ $user->email }}</div>
			</div>	
			
			<div class="row">
				<?php $color= ($user->contact_phone) ? "" : "text-muted";?>
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-earphone {{$color}}" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label {{$color}}">{{ ($user->phone != "") ? $user->phone :"You haven't provided a phone number" }}</div>
			</div>
			
			<div class="row" style="margin-bottom:10px;">
				<?php $color= ($user->contact_sms) ? "" : "text-muted";?>
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-comment {{$color}}" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label {{$color}}">{{ ($user->phone != "") ? $user->phone : "You haven't provided a phone number" }}</div>
			</div>
			
			<div class="row" style="margin-bottom:10px;">
				<?php $color= ($user->contact_notification) ? "" : "text-muted";?>
				<div class="col-xs-2 mobile-stretch"><h2 class="dashboard-splash"><b class="glyphicon glyphicon-bell {{$color}}" aria-hidden="true"></b></h2></div>
				<div class="col-xs-10 mobile-shrink dashboard-label {{$color}}">{{ ($user->contact_notification) ? "Notify me when items are expiring" : "Don't notify me when items are expiring" }}</div>
			</div>
			
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{ url('user/'.$user->id.'/edit')}}" class="btn btn-dashboard">Manage Contact Preferences</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ url('user/'.$user->id.'/edit')}}" class="btn btn-ub btn-block">Manage Contact Details</a></div>
			</div>
			
			<div class="row">	
				<div class="col-xs-8 col-xs-offset-1 hidden-xs hidden-sm"><a href="{{ url('user/password')}}" class="btn btn-dashboard">Change Password</a></div>
				<div class="col-xs-12 hidden-md hidden-lg hidden-xl"><a href="{{ url('user/password')}}" class="btn btn-ub btn-block">Change Password</a></div>
			</div>
		
		</div>
</div>
@endsection
