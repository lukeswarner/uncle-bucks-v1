<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>Uncle Bucks</title>
	
		<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/ub-layout.css?'.date('YmdHi')) }}" rel="stylesheet">
		<link href="{{ asset('/css/ub-footer.css') }}" rel="stylesheet">
	
		@yield('css')
		
		<link href="{{ asset('/css/rounded_corners.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/fonts.css') }}" rel='stylesheet' type='text/css'>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		
		
	</head>
	<body>
	
		<header class="masthead">
			<div class="container">
				
				
				<div class="row hidden-sm hidden-xs">
					<div id="ub-logo" class="col-sm-11">
				    	<div id="tagline">HELPING NEIGHBORS RENT FROM NEIGHBORS</div>
				    	
				      	<a href="{{ url() }}"><img class="img-responsive logo" src="{{ asset('img/logo-web-black.png') }}" /></a>
				      	
				    </div>
				    
				 </div>
				 
				 
		      	<div id="header-border"></div>
		  	</div>
		</header>	  	  	
		
		<!-- Begin Body -->
		<div class="container">
		
			<div class="row">
		  			<div class="col-md-1">
		              
		             	&nbsp;
		              
		              
		      		</div>  
		      		<div class="col-md-11">
		      			<div id="content">
		      			
		      			
		      			
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-7 col-md-offset-2">
							Uncle Bucks uses some pretty cool technology to make our site awesome, but your web browser is too old to take 
					      	advantage of all that goodness! But don't worry- it's really easy to upgrade your browser to the newest version. 
					      	Choose from any of the options below and you'll be set. We'll see you soon.<br /><br />
					      	<div class="row">
					      		<div class="col-md-4"><a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie"><img src="{{ asset('img/Internet-ie-icon.png')}}"/><br />Microsoft Internet Explorer</a></div>
					      		<div class="col-md-4"><a href="http://www.getfirefox.com"><img src="{{ asset('img/Firefox-icon.png')}}"/><br />Mozilla Firefox</a></div>
					      		<div class="col-md-4"><a href="https://www.google.com/chrome"><img src="{{ asset('img/Google-Chrome-icon.png')}}"/><br />Google Chrome</a></div>
					      	</div>
					     </div> 	
			</div>
	
	</div> 
		  	</div>
		</div>
		
		
		<footer class="footer">
	    	<div class="container">
	    		<div class="row">
			      	<div class="col-sm-9 col-xs-12 pull-left">
			      		<ul class="footer-list list-unstyled">
			      			<li><a href="{{url('about')}}">About Uncle Bucks</a></li>
			      			<li><a href="{{url('business-partnerships')}}">Business Partnerships</a></li>
			      			<li><a href="{{url('gives-back')}}">Uncle Bucks Gives Back</a></li>
			      		</ul>
			        </div>
			        
			        <div id="footer-right" class="col-sm-3 col-xs-12">
			        	<p class="text-muted">&copy; Uncle Bucks {{ date('Y') }}</p>
			        </div>
			   
		        </div>
	      	</div>
	    </footer>
	    
	    	
			
		<!-- Scripts -->
		<script src="{{ asset('/js/jquery-2.1.4.min.js') }}"></script>
		<script src="{{ asset('/js/bootstrap-3.3.5.js') }}"></script>
		
		<script>
			$('#sidebar').affix({
		      offset: {
		        top: 168
		      }
			});	
		</script>
		)
		
	</body>
</html>


			