
	<div class="category-container">
	
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		
		  
		  
		  @foreach  ($category_groups as $group)
		  <div class="panel panel-default panel-category">
		    <div role="tab" id="heading-{{ $group->slug }}">
		     
		        <button class="collapsed btn btn-lg btn-category-heading btn-block" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $group->slug }}" aria-expanded="false" aria-controls="collapse-{{ $group->slug }}">
		          {{ $group->name }}
		        </button>

		    </div>
		    <div id="collapse-{{ $group->slug }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $group->slug }}">
		    	@foreach ($group->categories as $categories)
		    	<button class="btn btn-category btn-block" onclick="alert('hello!')">{{ $categories->name }}</button>
		    	@endforeach
		    </div>
		  </div>
		  @endforeach
		  
		</div>
	
	</div>