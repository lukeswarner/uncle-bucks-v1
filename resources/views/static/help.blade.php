@extends('app')


@section('content')
<div class="container-fluid">

	<div class="row">
				
        @include('errors.list')

        {!! Form::open(['url'=>'send_message', 'method' => 'post', 'class'=>'form-horizontal']) !!}
            {!! Form::hidden('message-type', 'help') !!}
    
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Have a question? Find a problem?</h2>
                    <p>Send us a message and we'll do everything we can to respond to you right away.</p>
                </div>
            </div>

            
            <div class="form-group">
                <label class="col-md-3 control-label">Your Email Address</label>
                <div class="col-md-6">
                    {!! Form::email('email', ( !empty($user) ) ? $user->email : null , ['class' => 'form-control', 'placeholder' => 'you@domain.net']) !!}
                </div>
            </div>
            

            <div class="form-group">
                <label class="col-md-3 control-label">Your message</label>
                <div class="col-md-6">
                    {!! Form::textarea('msg', null, ['class' => 'form-control', 'placeholder' => 'Write your message here.']) !!}
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                
                    {!! Form::submit('Send ol\' Uncle Buck a message', ['class' => 'btn btn-ub btn-block']) !!}
                </div>
            </div>
                
            
        {!! Form::close() !!}

	</div>
</div>
@endsection
