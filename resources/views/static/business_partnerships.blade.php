@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-1">
		
			<h1>Business Partnerships</h1>
			<p>
			Uncle Bucks is geared first and foremost to peer to peer rentals. We believe that renting items from neighbors in the community is something unique and simple and are committed to this process. We also value local business and want to be apart of building community involving and partnering with local business. Uncle Bucks has a special section that allows local businesses that offer items for rent to post items. This not only gives you access to a growing population of people seeking to rent items, but is also a form of advertisement in front of an enormous number of faces everyday. We offer a monthly partnership that allows posting of an unlimited number of items in the local business section for a flat monthly fee. The win is two fold. Potential renters who are weary of renting an item from an individual or individuals that are needing a larger quantity of items will seek our a local business to rent from. Also users will see the local business section and see the names and services that your business is offering even if they don't rent today. That name recognition is the real win as they drive by your store and see your brand begin to repeat itself in their lives in a more consistent basis. 
			</p>
		</div>
	
	</div>
	<!-- start of row  -->
	
</div>
@endsection