@extends('app')

@section('content')
<div class="container-fluid">
	<h1>Administration Dashboard</h1>
	
	<!-- start of row  -->
	<div class="row">
	
		<div class="col-md-4">
			<h2>Users</h2>
			
			<a href="#" class="btn btn-danger btn-lg btn-block">Manage Users</a>
			
		</div>
		
		
		<div class="col-md-4">
			<h2>Categories</h2>
			
			<a href="{{ url('category')}}" class="btn btn-ub btn-lg btn-block">Manage Categories</a>
			<a href="{{ url('category/create')}}" class="btn btn-ub btn-lg btn-block">Create Category</a>
			<a href="{{ url('categorygroup/create')}}" class="btn btn-ub btn-lg btn-block">Create Category Group</a>

		</div>
		
	</div>
	<!--  end of row -->
	
	
</div>
@endsection
