@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-7 col-md-offset-2">
			<h3>Uncle Bucks Privacy Policy</h3>
            <p>
                This policy details how data about you is used when you access our websites and services (together, "UB") or interact with us. If we update it, we will revise the date, place notices on UB if changes are material, and/or obtain your consent as required by law.
            </p>
            
            <span class="attention">1. Protecting your privacy</span>
 
            <ul class="about">
                <li>We take precautions to prevent unauthorized access to or misuse of data about you. </li>
                <li>We do not run ads, other than the classifieds posted by our users.</li>
                <li>We do not share your data with third parties for marketing purposes.</li>
                <li>We do not engage in cross-marketing or link-referral programs.</li>
                <li>We do not employ tracking devices for marketing purposes.</li>
                <li>We do not send you unsolicited communications for marketing purposes.</li>
                <li>We do not engage in affiliate marketing (and prohibit it on UB).</li>
                <li>Please review privacy policies of any third party sites linked to from UB.</li>
            </ul>
       
            <span class="attention">2. Data we use to provide/improve our services and/or combat fraud/abuse: </span>

            <ul class="about">
                <li>data you post on or send via UB, and/or send us directly or via other sites </li>
                <li>credit card data, which is transmitted to payment processors via a security protocol (e.g. SSL). </li>
                <li>data you submit or provide (e.g. name, address, email, phone, fax, photos).</li>
                <li>web log data (e.g. web pages viewed, access times, IP address, HTTP headers).</li>
                <li>data collected via cookies (e.g. search data and "favorites" lists).</li>
                <li>data about your device(s) (e.g. screen size, DOM local storage, plugins)</li>
                <li>data from 3rd parties (e.g. phone type, geo-location via Google Maps API or user’s browser).</li>
            </ul>
                
            
            <span class="attention">3. Data we store</span>

            <ul class="about">
                <li>We retain data as needed for our business purposes and/or as required by law.</li>
                <li>We make good faith efforts to store data securely, but can make no guarantees.</li>
                <li>You may access and update certain data about you via your account login. </li>
            </ul>
            
            <span class="attention">4. Circumstances in which we may disclose user data:</span>

            <ul class="about">
                <li>to vendors and service providers (e.g. payment processors) working on our behalf. </li>
                <li>to respond to subpoenas, search warrants, court orders, or other legal process.</li>
                <li>to protect our rights, property, or safety, or that of users of UB or the general public.</li>
                <li>with your consent (e.g. if you authorize us to share data with other users).</li>
                <li>in connection with a merger, bankruptcy, or sale/transfer of assets.</li>
                <li>in aggregate/summary form, where it cannot reasonably be used to identify you.</li>
            </ul>
            
			<p>
            <span class="attention">International Users</span> - By accessing UB or providing us data, you agree we may use and disclose data we collect as described here or as communicated to you, transmit it outside your resident jurisdiction, and store it on servers in the United States. For more information please contact us.
            </p>
		</div>
	</div>	

</div>
@endsection