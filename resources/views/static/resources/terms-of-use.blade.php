@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3>Uncle Bucks Terms Of Use</h3>
<p>
<span class="attention">WELCOME TO UNCLE BUCKS.</span> We hope you find it useful. By accessing our servers, websites, or content therefrom (together, "UB"), you agree to these Terms of Use ("TOU"), last updated November 30, 2015. 
</p>

<p>
<span class="attention">LICENSE.</span> If you are 18 or older, we grant you a limited, revocable, nonexclusive, nonassignable, nonsublicensable license to access UB in compliance with the TOU; unlicensed access is unauthorized. You agree not to license, distribute, make derivative works, display, sell, or "frame" content from UB, excluding content you create and sharing with friends/family. You grant us a perpetual, irrevocable, unlimited, worldwide, fully paid/sublicensable license to use, copy, perform, display, distribute, and make derivative works from content you post. 
</p>

<p>
<span class="attention">USE.</span> You agree not to use or provide software (except for general purpose web browsers and email clients, or software expressly licensed by us) or services that interact or interoperate with UB, e.g. for downloading, uploading, posting, flagging, emailing, search, or mobile use. Robots, spiders, scripts, scrapers, crawlers, etc. are prohibited, as are misleading, unsolicited, unlawful, and/or spam postings/email. You agree not to collect users' personal and/or contact information ("PI"). 
</p>

<p>
<span class="attention">MODERATION.</span> You agree we may moderate UB access and use in our sole discretion, e.g. by blocking (e.g. IP addresses), filtering, deletion, delay, omission, verification, and/or access/account/license termination. You agree (1) not to bypass said moderation, (2) we are not liable for moderating, not moderating, or representations as to moderating, and (3) nothing we say or do waives our right to moderate, or not. All site rules, e.g. UB prohibited items, are incorporated herein. 
</p>

<p>
<span class="attention">SALES.</span> You authorize us to charge your account for UB fees. Unless noted, fees are in US dollars; tax is additional. To the extent permitted by law, fees are nonrefundable, even for posts we remove. We may refuse purchases, which may place a hold on your account. 
</p>

<p>
<span class="attention">DISCLAIMER.</span> MANY JURISDICTIONS HAVE LAWS PROTECTING CONSUMERS AND OTHER CONTRACT PARTIES, LIMITING THEIR ABILITY TO WAIVE CERTAIN RIGHTS AND RESPONSIBILITIES. WE RESPECT SUCH LAWS; NOTHING HEREIN SHALL WAIVE RIGHTS OR RESPONSIBILITIES THAT CANNOT BE WAIVED.<br />
To the extent permitted by law, (1) we make no promise as to UB, its completeness, accuracy, availability, timeliness, propriety, security or reliability; (2) your access and use are at your own risk, and UB is provided "AS IS" and "AS AVAILABLE"; (3) we are not liable for any harm resulting from (a) user content; (b) user conduct, e.g. illegal conduct; (c) your UB use; or (d) our representations; (4) WE AND OUR OFFICERS, DIRECTORS, EMPLOYEES ("UB ENTITIES"), DISCLAIM ALL WARRANTIES & CONDITIONS, EXPRESS OR IMPLIED, OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE, OR NON-INFRINGEMENT; (5) UB ENTITIES ARE NOT LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, OR ANY LOSS (E.G. OF PROFIT, REVENUE, DATA, OR GOODWILL); (6) IN NO EVENT SHALL OUR TOTAL LIABILITY EXCEED $100 OR WHAT YOU PAID US IN THE PAST YEAR. 
</p>

<p>
<span class="attention">CLAIMS.</span> You agree (1) any claim, cause of action or dispute ("Claim") arising out of or related to the TOU or your UB use is governed by Washington ("WA") law regardless of your location or any conflict or choice of law principle; (2) Claims must be resolved exclusively by state or federal court in Seattle, WA (except we may seek injunctive remedy anywhere); (3) to submit to personal jurisdiction of said courts; (4) any Claim must be filed by 1 year after it arose or be forever barred; (5) not to bring or take part in a class action against UB Entities; (6) (except government agencies) to indemnify UB Entities for any damage, loss, and expense (e.g. legal fees) arising from claims related to your UB use; (7) you are liable for TOU breaches by affiliates (e.g. marketers) paid by you, directly or indirectly (e.g. through an affiliate network); and (8) to pay us for breaching or inducing others to breach the "USE" section, not as a penalty, but as a reasonable estimate of our damages (actual damages are often hard to calculate): $0.10 per server request, $1 per post, email, flag, or account created, $1 per item of PI collected, and $1000 per software distribution, capped at $25,000 per day. 
</p>

<p>
<span class="attention">RENTER/BUYER BEWARE.</span> UB assumes absolutely no responsibility for any rentals made through the UB. At any time you transaction a rental you understand without reservation that these transactions are done at your own risk. We do not issue refunds if the product or service rented turns out to not be what you thought it was, or if the other user in your transaction does not hold up to their promises. You should thoroughly investigate any third party before making the decision to transact with them. UB is only the introduction service provider and assumes no responsibilities other than outlined in this user agreement. 
</p>

<p>
<span class="attention">MISC.</span> Users complying with prior written licenses may access UB thereby until authorization is terminated. Otherwise, this is the exclusive and entire agreement between us. If a TOU term is unenforceable, other terms are unaffected. If TOU translations conflict with the English version, English controls. See Privacy Policy for how we collect, use and share data. 
</p> 
		</div>
	</div>	

</div>
@endsection