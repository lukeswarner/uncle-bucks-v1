@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<h3>Uncle Bucks Avoiding Scams</h3>
            <p>
                <span class="attention">Deal locally, face-to-face —follow this one rule and avoid 99% of scam attempts.</span>
            </p>
			<ul class="about">
				<li><span class="attention">Do not extend payment to anyone you have not met in person.</span></li>
				<li><span class="attention">Beware offers involving shipping - </span>deal with locals you can meet in person.</li>
				<li><span class="attention">Never wire funds (e.g. Western Union) - </span>anyone who asks you to is a scammer.</li>
				<li><span class="attention">Don't accept cashier/certified checks or money orders - </span>banks cash fakes, then hold you responsible. </li>
				<li><span class="attention">Transactions are between users only, </span>no third party provides a "guarantee".</li>
				<li><span class="attention">Never give out financial info </span> (bank account, social security, paypal account, etc).</li>
				<li><span class="attention">Do not rent sight-unseen - </span>—that amazing "deal" may not exist.</li>
				<li><span class="attention">Refuse background/credit checks.</span></li>
             </ul>
   
            <p>
                <span class="attention">Who should I notify about fraud or scam attempts?</span><br />
                <span class="attention">United States</span>
            </p>
			<ul class="about">  
                <li><a target="_blank" class="underline" href="http://www.ic3.gov/">Internet Fraud Complaint Center</a></li>
                <li><a target="_blank" class="underline" href="http://www.ftc.gov/video-library/index.php/video/how-to-file-a-complaint-with-the-federal-trade-commission/1402334828001">FTC Video: How to report scams to the FTC</a></li>
                <li><a target="_blank" class="underline" href="https://www.ftccomplaintassistant.gov/">FTC complaint form</a> and hotline: 877-FTC-HELP (877-382-4357)</li>
                <li><a target="_blank" class="underline" href="http://www.ftc.gov/sentinel/military/index.shtml">Consumer Sentinel/Military (for armed service members and families) </a></li>
                <li><a target="_blank" class="underline" href="http://www.siia.net/piracy/report/report.asp">SIIA Software and Content Piracy reporting</a></li>
            </ul>
            
            <p>
                If you are defrauded by someone you met in person, contact your local police department. If you suspect that a Uncle Bucks post may be connected to a scam, please send us the details.
            </p>
		</div>
	</div>	

</div>
@endsection