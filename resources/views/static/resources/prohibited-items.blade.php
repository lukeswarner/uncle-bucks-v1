@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3>Uncle Bucks Prohibited Items</h3>
            <p>
                Users must comply with all applicable laws, the UB terms of use, and all posted site rules.<br />
                Here is a partial list of content prohibited on Uncle Bucks:             
            </p>
			<ul class="about">
				<li>weapons; firearms/guns and components; BB/pellet, stun, and spear guns; etc </li>
				<li>ammunition, clips, cartridges, reloading materials, gunpowder, fireworks, explosive</li>
				<li>recalled items; hazardous materials; body parts/fluids; unsanitized bedding/clothing </li>
				<li>prescription drugs, medical devices; controlled substances and related items </li>
				<li>alcohol or tobacco; unpackaged or adulterated food or cosmetics </li>
				<li>child pornography; bestiality; offers or solicitation of illegal prostitution </li>
				<li>pet rentals, animal parts, stud service</li>
				<li>ivory; endangered, imperiled and/or protected species and any parts thereof </li>
				<li>false, misleading, deceptive, or fraudulent content; bait and switch; keyword spam</li>
				<li>offensive, obscene, defamatory, threatening, or malicious postings or emai</li>
				<li>anyone’s personal, identifying, confidential or proprietary information </li>
				<li>food stamps, WIC vouchers, SNAP or WIC goods, governmental assistanc</li>
				<li>stolen property, property with serial number removed/altered, burglary tools, etc</li>
				<li>ID cards, licenses, police insignia, government documents, birth certificates, etc </li>
				<li>US military items not demilitarized in accord with Defense Department policy </li>
				<li>counterfeit, replica, or pirated items; tickets or gift cards that restrict transfer </li>
				<li>lottery or raffle tickets, sweepstakes entries, slot machines, gambling items </li>
				<li>spam; miscategorized, overposted, cross-posted, or nonlocal content </li>
				<li>postings or email the primary purpose of which is to drive traffic to a website</li>
				<li>postings or email offering, promoting, or linking to unsolicited products or services</li>
				<li>affiliate marketing; network, or multi-level marketing; pyramid schemes</li>
				<li>any good, service, or content that violates the law or legal rights of others</li>
			</ul>
            <p>
                Please don't use UB for these purposes, and flag anyone else you see doing so.<br />
                Thanks for helping keep Uncle Bucks safe and useful for everyone. 
            </p>
            
		</div>
	</div>	

</div>
@endsection