@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<h3>Uncle Bucks Personal Safety</h3>
            <p>
                The overwhelming majority of Uncle Bucks users are trustworthy and well-intentioned. With billions of human interactions facilitated, the incidence of violent crime is extremely low. Nevertheless, please take the same common sense precautions online as you would offline
            </p>
            <p>When meeting someone for the first time, please remember to: </p>
			<ul class="about">
				<li>Insist on a public meeting place like a cafe.</li>
				<li>Do not meet in a secluded place, or invite strangers into your home.</li>
				<li>Be especially careful renting high value items.</li>
				<li>Consider making high-value exchanges at your local police station.</li>
				<li>Tell a friend or family member where you're going.</li>
				<li>Take your cell phone along if you have one.</li>
				<li>Consider having a friend accompany you.</li>
				<li>Trust your instincts.</li>
             </ul>
   
            <p>
                Taking these simple precautions helps make Uncle Bucks safer for everyone. For more information about personal safety online, check out these resources: 
            </p>
			<ul class="about">  
                <li><a target="_blank" class="underline" href="http://www.staysafeonline.org/">http://www.staysafeonline.org</a></li>
                <li><a target="_blank" class="underline" href="http://www.onguardonline.gov/">http://www.onguardonline.gov</a></li>
                <li><a target="_blank" class="underline" href="http://getsafeonline.org">http://getsafeonline.org</a></li>
                <li><a target="_blank" class="underline" href="http://wiredsafety.org">http://wiredsafety.org</a></li>
            </ul>
		</div>
	</div>	

</div>
@endsection