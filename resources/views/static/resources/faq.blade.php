@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<h3>Frequently Asked Questions</h3>
            <p>
                <span class="attention">Q: How do I prevent someone from stealing my stuff?</span><br />
                A: Uncle Buck recommends using a card-reader to take a deposit which you can easily refund when your item is returned. <br />Check out our <a class="underline" target="_blank" href="{{ url('resources/ub-resources') }}">Resources</a> page for more information.
            </p>
            
			<p>
                <span class="attention">Q: What are my options to pay for items I want to post onto the site? </span><br />
                A: There are two different options for payment. You can pay to post individually for $1 each or you can pay a subscription of $10/month to post an unlimited amount of items as well as set them to auto renew.
            </p>
            
			<p>
                <span class="attention">Q: How long are my items listed as available for rent on unclebucks.co? </span><br />
                A: Items posted on Uncle Bucks are displayed for 1 month. After the 1 month the items will need to be renewed, unless you are on the subscription plan. If you have a subscription, all of your items will automatically renew each month.
            </p>
            
			<p>
                <span class="attention">Q: What if the owner of the items says I damaged the item and I didn't? </span><br />
                A: We recommend having a contract that is signed by the owner and renter as well as take images of the item before it is rented. That way everyone knows what the terms and expectations and the item's condition is documented. 
            </p>
            
			<p>
                <span class="attention">Q: Where do I ﬁnd an example of a rental contract to use?</span><br />
                A: There are many places to ﬁnd example contracts, user agreements, etc. online. If you jump over to our <a class="underline" target="_blank" href="{{ url('resources/ub-resources') }}">Resources</a> page there will be more information.
            </p>
            
			<p>
                <span class="attention">Q: When will Uncle Bucks be available in my area? </span><br />
                A: Right now Uncle Bucks is only availabe in the Bend, Oregon area. Uncle Buck’s truck isn’t the fastest, but we are trying to get him to every city and town around. We would love to hear where you're from. Drop us a line and we will put you on the map! 
            </p>
            
			<p>
                <span class="attention">Q: Why should I rent instead of buy?</span><br />
                A: Not needing to store large or bulky items, save money by only renting the few times you need the item, being apart of a community with your neighbors, try before you commit to buy, sharing items with neighbors instead of consuming more products just to name a few.
            </p>
            
			<p>
                <span class="attention">Q: How long did it take for Uncle Buck to grow out his beard? </span><br />
                A: The time it took for UB to grow his beard out has always been a mystery. The truth is nobody remembers him not having a beard.
            </p>
            
			<p>
                <span class="attention">Q: Is there a way I can get one of those totally awesome hats that Uncle Buck wears?</span><br />
                A: We don’t have an online store or anything like that that sells Uncle Bucks gear, but if you contact us, we may be able to ﬁgure something out.
            </p>
            
			<p>
                <span class="attention">Q: I don’t see a category that ﬁts the item I would like to post. What should I do?</span><br />
                A: Let us know what the item is and the category you think we are missing. There is also a general category for items that just don’t seem to ﬁt.
            </p>
            
			<p>
                <span class="attention">Q: Should I give out my address to have people meet to rent my stuff? </span><br />
                A: Uncle Buck recommends meeting in a public place that has other people around to help ensure a safe transaction. <br />Check out our <a class="underline" target="_blank" href="{{ url('resources/personal-safety') }}">Personal Safety</a> page for more information.
            </p>
            
			<p>
                <span class="attention">Q: What items can I rent on Uncle Bucks?</span><br />
                A: The possibilities are endless. Everything from a bicycle to a kitchen mixer, from camping supplies to a stroller. <br />There are only a few exceptions which can be found in our <a class="underline" target="_blank" href="{{ url('resources/prohibited-items') }}">Prohibited Items</a> page.
            </p>
            
        </div>
    </div>
           
	<div class="row">			
        @include('errors.list')

        {!! Form::open(['url'=>'send_message', 'method' => 'post', 'class'=>'form-horizontal']) !!}
            {!! Form::hidden('message-type', 'question') !!}
            
            <div class="form-group">
                <div class="col-md-11 col-md-offset-1 col-lg-10 col-lg-offset-2">
                    <h2>What's that? You have a question we forgot to cover?</h2>
                    <p>Send us a message and we'll get back to you right away.</p>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-offset-1 col-lg-2 control-label">Your&nbsp;email</label>
                <div class="col-md-offset-1 col-lg-offset-0 col-lg-6">
                    {!! Form::email('email', ( !empty($user) ) ? $user->email : null , ['class' => 'form-control', 'placeholder' => 'you@domain.net']) !!}
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-offset-1 col-lg-2 control-label">Your message</label>
                <div class="col-md-offset-1 col-lg-offset-0 col-lg-6">
                    {!! Form::textarea('msg', null, ['class' => 'form-control', 'placeholder' => 'Write your message here.']) !!}
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-offset-1 col-lg-6 col-lg-offset-3">
                
                    {!! Form::submit('Send ol\' Uncle Buck a message', ['class' => 'btn btn-ub btn-block']) !!}
                </div>
            </div>
                
            
        {!! Form::close() !!}

	</div>	

</div>
@endsection