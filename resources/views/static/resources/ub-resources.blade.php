@extends('app')

@section('content')
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3>Resources</h3>
            <p>
                It is a great thing to join the growing industry of neighbors renting from neighbors. There are also risks involved with meeting other people for the first time and allowing those people to use your items. Uncle Bucks recommends the use of contracts, waivers, security deposits and clear expectations. It is also important to know that items may have typical wear and tear. Below is a list of ways to be sure everyone involved is protected and aware of all expectations.
            </p>
			<ul class="about">
				<li>Set security deposits that are high enough to cover loss or damage to the item</li>
                <li>Take pictures of items before they are rented and when they are returned</li>
                <li>Have both parties sign and date a description of any damage that is preexisting</li>
                <li>Use contracts to set clear expectations on terms and condition of how and when items should be returned.<br />
                    [There are a number of example contracts online from places such as <a class="underline" target="_blank" href="http://www.printablecontracts.com">www.printablecontracts.com</a>]</li>
                <li>Consider signing a waiver or release to protect against the possibility of injury</li>
                <li>Use a free card reader from third party companies that attach to your smart phone for damage deposits</li>
                <li>Ask for background information (such as driver’s license number, employers, etc.) to have some extra contact information</li>
                <li>Always meet in a public place</li>
                <li>For more ideas, check out these pages on <a class="underline" href="{{url('resources/avoiding-scams')}}">Avoiding Scams</a> and <a class="underline" href="{{url('resources/personal-safety')}}">Personal Safety</a></li>
			</ul>
		</div>
	</div>	

</div>
@endsection