@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-1">
		
			<h1>Uncle Bucks Gives Back</h1>
			<p>
			Uncle Bucks is committed to the local community. Giving back is something that is at the very core of Uncle Bucks and the very reason that peer to peer renting was so appealing. Neighbor helping neighbor. Not needing to always buy in excess when it isn't needed, and borrowing and lending to those in need at reasonable rates and terms. These are all things that we at Uncle Bucks stand for. Because of this, we are committed to taking care of the orphans. A percentage of all income will be given back to the benefit of foster and adoption. We also encourage our business partners to look into ways they too can give time, energy and resources to taking care of the kids in our communities that need care. 
			</p>
		</div>
	
	</div>

	
	
	<!-- start of row  -->
	
</div>
@endsection