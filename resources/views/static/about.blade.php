@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<h1 class="text-center">Helping Neighbors Rent From Neighbors</h1>
		<div class="col-md-8 col-md-offset-2">
			<img class="img-responsive" src="{{asset('img/about_comic.png')}}" />
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3>Tid Bits</h3>
			<ul class="about">
				<li>Uncle Bucks is a place for neighbors to post items they would like to offer to other neighbors for rent</li>
				<li>The process of renting from neighbors enables one to try before you buy, rent so you don't have to store and simply use items that you wouldn't normally use very often</li>
				<li>The process of renting out your personal items allows you to generate some income on items that typically aren't being used - as well as be a good neighbor</li>
				<li>Uncle Bucks was created because there was not a place to rent items from neighbors</li>
				<li>Uncle Bucks gives back to the community by donating a percentage of profits to support foster and adoptive families in local communities</li>
				<li>Uncle Bucks was a dream that started in 2012. Until now, renting items has always been a frustrating experience</li>
				<li>Currently Uncle Bucks is in Central Oregon, but plans to expand to other cities and towns quickly</li>
				<li>Uncle Bucks has team members in Bend, OR, Seattle, WA and Belize.</li>
			</ul>
		</div>
	</div>	


	<div class="row  hidden-sm hidden-xs">
		<div class="col-md-12">
			<div class="about-resources">
			<a class="btn btn-about" href="{{ url('resources/ub-resources') }}">RESOURCES</a> 
			| <a class="btn btn-about" href="{{ url('resources/terms-of-use') }}">TERMS OF USE</a>
			| <a class="btn btn-about" href="{{ url('resources/privacy-policy') }}">PRIVACY POLICY</a>
			| <a class="btn btn-about" href="{{ url('resources/personal-safety') }}">PERSONAL SAFETY</a>
			| <a class="btn btn-about" href="{{ url('resources/prohibited-items') }}">PROHIBITED ITEMS</a>
			| <a class="btn btn-about" href="{{ url('resources/avoiding-scams') }}">AVOIDING SCAMS</a>
			</div>
		</div>
	</div>
	
	
	
	<div class="row hidden-md hidden-lg hidden-xl">
		<div class="col-md-12 about-resources">
			<a class="btn btn-about" href="{{ url('resources/ub-resources') }}">RESOURCES</a>
		</div>
		<div class="col-md-12 about-resources">	
			<a class="btn btn-about" href="{{ url('resources/terms-of-use') }}">TERMS OF USE</a>
		</div>
		<div class="col-md-12 about-resources">	
			<a class="btn btn-about" href="{{ url('resources/privacy-policy') }}">PRIVACY POLICY</a>
		</div>
		<div class="col-md-12 about-resources">	
			<a class="btn btn-about" href="{{ url('resources/personal-safety') }}">PERSONAL SAFETY</a>
		</div>
		<div class="col-md-12 about-resources">	
			<a class="btn btn-about" href="{{ url('resources/prohibited-items') }}">PROHIBITED ITEMS</a>
		</div>
		<div class="col-md-12 about-resources">	
			<a class="btn btn-about" href="{{ url('resources/avoiding-scams') }}">AVOIDING SCAMS</a>
		</div>
	</div>
	<!-- start of row  -->
	
</div>
@endsection