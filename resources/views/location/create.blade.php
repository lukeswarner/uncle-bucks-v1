@extends('app')


<?php 
/**
 * @issues
 * UB-131 Location [2015-11-23]
 */
?>


@section('scripts')
	@include('scripts.google-maps-api')
	@include('scripts.location-detect', ['type'=>'user', 'id' => $user->id])
@endsection



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container-fluid">

	<div class="row">
		<div id="message-row" class="hidden col-md-12"><h3 id="message"></h3></div>
	</div>
		
		
	<div id="explanation-row" class="row">
		

					<div class="col-md-10 col-md-offset-1">
						<h2>Set your location</h2>
						
						<p>
							Your location is used to find items closest to you and to help your neighbors find the items you have posted for rent. You can set your location using either one two options.
						</p>
						<p>
							We can try to detect your location automatically, or you can choose a location by entering an address, intersection, neighborhood, or landmark.
						</p>
						<p>
							If you don't get the results you expect, go ahead and try a different method. 
						</p>
						<p>
							<br /><br />
						</p>
					</div>
	</div>
	
	<div id="detect-row" class="row">
						
						
					@include('errors.list')

					
					
					<div id="message"></div>
					
						
					<div class="col-lg-4 col-lg-offset-1 col-md-10 col-md-offset-1">
						<div class="form-group">
							<input style="margin-top:15px;" type="button" class="btn btn-ub btn-block" value="Detect my Location" onclick="detectLocation()">
						</div>
					</div>
					
					<div class="col-lg-1 col-md-12" style="padding:0;">
						<h2 class="text-center">-&nbsp;OR&nbsp;-</h2>
						<h4 class="hidden-lg">&nbsp;</h4>
					</div>
					
					<div class="col-lg-4 col-lg-offset-0 col-md-10 col-md-offset-1">
						<div class="form-group">
							{!! Form::text('address', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'Enter an address, intersection, neighborhood...']) !!}
						</div>
						<div class="form-group">
							<input type="button" class="btn btn-ub btn-block" value="Find Location by Address" onclick="codeAddress()">
						</div>
					</div>
						
						
					
			
			<!-- 
			<p>
				(Hey, why do you want to know where I am?)
				Uncle Bucks helps neighbors rent from each other, but how can we help you find your neighbors if we don't know where you are?
				We use your location to help you find items that are close to you, and to help your neighbors find the items you have available for rent. 
				That's it, no ulterior motives or sneaky behavior. We'll never use your information for any other purposes. 
			</p>
			-->
	</div>
	
	<div id="map-row" class="row hidden">
		<div class="col-lg-5 col-xs-10 col-xs-offset-1">
			<div class="embed-responsive embed-responsive-16by9">
						
			    <div id="map-canvas" class="embed-responsive-item"></div>
			   
			</div>
		</div>
		<div class="col-lg-3 col-md-10 col-md-offset-1">
			<h3 id="address-results"></h3>
			{!! Form::open(['url'=>'location', 'method' => 'post', 'class'=>'form-horizontal']) !!}
				{!! Form::hidden('new-lat','', ['id' => 'new-lat']) !!}
				{!! Form::hidden('new-lng','', ['id' => 'new-lng']) !!}
				{!! Form::hidden('new-address', '', ['id' => 'new-address']) !!}
				{!! Form::hidden('type', 'user') !!}
				{!! Form::hidden('id', $user->id) !!}
				{!! Form::hidden('redirect_url', $redirect_url) !!}
				
				
				<input style="margin-top:15px;" type="submit" class="btn btn-ub btn-block" value="Use this Location">
				
				<input style="margin-top:15px;" type="button" class="btn btn-ub btn-block" value="Choose Another Location" onclick="resetView()">
				
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
