@extends('app')


<?php 
/**
 * @issues
 * 
 */
?>

 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
 <script>
	 var map;
	 function initialize() {
	   geocoder = new google.maps.Geocoder();
	   var latlng = new google.maps.LatLng({{ $lat }}, {{ $lng }});
	   var mapOptions = {
	     zoom: 8,
	     center: latlng
	   }
	   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	   var marker = new google.maps.Marker({
	          map: map,
	          position: results[0].geometry.location
	      });
	 }


	 google.maps.event.addDomListener(window, 'load', initialize);
 </script>

@section('content')
<div class="container-fluid">
	<div id="map-canvas"></div>
</div>
@endsection