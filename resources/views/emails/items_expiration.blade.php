	@extends('emails.template')

	
	<?php 
		if($expired_items->count() == 1){
			$exp_header= "WELL SHOOT! One of your Uncle Bucks items has EXPIRED!";
			$hdr_class= "alert-bad";
		}
		else if($expired_items->count() > 1){
			$exp_header= "WELL SHOOT! A few of your Uncle Bucks items have EXPIRED!";
			$hdr_class= "alert-bad";
		}
		else if($expiring_items->count() == 1){
			$exp_header= "WELL SHOOT!  One of your Uncle Bucks items is EXPIRING SOON!";
			$hdr_class= "alert-warning";
		}
		else if($expiring_items->count() > 1){
			$exp_header= "WELL SHOOT! A few of your Uncle Bucks items are EXPIRING SOON!";
			$hdr_class= "alert-warning";
		}
	?>
					
					
	@section('content')
		<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="content-block">
							<img style="padding-left: 20px;" src="{{asset('img/logo-web-black.png')}}" alt="Uncle Bucks logo"/>
						</td>
					</tr>
					
					
					<tr>
						<td style="{{$hdr_class}}">
							<h3>{{ $exp_header }}</h3>
						</td>
					</tr>
					
					
					
					<tr>
						<td class="content-wrap">
							<table width="100%" cellpadding="0" cellspacing="0">
							
							@if( $expired_items->count() > 0 )
								<tr>
									<td colspan="2" class="alert alert-bad">
										<h3>Expired Items</h3>
									</td>
								</tr>
					
								@foreach($expired_items as $item)
								<tr>
									<td style="width:120px; max-width:120px;">
										<img src="{{ url($item->thumbnail) }}" class="item-photo"/>
									</td>
									<td>
										<a class="item-title" href="{{ url('item/'.$item->id) }}"><h2 >{{$item->title}}</h2></a>
										Expired: <span class="ub-text">{{$item->expires_at->toFormattedDateString()}}</span>
									</td>
								</tr>
								@endforeach
							@endif
							
							
							@if( $expiring_items->count() > 0 )
								<tr><td colspan="2">&nbsp;</td></tr>
								<tr>
									<td colspan="2" class="alert alert-warning">
										<h3>Expiring Items</h3>
									</td>
								</tr>
								
								@foreach($expiring_items as $item)
								<tr>
									<td style="width:120px; max-width:120px;">
										<div class="item-photo">
											<img src="{{ url($item->thumbnail) }}"/>
										</div>
									</td>
									<td>
										<a class="item-title" href="{{ url('item/'.$item->id) }}"><h2 >{{$item->title}}</h2></a>
										Expires: <span class="ub-text">{{$item->expires_at->toFormattedDateString()}}</span>
									</td>
								</tr>
								@endforeach
							@endif
							
							</table>
						</td>
					</tr>
					
					
				
					
					
					<tr>
						<td class="content-wrap" style="padding-top:0;">
							<table width="100%" cellpadding="0" cellspacing="0">
								
								<tr>
									<td class="content-block">
										To renew your items, scoot right over to Uncle Bucks, login to your account, and click "Manage Items". Or, just click the button below. You can always set your item to auto renew to you don't have to worry about your items expiring. 

									</td>
								</tr>
								<tr>
									<td class="content-block">
										<a href="http://unclebucks.co/auth/login" class="btn btn-ub btn-block">Manage My Uncle Bucks Items</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table width="100%">
						<tr>
							<td class="aligncenter content-block">This is a courtesy message regarding items you posted at Uncle Bucks.<br /> If you no longer want to receive these messages you can change your contact preferences by logging into your <a href="http://unclebucks.co/auth/login">Uncle Bucks Account</a>. </td>
						</tr>
					</table>
				</div></div>
		@endsection