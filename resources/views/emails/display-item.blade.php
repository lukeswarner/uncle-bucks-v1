                              <!-- display item -->
                              <tr>
                                    <td>
                                        <!-- image -->
                                        <table width="100" align="left" border="0" cellpadding="0" cellspacing="0" class="h">
                                           <tbody>
                                              <tr>
                                                 <td align="right">
                                                    <div class="imgpop">
                                                        <img src="{{ url($item->thumbnail) }}" width="100px" height="100px" alt="" border="0"  style="display:block; border:none; outline:none; text-decoration:none;" class="">
                                                    </div>
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                        <!-- end of image -->
                                    </td>
                                    <td>
                                        <table width="20" align="left" border="0" cellpadding="0" cellspacing="0" class="">
                                           <tbody>
                                              <tr>
                                                 <td align="left">&nbsp;
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                   </td>
                                  <td>
                                        <!-- title and expiration -->
                                        <table width="70%" cellpadding="0" cellspacing="0" border="0" align="right" class="">
                                            <tbody>
                                                <tr>
                                                    <th align="left" valign="bottom" height="24" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #000000" st-content="viewonline">
                                                        {{$item->title}}
                                                     </th>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="bottom" height="24" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #797979;" st-content="viewonline">
                                                        {{$expiration_text}} {{$item->expires_at->toFormattedDateString()}}
                                                     </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- end of title and expiration -->
                                    </td>
                                </tr>
                              
                                <!-- spacer -->
                                <tr>
                                    <td width="100%">
                                        <table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="">
                                            <tr>
                                                <td width="100%" height="10"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                
                                
                                
                                
                                