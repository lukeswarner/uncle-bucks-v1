	@extends('emails.template')

	@section('content')
		<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="content-block">
							<img style="padding-left: 20px;" src="{{asset('img/logo-web-black.png')}}" alt="Uncle Bucks logo"/>
						</td>
					</tr>
					<tr>
						<td class="alert alert-ub">
							<h3>Welcome to Uncle Bucks: Helping Neighbors Rent From Neighbors</h3>
						</td>
					</tr>
					<tr>
						<td class="content-wrap">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="content-block">
										A big rootin' tootin' thank you for creating an account with Uncle Bucks. Now you are ready to post items for your neighbors to rent! 
									</td>
								</tr>
								<tr>
									<td class="content-block">
										Each item you post will be listed on Uncle Bucks for 1 month, but you can always set your items to AutoRenew. You can easily manage your items and contact preferences at any time! Just login to <a href="http://unclebucks.co/auth/login">Uncle Bucks</a> and clicking the "My Account" button.
									</td>
								</tr>
								<tr>
									<td class="content-block">
										<a href="http://unclebucks.co/auth/login" class="btn btn-ub">Manage My Uncle Bucks Account</a>
									</td>
								</tr>
								<tr>
									<td class="content-block">
										We're happy you've joined the Uncle Bucks neighborhood. Don't forget to invite your family and friends. Everybody is welcome here!
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table width="100%">
						<tr>
							<td class="aligncenter content-block">You received this email because an account was just created at UncleBucks.co with this email address. If you did not create this acount, or you think this email was sent in error, please contact us at <a href="mailto:info@unclecbucks.co">info@unclebucks.co</a> and let us know how we can help you. </td>
						</tr>
					</table>
				</div></div>
		@endsection