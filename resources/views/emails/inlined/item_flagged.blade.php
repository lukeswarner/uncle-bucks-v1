	@extends('emails.inlined.template')

	@section('content')	
	
			<div class="content" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
				<table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
					<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
						<td class="content-block" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align: center; valign:top;">
							<img style="padding-left: 20px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; max-width: 100%; margin: auto;" src="http://unclebucks.co/img/logo-email.jpg" alt="Uncle Bucks" />
						</td>
					</tr>
					
					
					<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
						<td class="alert alert-bad" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; margin: 0; padding: 12px;" align="center" bgcolor="{{ $bg_color }}" valign="top">
							<h3 style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 16px !important; color: #ffffff; background-color: {{ $bg_color }}; line-height: 1.2em; font-weight: 800 !important; margin: 20px 0 5px;">{{ $exp_header }}</h3>
						</td>
					</tr>
					
					
					
					<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin:0;">
						<td class="content-wrap" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 10px;" valign="top">
							<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
							
							@if( $expired_items->count() > 0 )
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
									<td colspan="2" class="alert alert-bad" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #D0021B; margin: 0; padding: 12px;" align="center" bgcolor="#D0021B" valign="top">
										<h3 style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 16px !important; color: #ffffff; line-height: 1.2em; font-weight: 800 !important; margin: 12px 0 5px;">Expired Items</h3>
									</td>
								</tr>
					
								@foreach($expired_items as $item)
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0; padding-bottom: 8px;">
									<td style="width: 120px; max-width: 120px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
										<div class="item-photo" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; width: 100px; height: 100px; overflow: hidden; margin: 0;">
											<img src="{{ url($item->photo_xs) }}" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; max-width: 600%; width: 600%; margin: 0;" />
										</div>
									</td>
									<td style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; margin-left:15px;" valign="top">
										<a class="item-title" href="http://unclebucks.co/item/1" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; color: #000000; text-decoration: none; margin: 0;"><h2 style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 18px !important; color: #000; line-height: 1.0em; font-weight: 800 !important; margin: 0;"  valign="top">{{ $item->title }}</h2></a>
										Expired: <span class="ub-text" style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 18px; color: #000; line-height: 1.0em; font-weight: 400; margin:0;">{{$item->expires_at->toFormattedDateString()}}</span>
									</td>
								</tr>
								@endforeach
								
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;"><td colspan="2" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"> </td></tr>
							@endif	
							
							
							@if( $expiring_items->count() > 0 )	
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
									<td colspan="2" class="alert alert-warning" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #FF9F00; margin: 0; padding: 12px;" align="center" bgcolor="#FF9F00" valign="top">
										<h3 style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 16px !important; color: #ffffff; line-height: 1.2em; font-weight: 800 !important; margin: 12px 0 5px;">Expiring Items</h3>
									</td>
								</tr>
								
								@foreach($expiring_items as $item)
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin:0;  padding-bottom: 8px;">
									<td style="width: 120px; max-width: 120px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
										<div class="item-photo" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; width: 100px; height: 100px; overflow: hidden; margin: 0;">
											<img src="{{ url($item->photo_xs) }}" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; max-width: 60%; width: 60%; margin: 0;" />
										</div>
									</td>
									<td style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;  margin-left:15px;" valign="top">
										<a class="item-title" href="http://unclebucks.co/item/1" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; color: #000000; text-decoration: none; margin: 0;"><h2 style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 18px !important; color: #000; line-height: 1.0em; font-weight: 800 !important; margin:0;"  valign="top">{{ $item->title }}</h2></a>
										Expires: <span class="ub-text" style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 18px; color: #000; line-height: 1.0em; font-weight: 400; margin:0;">{{$item->expires_at->toFormattedDateString()}}</span>
									</td>
								</tr>
								@endforeach
							@endif								
																						
							</table>
						</td>
					</tr>
					
					
				
					
					
					<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
						<td class="content-wrap" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 10px;" valign="top">
							<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
								
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
									<td class="content-block" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										To renew your items, scoot right over to Uncle Bucks, login to your account, and click "Manage Items". Or, just click the button below. You can always set your item to auto renew to you don't have to worry about your items expiring. 

									</td>
								</tr>
								<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
									<td class="content-block" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										<a href="http://unclebucks.co/auth/login" class="btn btn-ub btn-block" style="font-family: 'Eurostile', sans-serif'; box-sizing: border-box; font-size: 16pt; color: #000000; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline; border-radius: 0px;  background-color: #ffffff; margin: 0 0 8px; padding: 6px 12px; border: 2px solid #000000;">Manage My Uncle Bucks Items</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
					<table width="100%" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
						<tr style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;">
							<td class="aligncenter content-block" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">This is a courtesy message regarding items you posted at Uncle Bucks.<br style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 14px; margin: 0;" /> If you no longer want to receive these messages you can change your contact preferences by logging into your <a href="http://unclebucks.co/auth/login" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif'; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">Uncle Bucks Account</a>. </td>
						</tr>
					</table>
				</div></div>
			@endsection