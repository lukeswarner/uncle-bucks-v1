        <!-- start of call to action -->
        <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;"><tbody><tr><td style="border-collapse: collapse;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><!-- Spacing --><tr><td height="20" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --><tr><td style="border-collapse: collapse;">
                                    <table width="560" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td style="border-collapse: collapse;">
                                                
                                                <!-- start of right column -->
                                                <table width="560" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td height="15" align="center" style="font-size: 1px; line-height: 1px; border-collapse: collapse;"> </td>
                                                        </tr><!-- button --><tr><td style="border-collapse: collapse;">
                                                            <table width="100%" height="60" bgcolor="#ffffff" align="center" valign="middle" border="2" border-color="#000000;" cellpadding="0" cellspacing="0" style="border-radius: 3px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" class="tablet-button"><tbody><tr><td height="30" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 24px; color: #ffffff; text-align: center; line-height: 30px; border-collapse: collapse;" class="tablet-button">
                                                                        <a style="color: #000000; text-align: center; text-decoration: none !important;" href="{{ $button_url }}">{{ $button_text; }}</a>
                                                                     </td>
                                                                  </tr></tbody></table></td>
                                                      </tr><!-- end of button --><tr><td height="15" align="center" style="font-size: 1px; line-height: 1px; border-collapse: collapse;"> </td>
                                                    </tr></tbody></table><!-- end of right column --></td>
                                          </tr></tbody></table></td>
                              </tr><!-- Spacing --><tr><td height="20" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --></tbody></table></td>
                  </tr></tbody></table></td>
      </tr></tbody></table>
      <!-- end of call to action -->