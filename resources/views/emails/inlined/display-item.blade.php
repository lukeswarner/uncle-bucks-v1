
                                <!-- print  items --><!-- display item --><tr><td style="border-collapse: collapse;">
                                        <!-- image -->
                                        <table width="100" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td align="right" style="border-collapse: collapse;">
                                                    <div class="imgpop">
                                                        <img src="{{ url($item->thumbnail) }}" width="100px" height="100px" alt="" border="0" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none;" class="bannerbig" /></div>
                                                 </td>
                                              </tr></tbody></table><!-- end of image --><table width="20" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td align="left" style="border-collapse: collapse;"> 
                                                 </td>
                                              </tr></tbody></table><!-- title and expiration --><table width="470" cellpadding="0" cellspacing="0" border="0" align="right" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><th align="left" valign="bottom" height="24" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #000000;" st-content="viewonline">
                                                        {{ $item->title }}
                                                     </th>
                                                </tr><tr><td align="left" valign="bottom" height="24" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #797979; border-collapse: collapse;" st-content="viewonline">
                                                        <p>{!! $text !!} </p>
                                                     </td>
                                                </tr></tbody></table><!-- end of title and expiration --></td>
                                </tr><!-- spacer --><tr><td width="100%" style="border-collapse: collapse;">
                                        <table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr><td width="100%" height="10" style="border-collapse: collapse;"></td>
                                            </tr></table></td>
                                </tr>
                                <!-- end items -->