<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Uncle Bucks</title>
  </head>
  <body style="width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0;"><style type="text/css">
@media only screen and (max-width: 640px) {
  a[href^="tel"] {
    text-decoration: none; color: #33b9ff; pointer-events: none; cursor: default;
  }
  a[href^="sms"] {
    text-decoration: none; color: #33b9ff; pointer-events: none; cursor: default;
  }
  .mobile_link a[href^="tel"] {
    text-decoration: default; color: #33b9ff !important; pointer-events: auto; cursor: default;
  }
  .mobile_link a[href^="sms"] {
    text-decoration: default; color: #33b9ff !important; pointer-events: auto; cursor: default;
  }
  table[class=devicewidth] {
    width: 440px !important; text-align: center !important;
  }
  table[class=devicewidthinner] {
    width: 420px !important; text-align: center !important;
  }
  table[class=mainsmall1] {
    float: left !important;
  }
  table[class=mainsmall2] {
    float: right !important;
  }
  table[class=banner-gap] {
    display: none !important;
  }
  img[class="bannerbig"] {
    width: 440px !important; height: 371px !important;
  }
  img[class="spacinglines"] {
    width: 420px !important;
  }
}
@media only screen and (max-width: 480px) {
  a[href^="tel"] {
    text-decoration: none; color: #33b9ff; pointer-events: none; cursor: default;
  }
  a[href^="sms"] {
    text-decoration: none; color: #33b9ff; pointer-events: none; cursor: default;
  }
  .mobile_link a[href^="tel"] {
    text-decoration: default; color: #33b9ff !important; pointer-events: auto; cursor: default;
  }
  .mobile_link a[href^="sms"] {
    text-decoration: default; color: #33b9ff !important; pointer-events: auto; cursor: default;
  }
  table[class=devicewidth] {
    width: 280px !important; text-align: center !important;
  }
  table[class=devicewidthinner] {
    width: 260px !important; text-align: center !important;
  }
  table[class=mainsmall1] {
    float: left !important; width: 120px !important; height: 90px !important;
  }
  table[class=mainsmall2] {
    float: right !important; width: 120px !important; height: 90px !important;
  }
  img[class=mainsmall1] {
    width: 120px !important; height: 90px !important;
  }
  img[class=mainsmall2] {
    width: 120px !important; height: 90px !important;
  }
  table[class=banner-gap] {
    display: none !important;
  }
  img[class="bannerbig"] {
    width: 280px !important; height: 236px !important;
  }
  img[class="spacinglines"] {
    width: 260px !important;
  }
}
</style>

<!-- Start of preheader -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;"><tbody><tr><td style="border-collapse: collapse;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><!-- Spacing --><tr><td width="100%" height="10" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --><tr><td style="border-collapse: collapse;">
                                    
                                 </td>
                              </tr><!-- Spacing --><tr><td width="100%" height="10" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --></tbody></table></td>
                  </tr></tbody></table></td>
      </tr></tbody></table><!-- End of preheader --><!-- Start of header --><table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;"><tbody><tr><td style="border-collapse: collapse;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><!-- Spacing --><tr><td width="100%" height="10" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --><tr><td style="border-collapse: collapse;">
                                    <!-- logo -->
                                    <table width="251" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="252" height="66" align="left" style="border-collapse: collapse;">
                                                <div class="imgpop">
                                                   <a target="_blank" href="http://unclebucks.co" style="color: #33b9ff; text-decoration: none !important;">
                                                   <img src="{{ asset('img/logo-email.jpg') }}" alt="" border="0" width="252" height="66" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none;" /></a>
                                                </div>
                                             </td>
                                          </tr></tbody></table><!-- end of logo --></td>
                              </tr><!-- Spacing --><tr><td width="100%" height="10" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --></tbody></table></td>
                  </tr></tbody></table></td>
      </tr></tbody></table><!-- End of Header --><!-- Start of menu --><table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="menu" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;"><tbody><tr><td style="border-collapse: collapse;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table width="600" bgcolor="#000000" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><!-- Spacing --><tr><td width="100%" height="20" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --><tr><td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #FFFFFF; border-collapse: collapse;" st-content="viewonline">
                                    Confirmation of your payment to Uncle Bucks
                                 </td>
                              </tr><!-- Spacing --><tr><td width="100%" height="20" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --></tbody></table></td>
                  </tr></tbody></table></td>
      </tr><!-- Spacing --><tr><td style="border-collapse: collapse;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr><td width="100%" height="20" style="border-collapse: collapse;"></td>
                            </tr></table></td>
                </tr></tbody></table></td>
      </tr><!-- Spacing --></tbody></table><!-- End of menu -->
     
     <!-- text --><table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;"><tbody><tr><td style="border-collapse: collapse;">
            <table width="600" bgcolor="#f7f7f7" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td style="border-collapse: collapse;">
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><!-- spacing --><tr><td width="100%" height="20" style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; border-collapse: collapse;"> </td>
                                          </tr><!-- End of spacing --><!-- Title --><!-- content --><tr><td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #555; text-align: left; line-height: 24px; border-collapse: collapse;" align="left">
                                                Well thank ya kindly! We received your payment of ${{$amount}}, and your {{$item->title}} has been posted to Uncle Bucks.<br /><br />
                                                Payment confirmation code: {{$confirmation_id}}<br />
                                                Item expiration date: {{$item->expires_at->format('F d, Y') }}
                                                </td>
                                          </tr><!-- End of content --><!-- spacing --><tr><td width="100%" height="20" style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; border-collapse: collapse;"> </td>
                                          </tr><!-- End of spacing --></tbody></table></td>
                              </tr></tbody></table></td>
                  </tr></tbody></table></td>
      </tr></tbody></table><!-- end of text -->
      
      <!-- Start of preheader --><table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;"><tbody><tr><td style="border-collapse: collapse;">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td width="100%" style="border-collapse: collapse;">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><!-- Spacing --><tr><td height="20" style="border-collapse: collapse;"></td>
                              </tr><tr><td style="border-collapse: collapse;">
                                    <table width="600" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tbody><tr><td align="left" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #666666; border-collapse: collapse;" st-content="viewonline">
                                                You received this email to confirm your payment at UncleBucks.co. If you did not make this payment, or you think this email was sent in error, please contact us at <a style="color:#333333;" href="mailto:info@unclebucks.co">info@unclebucks.co</a> and let us know how we can help you.                                                
                                             </td>
                                          </tr></tbody></table></td>
                              </tr><!-- Spacing --><tr><td width="100%" height="20" style="border-collapse: collapse;"></td>
                              </tr><!-- Spacing --></tbody></table></td>
                  </tr></tbody></table></td>
      </tr></tbody></table><!-- End of preheader --></body>
</html>
