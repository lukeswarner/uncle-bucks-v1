	@extends('emails.template')

	@section('content')
		<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="alert alert-ub">
							<h3>{{ $subject }}</h3>
						</td>
					</tr>
					<tr>
						<td class="content-wrap">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="content-block">
										From:  
									</td>
									<td>
										{{ $from_email }}
									</td>
								</tr>
								<tr>
									<td class="content-block" >
										Message:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
									</td>
									<td>
										{{ $msg }}
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table width="100%">
						<tr>
							<td class="aligncenter content-block"></td>
						</tr>
					</table>
				</div></div>
		@endsection