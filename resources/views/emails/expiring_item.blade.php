	@extends('emails.template')

	@section('content')
		<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="content-block">
							<img style="padding-left: 20px;" src="{{asset('img/logo-web-black.png')}}" alt="Uncle Bucks logo"/>
						</td>
					</tr>
					<tr>
						<td class="alert alert-warning">
							<h3>WELL SHOOT! {{$item->title}} expires soon!</h3>
						</td>
					</tr>
					
					<tr>
						<td class="content-wrap">
							<table width="100%" cellpadding="0" cellspacing="0">
								
								<tr>
									<td>
										<img src="{{ url($item->thumbnail) }}" class="item-photo"/>
									</td>
									<td>
										<a class="item-title" href="{{ url('item/'.$item->id) }}"><h2 >{{$item->title}}</h2></a>
										Expires: <span class="ub-text">{{$item->expires_at->toFormattedDateString()}}</span>
									</td>
								</tr>
								
							</table>
						</td>
					</tr>
					
			
					<tr>
						<td class="content-wrap">
							<table width="100%" cellpadding="0" cellspacing="0">
								
								<tr>
									<td class="content-block">
										It's lookin' like your item is set to expire soon. Scoot right over to Uncle Bucks and renew your item. You can always set your item to auto renew so you don't have to worry about your items expiring.

									</td>
								</tr>
								<tr>
									<td class="content-block">
										<a href="http://unclebucks.co/auth/login" class="btn btn-ub">Manage My Uncle Bucks Items</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table width="100%">
						<tr>
							<td class="aligncenter content-block">This is a courtesy message regarding an item you posted at Uncle Bucks.<br /> If you no longer want to receive these messages you can change your contact preferences by logging into your <a href="http://unclebucks.co/auth/login">Uncle Bucks Account</a>. </td>
						</tr>
					</table>
				</div></div>
		@endsection