@extends('app')


@section('scripts')
	<script src="{{ asset('/js/ub-item-select.js') }}"></script>
@endsection

<?php 
/**
 * @issues
 * UB-16 Category CRUD [2015-06-16]
 */
?>

<?php 
	if( empty($category) ){
		$category_id= "";
		$category_name= "All Items";
	}
	else {
		$category_id= $category->id;
		$category_name= $category->name;
	}
?>

@section('content')
<div class="container-fluid">
	
	{!! Form::open(['id' => 'itemform', 'url' => 'ajax', 'method' => 'post', 'class'=>'form-horizontal']) !!}
					
		
		{!! Form::hidden('category_id', $category_id, ['id'=>'category_id']) !!}
		{!! Form::hidden('filter', $filter, ['id'=>'filter']) !!}
		{!! Form::hidden('kw', $keyword, ['id'=>'kw']) !!}
			
		
	{!! Form::close() !!}
	
	<div class="row">
		<div class="col-md-4">
			<h1>{{ $category_name }}</h1>
			<button class="btn btn-ub btn-block btn-category" name="1">Motorsports</button>
			<button class="btn btn-ub btn-block btn-category" name="2">Tools</button>
			<button class="btn btn-ub btn-block btn-category" name="3">Bicycles</button>
			<input type="text" id="keyword" name="keyword" placeholder="Keyword" value="{{ $keyword }}"></input>
			<button class="btn btn-default btn-filter btn-search">
			  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</button>	
		</div>
		<div class="col-md-4">
			<button class="btn btn-ub btn-block btn-filter" name="recent">Most Recent</button>
			<button class="btn btn-ub btn-block btn-filter" name="location">Nearest Me</button>
			<button class="btn btn-ub btn-block btn-filter" name="favorite">Favorites</button>
			<button class="btn btn-ub btn-block btn-filter" name="user">My Items</button>
		</div>
	</div>

</div>
@endsection