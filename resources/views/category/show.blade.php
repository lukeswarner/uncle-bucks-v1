@extends('app')


<?php 
/**
 * @issues
 * UB-12 Category CRUD [2015-06-16]
 */
?>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<h2>{{ $category->name }}</h2>
			<p><b>slug: </b> {{ $category->slug }}</p>
			<p><b>Group: </b>{{ $category->category_group->name }}</p>
			<p><b>Description: </b><br />
				 {{ $category->description }}
			</p>
			
		</div>
		<div class="col-md-2">
			<a href="{{ url('category/'.$category->slug.'/edit') }}">edit category</a>
		</div>
		
	</div>
</div>
@endsection