



	@if(Request::path() === '/')
			
				
				<div class="collapse" id="filter-collapse">
					<div class="filter-content">
						<h2 class="h-filter">Filter items by category or keyword</h2>
						{!! Form::open(['url'=>' ', 'method' => 'post', 'class'=>'']) !!}
							<div class="form-group">
								<label class="sr-only" for="category_id">Category</label>
								
									{!! Form::select('category_id', $categorie_list, $default_category_id, ['class'=>'form-control btn-filter']); !!}
								
							</div>
						
							<div class="form-group">
							    <label class="sr-only" for="keyword">Keyword</label>
							    {!! Form::text('keyword', null, ['class' => 'form-control btn-filter', 'placeholder' => 'Keyword']) !!}
							</div>	
			
							
							<button type="submit" class="btn btn-default btn-filter">
							  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</button>				
									
						{!! Form::close() !!}
					</div>
				</div>
			
			@endif
		