@extends('app')


<?php 
/**
 * @issues
 * UB-16 Category CRUD [2015-06-16]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Update <b>{{ $category->name }}</b> Category</div>
				<div class="panel-body">
					<div class="col-md-9">
					@include('errors.list')
				
					{!! Form::model( $category, ['action'=>['CategoryController@update', $category->id], 'method' => 'patch', 'class'=>'form-horizontal']) !!}
					

						@include('category.form', ['submitButtonText' => 'Update Category', 'defaultCategoryGroupID' => null])
							
						
					{!! Form::close() !!}
					</div>
					
					@include('partials.delete-modal', [ 'actionURL' => 'category/'.$category->id, 'resource' => 'Category', 'message' => null ])
	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
