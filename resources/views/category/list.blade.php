@extends('app')


<?php 
/**
 * @issues
 * UB-12 Category CRUD [2015-06-16]
 */
?>

@section('content')
<div class="container-fluid">

	
	
	<div class="row">
		<div class="col-md-2 col-md-offset-1">
			<h2 style="margin-top:0;">Categories</h2>
		</div>
		<div class="col-md-4 col-md-offset-4">
			<a class="btn btn-ub-inverse pull-right" style="margin-left:30px;" href="{{url('category/create')}}">Create Category</a>  
		 	<a class="btn btn-ub-inverse pull-right" href="{{url('categorygroup/create')}}">Create Category Group</a>
		 </div>
	</div>
	
	
	@forelse ($category_groups as $group)
		<div class="row">	
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
				    	<h2 class="panel-title"><a href="{{ url('categorygroup/'.$group->slug.'/edit') }}">{{ $group->name }}</a></h2>
				  	</div>
				  	
					@forelse ($group->categories as $category)
						<div class="panel-body">
						<div class="row">
							<div class="col-md-3">
								<a href="{{ url('category/'.$category->slug.'/edit') }}"><b>{{ $category->name }}</b></a>
								&nbsp;&nbsp;&nbsp;(/{{ $category->slug }})
							</div>
							<div class="col-md-9">
									 {{ Illuminate\Support\Str::limit($category->description, 140) }}
							</div>
						</div>
						</div>
					@empty
					
						<div class="row"><div class="col-md-10"><p style="padding:15px;">This group has no categories. You should <a href="{{ url('category/create') }}">make some</a>.</p></div></div>
						
					@endforelse
				</div>
			</div>
		</div>	
	@empty
	
		<div class="row"><div class="col-md-10"><p style="padding-left:15px;">There are no Category Groups. You should <a href="{{ url('categorygroup/create') }}">make some</a>.</p></div></div>
	@endforelse
	
	
	
	
</div>
@endsection