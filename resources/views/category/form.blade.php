<?php 
/**
 * @issues
 * UB-12 Category CRUD) [2015-06-16]
 */

	/*
	 * required variables
	 * 		$submitButtonText	- used to name the submit button
	 */

?>

		<div class="form-group">
			{!! Form::label('name', 'Name:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('slug', 'URL slug:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::text('slug', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		
		<div class="form-group">
			{!! Form::label('category_group_id', 'Category Group:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::select('category_group_id', $category_groups, $defaultCategoryGroupID, ['class'=>'form-control']); !!}
			</div>
		</div>
		
		<div class="form-group">
			{!! Form::label('description', 'Description:', ['class'=>'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				{!! Form::submit($submitButtonText, ['class' => 'btn btn-ub btn-block']) !!}
			</div>
		</div>