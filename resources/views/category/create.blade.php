@extends('app')


<?php 
/**
 * @issues
 * UB-16 Category CRUD [2015-06-16]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Create New Category</div>
				<div class="panel-body">
					
					@include('errors.list')

					{!! Form::open(['url'=>'category', 'method' => 'post', 'class'=>'form-horizontal']) !!}
					

						@include('category.form', ['submitButtonText' => 'Create Category', 'defaultCategoryGroupID' => " "])
							
						
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
