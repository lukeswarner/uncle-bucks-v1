<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Uncle Bucks</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/ub.css?'.date('YmdHi')) }}" rel="stylesheet">
	<link href="{{ asset('/css/masonry.css') }}" rel="stylesheet">
	
	<!--  toggle switch css -->
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	@yield('scripts')
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="{{ url() }}"><img class="img-responsive" src="{{ asset('img/logo-web.png') }}" /></a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				<ul class="nav navbar-nav navbar-right navbar-ub">
					
					
					
					<input type="checkbox" name="menu-toggle" id="menu-toggle" style="url('http://localhost/ub/img/menu.png') no-repeat;"/>
					<label for="menu-toggle"></label>
					
					<li class="navbar-item-browse"><a href="{{ url('/items') }}">Browse</a></li>
					
					<?php 
					/*
					@if (Auth::guest())
						<li class="navbar-item-browse"><a href="{{ url('/auth/login') }}">Login</a></li>
						<li class="navbar-item-browse"><a href="{{ url('/auth/register') }}">Register</a></li>
						
					@else
						<li class="navbar-item-browse"><a href="{{ url('user/'.Auth::user()->username.'/edit')}}">My Account</a></li>
						<li class="navbar-item-browse"><a href="{{ url('/auth/logout') }}">Logout</a></li>
						
					@endif
					*/
					?>
					
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
</body>
</html>
