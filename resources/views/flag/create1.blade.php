@extends('app')


<?php 
/**
 * @issues
 * UB-150 Flag Item As Inappropriate [2016-02-24]
 */
?>




@section('content')
<div class="container-fluid">
	<div class="row">
			
            @include('errors.list')
            <div class="col-md-offset-1 col-md-8">
            </div>
            <div class="col-lg-offset-1 col-lg-6">
                
                <h2>Flag Inappropriate Item</h2>
                <p>
                    If an item is miscategorized, has content which is inapropriate, is prohibited, or violates the terms of use, please let us know! Inappropriate items can be removed and violators could have their accounts terminated, as outline in the Uncle Bucks terms of use.
                </p>
            </div>
            <div class="col-lg-3 col-md-12">   
                <div class="row">
                    <h2 class="visible-lg" style="margin-top: 10px;">&nbsp;</h2>
                    <div class="col-lg-12 col-xs-6">
                        <a href="#" target="_blank" class="btn btn-ub btn-block">Prohibited Items</a>
                    </div>
                    <div class="col-lg-12 col-xs-6">
                        <a href="#" target="_blank" class="btn btn-ub btn-block">Terms of Use</a>
                    </div>
                </div>
            </div>
            
    </div>      
    <div class="row">      
        <div class="col-lg-2 col-lg-offset-1">
			<img src="{{ url($item->photo) }}" class="img-responsive current-photo" />	
        </div>
                        
        <div class="col-lg-6">
            <h3>{{ $item->title }}</h3>
            <h4>{!! $item->displayRentalDetails(false, true); !!}</h4>
    
            <h4>{!! $item->print_category() !!}</h4>

            <div class="item-description">{!! nl2br(e($item->description))  !!}</div>	
            
            
        </div>
        
        
        
    
    </div>
     
    <div class="row">
					{!! Form::open(['url'=>'flag', 'method' => 'post', 'class'=>'form-horizontal']) !!}
						
                        
                        

						<div class="form-group">
                            <label class="col-lg-3 control-label">Why are you flagging this item?</label>
                            <div class="col-lg-7">
                                {!! Form::select('reason', ['miscategorized'=>'Item has been miscategorized', 'prohibited'=>'Item is prohibitted', 'inappropriate'=>'Posting contains inappropriate content'], null , ['class'=>'form-control']); !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Can you provide more details?</label>
                            <div class="col-lg-7">
                                {!! Form::textarea('explanation', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-7 col-lg-offset-3">
                            
                                {!! Form::submit('Flag the Item!', ['class' => 'btn btn-ub btn-block']) !!}
                            </div>
                        </div>
						
					{!! Form::close() !!}
		</div>	
		
	</div>
</div>
@endsection
