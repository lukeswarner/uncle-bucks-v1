@extends('app')


<?php 
/**
 * @issues
 * UB-150 Flag Item As Inappropriate [2016-02-24]
 */
?>




@section('content')
<div class="container-fluid">
	<div class="row">
			
            @include('errors.list')
    </div>
    
    <div class="row">
        <div class="col-sm-4 col-lg-3 visible-sm visible-md visible-lg">
                <img src="{{ url($item->photo) }}" class="img-responsive flag-photo" />
                             
                <div class="clearfix"><!-- --></div>
                <h2>{{ $item->title }}</h2>
                <h4>{!! $item->displayRentalDetails(false, true); !!}</h4>
        
                <h4>{!! $item->print_category() !!}</h4>

                <p style="padding-right: 20px;">{!! nl2br(e($item->description))  !!}</p>	
            
        </div>
        
        <div class="col-sm-8 col-lg-9">
            <div class="row">
                <div class="col-lg-8">
                    <h2>Flag Inappropriate Item</h2>
                    <p>
                        If an item is miscategorized, has content which is inapropriate, is prohibited, or violates the terms of use, please let us know! Inappropriate items can be removed and violators could have their accounts terminated, as outline in the Uncle Bucks terms of use.
                    </p>
                </div>
                
                <div class="col-lg-4 col-md-12">   
                    <div class="row">
                        <h2 class="visible-lg" style="margin-top: 10px;">&nbsp;</h2>
                        <div class="col-lg-12 col-md-6 col-sm-4 col-xs-6">
                            <a href="{{ url('resources/prohibited-items') }}" target="_blank" class="btn btn-ub btn-flag">Prohibited Items</a>
                        </div>
                        <div class="col-lg-12 col-nd-6 col-sm-4 col-xs-6">
                            <a href="{{ url('resources/terms-of-use') }}" target="_blank" class="btn btn-ub btn-flag">Terms of Use</a>
                        </div>
                    </div>
                </div>
            </div>
 
           
        
            <div class="row visible-xs ">
                <div class="col-xs-5 col-sm-4 col-xxs-12">
                        <img src="{{ url($item->photo) }}" class="img-responsive flag-photo" />
                 </div>
                <div class="col-xs-7 col-sm-8 col-xxs-12">
                        
                        <h2>{{ $item->title }}</h2>
                        <h4>{!! $item->displayRentalDetails(false, true); !!}</h4>
                
                        <h4>{!! $item->print_category() !!}</h4>

                        <p style="padding-right: 20px;">{!! nl2br(e($item->description))  !!}</p>	
                    
                </div>
            </div>


 
            
            <div class="row">
            
                <div class="col-lg-8 flag-form">
                    {!! Form::open(['url'=>'flag/'.$item->id, 'method' => 'post', ]) !!}
                                
                        <div class="form-group">
                            <label for="reason">Why are you flagging this item?</label>
                            
                            {!! Form::select('reason', ['miscategorized'=>'Item has been miscategorized', 'prohibited'=>'Item is prohibited', 'inappropriate'=>'Posting contains inappropriate content'], null , ['class'=>'form-control', 'id'=>'reason']); !!}
                            
                        </div>
                        
                        <div class="form-group">
                            <label for="explanation">Can you provide more details?</label>
                            
                            {!! Form::textarea('explanation', null, ['class' => 'form-control', 'id' => 'explanation']) !!}
                            
                        </div>
                        
                        <div class="form-group">
                            
                            {!! Form::submit('Flag the Item!', ['class' => 'btn btn-ub btn-block']) !!}
                            
                        </div>
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
	</div>
</div>
@endsection
