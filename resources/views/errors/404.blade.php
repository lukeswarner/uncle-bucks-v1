<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>Uncle Bucks</title>
	
		<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/ub-layout.css?'.date('Ymd')) }}" rel="stylesheet">
		<link href="{{ asset('/css/ub-footer.css') }}" rel="stylesheet">
	
		<link href="{{ asset('/css/rounded_corners.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/fonts.css') }}" rel='stylesheet' type='text/css'>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		
		
	</head>
	<body>
	


   
        <div class="container">   
            <div class="row" style="margin-top: 5%;">
                <img class="img-responsive logo center-block" src="{{ asset('img/buck.png') }}" />
            </div>
            <div class="row">
                <h1 class="text-center" >Well this is embarrassing... </h1>
                <h3 style="text-align: center;">I'm afraid we couldn't find the page you were lookin' for.</h3>
                
            </div>
            
            
        </div>
        
    </body>
</html>
