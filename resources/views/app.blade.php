<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>Uncle Bucks</title>
	
		<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/ub-layout.css?'.date('Ymd')) }}" rel="stylesheet">
		<link href="{{ asset('/css/ub-footer.css') }}" rel="stylesheet">
	
		@yield('css')
		
		<link href="{{ asset('/css/rounded_corners.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/fonts.css') }}" rel='stylesheet' type='text/css'>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		
		
	</head>
	<body>
	
		<header class="masthead">
			<div class="container">
				
				
				<div class="row hidden-sm hidden-xs">
					<div id="ub-logo" class="col-sm-11">
				    	<div id="tagline">HELPING NEIGHBORS RENT FROM NEIGHBORS</div>
				    	
				    	<ul id="desktop-nav" class="nav navbar-nav navbar-right">    
				        	
				        		<li><a class="btn btn-category" href="{{ url('/about') }}">About</a></li>
                                <li><a class="btn btn-category" href="{{ url('/resources/faq') }}">FAQs</a></li>
								<li><a class="btn btn-category" href="{{ url('/item/create')}}">Post Item</a></li>
				        	@if (Auth::guest())
								
								<li><a class="btn btn-category" href="{{ url('/auth/login') }}">Login</a></li>
								
							@else
								<li><a class="btn btn-category" href="{{ url('/home')}}">My Account</a></li>
								<li><a class="btn btn-category" href="{{ url('/auth/logout') }}">Logout</a></li>
								
							@endif
				      	</ul>
				      	
				      	<a href="{{ url() }}"><img class="img-responsive logo" src="{{ asset('img/logo-web-black.png') }}" /></a>
				      	
				    </div>
				    
				 </div>
				 
				 
				 <div class="row hidden-md hidden-lg hidden-xl">
				 	
				    <div id="tagline">HELPING NEIGHBORS RENT FROM NEIGHBORS</div>
				    	
					<div id="ub-logo" class="col-xs-8">
				    	<a href="{{ url() }}"><img class="img-responsive logo" src="{{ asset('img/logo-web-black.png') }}" /></a>
				    </div>
				    <div id="mobile-navbar" class="col-xs-4">
				    	<div class="pull-right">  
				    		<h1>
				    		@if(Request::path() === '/')
								<a class="header-button" role="button" data-toggle="collapse" href="#filter-collapse" aria-expanded="false" aria-controls="filter-collapse">
									<i class="glyphicon glyphicon-search"></i>
								</a>
							@endif  
					        	<a class="header-button" role="button" data-toggle="collapse" href="#menu-collapse" aria-expanded="false" aria-controls="menu-collapse">
					        		<i class="glyphicon glyphicon-menu-hamburger"></i>
				        		</a>
				        	</h1>
			        	</div>
			        </div>
			     
			        
				 </div>
				 		
				 
		      	<div id="header-border"></div>
		  	</div>
		</header>	  	  	
		
		<!-- Begin Body -->
		<div class="container">
		
			<div class="row hidden-md hidden-lg hidden-xl">
				<div class="collapse" id="menu-collapse">
					<ul class="menu-content">
                        <li><a class="btn btn-block btn-nav" href="{{ url('/about') }}">About</a></li>
                        <li><a class="btn btn-block btn-nav" href="{{ url('/resources/faq') }}">FAQs</a></li>
							<li><a class="btn btn-block btn-nav" href="{{ url('/item/create')}}">Post Item</a></li>
						@if (Auth::guest())
							<li><a class="btn btn-block btn-nav" href="{{ url('/auth/login') }}">Login</a></li>
						@else
							<!-- <li><a class="btn btn-ub btn-menu" href="{{ url('user/'.Auth::user()->username.'/edit')}}">My Account</a></li> -->
							<li><a class="btn btn-block btn-nav" href="{{ url('/home')}}">My Account</a></li>
							<li><a class="btn btn-block btn-nav" href="{{ url('/auth/logout') }}">Logout</a></li>
							
						@endif
					</ul>
				</div>
			</div>
		
		
			<div class="row">
		  			<div class="col-md-2">
		              
		             	 @include('search.sidebar')
		              
		              
		      		</div>  
		      		<div class="col-md-10">
		      			<div id="content">
                         @if(Session::has('message'))
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12 {{ Session::get('alert-class', 'alert-ub-inverse') }}"><h3>{{ Session::get('message') }}</h3></div>
                                </div>
                            </div>
                        @endif
                            
		      				@yield('content')
		      			
		    		</div> 
		  	</div>
		</div>
		
		
		<footer class="footer">
	    	<div class="container">
	    		<div class="row">
	    			<?php /*   //UB-203 Footer only has copyright info
			      	<div class="col-sm-9 col-xs-12 pull-left">
			      		<ul class="footer-list list-unstyled">
			      			<li><a href="{{url('about')}}">About Uncle Bucks</a></li>
			      			<li><a href="{{url('business-partnerships')}}">Business Partnerships</a></li>
			      			<li><a href="{{url('gives-back')}}">Uncle Bucks Gives Back</a></li>
			      		</ul>
			        </div>
			        */?>
			        <div id="footer-right" class="col-sm-3 col-xs-12">
			        	<p class="text-muted">&copy; Uncle Bucks {{ date('Y') }}</p>
			        </div>
			   
		        </div>
	      	</div>
	    </footer>
	    
	    
	   
		
			
		<!-- Scripts -->
		<?php 
		if (App::isLocal()) { ?>
		<script src="{{ asset('/js/jquery-2.1.4.min.js') }}"></script>
		<script src="{{ asset('/js/bootstrap-3.3.5.js') }}"></script>
		<?php 
		} else { ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<?php 
		}
		?>
		<script>
		if (document.all && !document.addEventListener) {
		    window.location.href = "{{ url('old-browser') }}";
		}
		</script>
		
		
		<script>
			$('#sidebar').affix({
		      offset: {
		        top: 168
		      }
			});	
		</script>
		<script src="{{ asset('/js/ub-item-select.js') }}"></script>
		
		@yield('scripts')
		
	</body>
</html>


