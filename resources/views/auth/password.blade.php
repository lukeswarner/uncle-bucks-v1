@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		
				
				
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					
					@include('errors.list')

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h2>Reset Password</h2>
								<p>If you don't remember your password, we can help you reset it. Enter in the email address which you used to register at Uncle Bucks, and we'll email you a link to reset your password.</p>
								<p>The link is only valid for 24 hours, so don't wait too long!</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<button type="submit" class="btn btn-ub">
									Send Email to Reset My Password
								</button>
							</div>
						</div>
					</form>
				
	</div>
</div>
@endsection
