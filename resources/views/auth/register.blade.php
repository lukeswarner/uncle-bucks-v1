@extends('app')


<?php 
/**
 * @issues
 * UB-20 User Migration (sub-table of UB-1 User CRUD) [2015-06-13]
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
			
					
					@include('errors.list')

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<h2>You're a-fixin to make an account with Uncle Bucks</h2>
								<p>Registering with Uncle Bucks allows you to post items for your neighbors to rent. After registering, you can update your details and control which contact information you want to keep private.</p>
							</div>
						</div>
						
						
						@include('user.form.required-fields')

						<div class="form-group">
							<label class="col-md-3 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                By clicking on "Create My Account", you agree to the Uncle Bucks 
                                <a target="_blank" class="underline" href="{{ url('resources/terms-of-use') }}">Terms of Use</a> and 
                                <a target="_blank" class="underline" href="{{ url('resources/privacy-policy') }}">Privacy Policy</a> and 
                                agree not to post any <a target="_blank" class="underline" href="{{ url('resources/prohibited-items') }}">Prohibited Items</a>.
                            </div>
                        </div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								@include('user.form.submit-button', ['submitButtonText' => 'Create My Account'])
							</div>
						</div>
					</form>
		
	</div>
</div>
@endsection
