@extends('app')

<?php 
/**
 * @issues
 * UB-7 Update User Account (sub-table of UB-1 User CRUD) [2015-06-13]
 * 	
 */
?>

@section('content')
<div class="container-fluid">
	<div class="row">
		
				
				
				
					@include('errors.list')

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-md-6 col-md-offset-2">
								<h2>Login to your account</h2>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label">E-Mail Address</label>
							<div class="col-md-8 col-lg-7">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label">Password</label>
							<div class="col-md-8 col-lg-7">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-2 col-lg-7">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-2 col-lg-7 hidden-xs hidden-sm">
								<button type="submit" class="btn btn-ub">Login to Uncle Bucks</button>

								<a class="btn btn-ub pull-right" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
							
							<div class="col-md-8 col-md-offset-2 col-lg-7 hidden-md hidden-lg hidden-xl">
								<button type="submit" class="btn btn-ub btn-block">Login to Uncle Bucks</button>

								<a class="btn btn-ub btn-block" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
						</div>
					</form>
				
	</div>
</div>
@endsection
