
	var last_item= 0;
	var last_type= "";

    $(document).ready( initialize_overlay );
    
    function initialize_overlay(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
            	
            	//close any open overlays by looping through all .img elements and closing (if hovered)
				$(".img").each( function( i ){
					if( $(this).hasClass("hover") ){
						$(this).removeClass("hover");
					}
				});

            	//then add hover to the newly-hovered element
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
                
                
        	
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
                resetContent();
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
                resetContent();
            });
        }
    }

    function resetContent(){

    		$("#overlay-contact"+last_item).fadeOut('fast');
			$("#overlay-content"+last_item).fadeIn('fast');
			$("#item-title"+last_item).fadeIn('fast');
			
    }

	
	function showContact(item_id, contact_url, item_type) {

		switch (item_type){
			case "email":
				glyph= '<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>&nbsp;';
				prefix= 'mailto:';
				break;
				
			case "phone":
				glyph= '<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> ';
				prefix= 'tel:';
				break;
				
			case "sms":
				glyph= '<i class="glyphicon glyphicon-comment" aria-hidden="true"></i> ';
				prefix= 'sms:';
				break;
				
		}
		contact_div= "#overlay-contact"+item_id;
		content_div= "#overlay-content"+item_id;
		title_div= "#item-title"+item_id;
		
		

		if( $(content_div).is(":visible") ){	//hide title and content and show contact_div

			$(title_div).fadeOut('fast');
			$(content_div).fadeOut('fast', function() {
					$(contact_div).html('<a class="contact-link-'+item_type+'" href="'+ prefix + contact_url + '">' + contact_url + '</a>').fadeTo('slow', 100);
			});		
			
			last_item= item_id;
			last_type= item_type;
		}
		
		else if(last_type == item_type){

			resetContent();
			last_type= "";
		}
		
		else {		//refresh the contact info with new contact link
			
			$(title_div).fadeOut('fast');
			$(contact_div).fadeOut("fast", function(){
					$(contact_div).html('<a class="contact-link-'+item_type+'" href="'+ prefix + contact_url + '">' + contact_url + '</a>').fadeIn('fast');
			});
			
			last_item= item_id;
			last_type= item_type;
		}


	}
	
	
