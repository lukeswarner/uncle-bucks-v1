	

		// This identifies your website in the createToken call below
		Stripe.setPublishableKey('pk_test_ZbuBG3nOSI4Yn8rma4KsclM3');
	
		var stripeResponseHandler = function(status, response) {
				
			console.log('in Stripe Response Handler');
			
			var $form = $('#payment-form');
		    if (response.error) {
		    	
		    	console.log('stripe returned an error: '+response.error.message);
		    	
		    	// Show the errors on the form
		    	$('#error-wrapper').show();
		        $('#payment-errors').text(response.error.message);
		        
		        
		        $btn= $form.find('button');
		       reset($btn);
		        
		    } else {
		    	console.log('stripe returned a token: '+response.id);
		    	
		        // token contains id, last4, and card type
		        var token = response.id;
		        
		        console.log('token: '+token);
		        
		        // Insert the token into the form so it gets submitted to the server
		        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
		        // and re-submit
		        $form.get(0).submit();
		    }
	    };
	    
	    jQuery(function($) {
	    	$('#payment-form').submit(function(e) {
		        console.log('payment-from submitted');
		        
	    		var $form = $(this);
		        
		        // Disable the submit button to prevent repeated clicks
		        $btn= $form.find('button');
		        switch_text($btn);
		        
		        console.log('calling Stripe');
		        Stripe.card.createToken($form, stripeResponseHandler);
		        
		        // Prevent the form from submitting with the default action
		        return false;
	    	});
	    });
	    
	    
	    /*
	     * when payment buttons are clicked, switch their text
	     
	    $('.payment-btn').on('click', function (e) {
	    	console.log('payment button on click');
	    	switch_text( $(this) );
	    	console.log('payment button - done');
	  	});
	  	*/
	  	
	    /*
	     * switch the text on the specified button, and disable to prevent multiple clicks
	     * 		uses data-submit-text to specify
	     */
	    function switch_text($btn){
	    	console.log('switch text');
	    	
	    	$btn.text( $btn.attr("data-submit-text") );
	    	$btn.prop('disabled', true);
	    	
	    	console.log('text switched');
	    }
	    
	    
	  	/*
	  	 * if there is an error, reset the submit button to its default values
	  	 * 		uses data-default-text
	  	 */
	  	function reset($btn){
	  		console.log('reset text');
			$btn.text( $btn.attr("data-default-text") );
			$btn.prop('disabled', false);
			
			console.log('text reset');	
		}
		