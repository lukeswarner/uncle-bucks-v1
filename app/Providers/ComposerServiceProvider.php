<?php namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
		// Setup a View Composer to load data needed for Category Sidebar
		View::composer('search.sidebar', 'App\Http\ViewComposers\SidebarMenuComposer');
		
		// Setup a View Composer to load data needed for Filter Menu nav bar
		View::composer('search.filter-menu', 'App\Http\ViewComposers\FilterMenuComposer');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
