<?php namespace App;

/**
 *
 * @author Luke
 * @issues
 * UB-14 CategoryGroups Model
 *
 */

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;		// UB-114 Cache Search Results

/*
 * model: CategoryGroup
 * table: category_groups
 * route: categorygroup
 */
class CategoryGroup extends Model {
	
	use Rememberable;		// UB-114 Cache Search Results
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'slug', 'description']; //UB-22 CategoryGroup CRUD
	
	
	/**
	 * a CategoryGroup has many Categories
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function categories(){						//UB-14 - a CategoryGroup hasMany Categorys
		
		return $this->hasMany('App\Category')->remember(20);
	}
	

}
