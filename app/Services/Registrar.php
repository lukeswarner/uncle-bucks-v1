<?php namespace App\Services;

/**
 * @issues
 * UB-20 User Migration (sub-table of UB-1 User CRUD) [2015-06-13]
 * UB-11 User Roles (sub-table of UB-1 User CRUD) [2015-06-19]
 * 
 * 
 */
use App\User;
use App\Role;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Mail;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			//'username' => 'required|max:255|unique:users',		////UB-139 Remove username
			'email' => 'required|email|max:255|unique:users',
			'phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		//if phone is not provided, disable contact by phone and sms
		if( empty( $data['phone'] ) ){							//UB-133 Phone Optional for Register
			$data['contact_phone']= FALSE;					
			$data['contact_sms']= FALSE;
		}
		else {
			$data['contact_phone']= TRUE;
			$data['contact_sms']= TRUE;
		}
		
		$user= User::create([
			//'username' => $data['username'], 				//UB-139 Remove username
			'email' => $data['email'],
			'phone' => $data['phone'],
			'password' => bcrypt($data['password']),
			'contact_email' => true,						//UB-133 Phone Optional for Register
			'contact_phone' => $data['contact_phone'],		
			'contact_sms' => $data['contact_sms']			
		]);
		
		
		$individual_role= Role::where('name', '=', 'individual')->first();	// UB-11 User Roles [2015.06.19]
		if($individual_role){
			$user->attachRole($individual_role);
		}
		
		
		// UB-208 Automatic Email Systems
		$to_email= $user->email;
		
        try{ 
            Mail::send('emails.inlined.welcome', ['user' => $user], function($message) use ($to_email)
            {
                $message->to($to_email)->subject('Welcome to Uncle Bucks');
            });
        }
        catch(GuzzleHttp\Exception\ClientException $e){
            \Log::error('Exception while attempting to send welcome email to new user: '.$user->id);
        }
		
		return $user;
	}
	

}
