<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Flag extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [		
			'item_id', 
			'owner_id',		
			'flagger_id',
			'flagger_ip', 
			'reason', 
			'explanation', 
			'created_at',
			'updated_at'
	]; 


    const MAX_FLAGS = 3;			//UB-150 Items can be flagged as inappropriate
	
	public function item(){
		return $this->belongsTo('App\Item');
	}
	
	public function owner(){		
		return $this->belongsTo('App\User', 'owner_id');
	}
	
	public function flagger(){
		return $this->belongsTo('App\User', 'flagger_id');
	}
    
    
    public function reason_pretty(){
        switch($this->reason){
            
            case "inappropriate":
                return "Inappropriate content";
                break;
            
            case "miscategorized":
                return "Miscategorized";
                break;
                
            case "prohibited":
                return "Prohibited item";
        }
    }
    
    /**
     * @brief check if a flag has already been created by this person 
     * @param string $item_id 
     * @param string $ip 
     * @param string $flagger_id 
     * @return  BOOL 
     */
    
    public static function isDuplicateFlag($item_id, $ip, $flagger_id){
        
       if( empty($flagger_id) ){
           return \App\Flag::where('item_id','=', $item_id)->where('flagger_ip', '=', $ip)->where('active', '=', 1)->count() ;
       }
        
       return ( \App\Flag::where('item_id','=', $item_id)->where('flagger_ip', '=', $ip)->where('active', '=', 1)->count()  || 
                \App\Flag::where('item_id','=', $item_id)->where('flagger_id', '=', $flagger_id)->where('active', '=', 1)->count() 
                ); 

    }

    
    
    public static function findByItem($item_id){
        return \App\Flag::where('item_id','=', $item_id)->get();
    }
}
