<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use \App\Category;
use Illuminate\Http\Request;

class FilterMenuComposer {

    
   protected $filter;

    /**
     * Create a new profile composer.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
          $this->filter = $request->session()->get('filter', 'recent');
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
        		'filter' => $this->filter 
			] );
    }

}