<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use \App\Category;
use \App\CategoryGroup;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class SidebarMenuComposer {

    /**
     * The list of all Categories
     *
     * @var Category
     */
    protected $groups;
    protected $categories;
    protected $category_list;
    protected $category_id;
    protected $keyword;

    /**
     * Create a new profile composer.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
    	
    	
        // Dependencies automatically resolved by service container...
        //$this->groups= CategoryGroup::all();
    	
        $this->categories = Category::all()->sortBy('name');
        
        $this->category_list=  $this->categories->lists('name', 'id');  	//lists gets all() the categories and formats them as [ 'id' => 'name', ...] for dropdowns!
        $this->category_list[0]="All Categories";
        sort($this->category_list);   //sort categories so that "All Categories" is first
        
        $this->category_id = $request->session()->get('category_id', 0);
        $this->filter = $request->session()->get('filter', 'recent');
        $this->keyword = $request->session()->get('keyword', '');
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
        		//'groups' => $this->groups, 
        		'categories' => $this->categories,
        		'category_list' => $this->category_list, 
        		'category_id' => $this->category_id, 
        		'keyword' => $this->keyword, 
        		'filter' => $this->filter 
			] );
    }

}