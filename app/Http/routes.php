<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::filter('editable', function($route, $request){

	if ( strncmp( $request->path(), "item", 4) == 0 ){
		$id= $request->segment(2);
		
		$item= App\Item::find($id);
		if( ! $item ){
			abort('404');
		}
		
		
		if( Auth::guest() ){
			\Log::info('item route filter #'.$item->id.' - NOT EDITABLE. User is a guest');
			return redirect(url('item', $id));
		}
		
		$user = Auth::user();
		
		if (  $item->checkEditable( $user ) == FALSE ){
			\Log::info('item route filter #'.$item->id.' - NOT EDITABLE. User '.$user->id.' does not have checkEditable priviledges ');
			return redirect(url('item', $id));
		}
		
		
		/*
		if ( Auth::guest() || $item->checkEditable( Auth::user() ) == FALSE ){
			\Log::info('item route filter - NOT EDITABLE. guest?'.Auth::guest().' check editable?'.$item->checkEditable( Auth::user() ) );
			return redirect(url('item', $id));
		}
		*/
	}
	
	
	//if we make it through the filter, we don't return - just continue processing
	
});



/*********** TEST ROUTES *****/
//test page to see php_info
Route::get('info', 'WelcomeController@info');

//test page to see menu transitions
Route::get('menu', 'FooController@menu');

//test page to see home layout with fixed sidebar
Route::get('glyphicons', 'FooController@glyphicons');

//test page to see menu transitions
Route::get('isotope', 'FooController@index');


Route::get('ajax', 'FooController@ajax');
Route::post('ajax', 'FooController@ajax');


//Static Content Pages
Route::get('/old-browser', 'HomeController@old_browser');
Route::get('/about', 'HomeController@about');
Route::get('/gives-back', 'HomeController@givesback');
Route::get('/business-partnerships', 'HomeController@business');
Route::get('/help', 'HomeController@help');
Route::post('/send_message', 'HomeController@send_message');

//Static Resource Pages
Route::get('/resources/{page}', 'HomeController@resources');

Route::get('/emails/{template}', 'HomeController@email_template');

Route::get('bulk_emails', 'ItemController@email_item_owners');


Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@index');  // make sure form is posting to url=>' ' and NOT url=>'/' - Laravel breaks when an action ends in /
Route::post('/more_items', 'HomeController@more');  // make sure form is posting to url=>' ' and NOT url=>'/' - Laravel breaks when an action ends in /


//home page is only accessible to registered individual accounts
Route::get('home', 'HomeController@home');
Entrust::routeNeedsRole('home*', 'individual', Redirect::to('dashboard'));	//UB-11 User Roles [2015.06.19]

//admin dashboard is only accessible to admins
Route::get('dashboard', 'HomeController@dashboard');
Entrust::routeNeedsRole('dashboard*', 'admin', Redirect::to('/'));			//UB-11 User Roles [2015.06.19]


/********  Category Routes  ********/										//UB-12 Category CRUD
Route::get('category', 'CategoryController@index');				
Route::get('category/create', 'CategoryController@create');
Route::post('category', 'CategoryController@store');
Route::get('category/{slug}', 'CategoryController@edit');					//don't SHOW- always point to edit
Route::get('category/{slug}/edit', 'CategoryController@edit');
Route::patch('category/{id}', 'CategoryController@update');					//use ID rather than slug, so category slug can be updated
Route::delete('category/{id}', 'CategoryController@destroy');				//use ID rather than slug

Entrust::routeNeedsRole('category*', 'admin', Redirect::to('/'));			//UB-11 User Roles


/********  CategoryGroup Routes   ********/									//UB-22 CategoryGroup CRUD
Route::get('categorygroup', 'CategoryController@index');					// send to CATEGORY list page- combine them to one
Route::get('categorygroup/create', 'CategoryGroupController@create');
Route::post('categorygroup', 'CategoryGroupController@store');
Route::get('categorygroup/{slug}', 'CategoryGroupController@edit');			//don't SHOW- always point to edit.
Route::get('categorygroup/{slug}/edit', 'CategoryGroupController@edit');
Route::patch('categorygroup/{id}', 'CategoryGroupController@update');		//use ID rather than slug, so category slug can be updated
Route::delete('categorygroup/{id}', 'CategoryGroupController@destroy');		//use ID rather than slug,

Entrust::routeNeedsRole('categorygroup*', 'admin', Redirect::to('/'));		////UB-11 User Roles NOTE: this is actually unneccessary, but provided for clarity.
																			//the Entrust route on "category*" matches categorygroup as well.


Route::get('payment/subscription/manage', 'PaymentController@manage_subscription');
Route::post('payment/subscription/{id}/cancel', 'PaymentController@cancel_subscription');
Route::post('payment/subscription/{id}/reactivate', 'PaymentController@reactivate_subscription');
Route::get('payment/card/update', 'PaymentController@manage_card');
Route::post('payment/card/update', 'PaymentController@update_card');
Route::get('payment/{type}/{plan}', 'PaymentController@prepare_transaction');
Route::post('payment/{type}/{plan}', 'PaymentController@process_transaction');

/********  Item Routes  ********/
Route::get(		'items', 		'ItemController@index');
Route::get(		'item/info', 	'ItemController@info');	
Route::get(		'item/create', 	'ItemController@create');
Route::post(	'item', 		'ItemController@store');
Route::get(		'item/{id}', 	'ItemController@show');
//Route::get(		'item/{id}/edit/location',	array('before' => array('editable'), 'uses' => 'ItemController@edit_location'));
Route::get(		'item/{id}/edit/category', 	array('before' => array('editable'), 'uses' => 'ItemController@edit_category'));
Route::get(		'item/{id}/edit/photo', 	array('before' => array('editable'), 'uses' => 'ItemController@edit_photo'));
Route::get(		'item/{id}/edit', 			array('before' => array('editable'), 'uses' => 'ItemController@edit'));

Route::patch(	'item/{id}', 				array('before' => array('editable'), 'uses' => 'ItemController@update'));
Route::patch(	'item/{id}/update_category', array('before' => array('editable'), 'uses' => 'ItemController@update_category'));
Route::post(	'item/{id}/upload_photo', 	array('before' => array('editable'), 'uses' => 'ItemController@upload_photo'));
Route::delete(	'item/{id}', 				array('before' => array('editable'), 'uses' => 'ItemController@destroy'));

Route::post(	'item/favorite/set', 'ItemController@set_favorite');
Route::post(	'item/favorite/remove', 'ItemController@remove_favorite');

Route::get(		'item/{id}/publish', 		array('before' => array('editable'), 	'uses' => 'ItemController@publish'));
Route::post(	'item/{id}/publish', 		array('before' => array('editable'), 	'uses' => 'ItemController@publish_item'));
Route::get(		'item/{id}/renew', 			array('before' => array('editable'), 	'uses' => 'ItemController@renew'));
Route::patch(	'item/{id}/renew', 			array('before' => array('editable'), 	'uses' => 'ItemController@update_renew'));
Route::get(	'item/{id}/publish_flagged', 		array('before' => array('editable'), 	'uses' => 'ItemController@publish_flagged'));

Entrust::routeNeedsRole('item/create', array('individual','admin'), Redirect::to('item/info'), false);    
//Entrust::routeNeedsRole('item/{id}/edit*', array('individual','admin'), Redirect::to('/'), false);		//need to verify that individual is the owner ofthe item in the Controller...
//Entrust::routeNeedsRole('item/{id}/update*', array('individual','admin'), Redirect::to('/'), false);	//need to verify that individual is the owner ofthe item in the Controller...


//Route::get('location', 'LocationController@Index');
Route::get('location/{resource?}/{id?}', 'LocationController@create');
Route::post('location', 'LocationController@store');
//Route::post('location/geocode', 'LocationController@Geocode');


/********  Flag Item Routes *********/
Route::get(		'flag/{id}', 	'FlagController@create');
Route::post(	'flag/{id}', 	'FlagController@store');

/********  User Routes  ********/
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('user/password', 'UserController@edit_password');
Route::post('user/password', 'UserController@update_password');

Route::get('user/{username}/edit', 'UserController@edit');
Route::patch('user/{id}', 'UserController@update');  		//use ID rather than username, so username can be updated
Route::get('user/{username}', 'UserController@show');

