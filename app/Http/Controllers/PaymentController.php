<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
use Auth;
use Mail;
use \App\Item;
use PhpParser\Node\Expr\Cast\Object_;

class PaymentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return view('payment.options');
	}

	
	
	
	public function prepare_transaction($type="charge", $plan="one"){
			
        if(Auth::guest()){
            redirect(url('home'));
        }
		//get the saved card, if the user has one saved
		//if the user does not want to use a card that has been saved, savedCard() also returns FALSE
		$saved_card= Auth::user()->savedCard();
		
		$item= Item::find(Session::get('publish_item_id'));
		
		if( empty($item) ){ 
            \Log::info('PURCHASE SUBSCRIPTION: User ['.Auth::user()->id.'] '.Auth::user()->email.' is beginning transaction : type: ['.$type.'] plan: ['.$plan.'] saved card ['.($saved_card) ? "yes" : "no".']');
        }
        else {
            \Log::info('PAY FOR ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' is beginning transaction : type: ['.$type.'] plan: ['.$plan.'] saved card ['.($saved_card) ? "yes" : "no".']');
		}
		
		if($type == "charge"){
			if( $plan == "one" ){

				$title= "Purhase One Month for One Buck";
				$amount= 100; //amount in CENTS
				$amount_duration= "$1";
				$description= "This one-time payment of $1 will post a single item to Uncle Bucks for a period of 1 month, after which the item will expire. 
						When it expires, your item will no longer be publicly listed on Uncle Bucks, but you can still access, update, and renew your item at any time. 
						In addition, you will receive email notifying you a few days before your item expires, and the day your item is set to expire.";
			}
		}
		
		
		
		if($type == "subscription"){
			if( $plan == "monthly" ){
				
				$title= "Purchase \"Unlimited Items\" Subscription";
				$amount= 1000; //amount in CENTS
				$amount_duration= "$10/month";
				$description= "With an Unlimited Items monthly subscription you can post an unlimited number of items to Uncle Bucks. 
						In addition, all of your items will automatically renew without charge as long as your subscription is current. 
						Your credit card will be automatically charged $10 each month while your subscription is active. 
						You can update your credit card or cancel your monthly subscription at any time by clicking \"My Account\" in the navigation menu at the top of the page."; 
			}
		}
		
		
		
		return view('payment.credits', compact('title', 'item', 'type', 'plan', 'amount', 'amount_duration', 'description', 'saved_card'));
	}
	
	
	public function process_transaction($type="charge", $plan="one", Request $request){
		
		if(Session::has('publish_item_id')){
			$item_id= Session::get('publish_item_id');
			
			
			$item= \App\Item::find($item_id);
			if( ! $item ){
				abort('404');
			}
			
			if ( $item->checkEditable( Auth::user() ) == FALSE ){
				\Log::info('Complete Charge: Auth User is not the Item Owner '.$item->user->id);
				return redirect(url('item', $item->id));
			}
			
			$user= $item->user;
			
			\Log::info('PROCESS TRANSACTION ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' PUBLISH ITEM! ['.$type.'] plan: ['.$plan.']');
				
		}
		else {
			
			$user= Auth::user();
			
			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' No item, just a subscription! : type: ['.$type.'] plan: ['.$plan.']');
		}
		
		
		
		
		
		
		
		
		/**
		 *****************		Setup Payment Method : "saved" or "captured"		*********************
		 */
		if( $request->input('source') == "saved" ){
			
			/*
			 * we're just running the user's saved card, which is referenced by the customer account
			 */
			$customer= $user->customer();
			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' Use the Customers Saved Card');
			
		}
		else if( $request->input('source') == "captured" ){
			/*
			 * users are assigend a stripe customer ID after they post their first (free) item.
			 * 		If they don't have a saved credit card (customer->source) or they choose not to use the saved card,
			 * 		this code will attach the token from the card entered in the form to their customer object, replacing the old.
			 * 
			 * By conecting this credit card token tothe customer - EVEN if we aren't going to "save" the card for the user to reuse,
			 * 		we make sure that all transactions for the user are attached to the same Customer 
			 */
			
			if($user->stripeIsActive() == FALSE){
				$user->createStripeCustomer();
			}
			
			$customer= $user->customer();
			$token= $request->input('stripeToken');
			$customer->source= $token;					//this attaches the card entered by the user to the cutomer account we created after they posted thier first (free) item
			$customer->save();							//the card is referenced via $customer->default_source
														//	ex: $card = $customer->sources->retrieve( $customer->default_source ); //get the card which we just attached to the cutomer via the token

			//now check if the user wnats to save this card
			//		Again, we ALWAYS connect the card to the customer, but the user
			//		also gets to choose whether they want the option to pay with the same card.
			$user->save_card = ($request->has('save_card')) ? 1 : 0;
			$user->save();
			
			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' Use the new card we just captured');
		}
		
		/**
		 ************************************************************************************************
		 */
		
		
		
		
		/**
		 *****************		Run the Stripe Transaction Info...		****************************
		 */
		
		if($type == "subscribed"){
			/*
			 * this means the user has an active subscription... but let's make sure
			*
			*/
				
			if( $user->hasSubscription() == FALSE ){
			
				$error_number= "#5".str_pad($user->id, 5, '0', STR_PAD_LEFT).'-'.str_pad($item->id, 10, '0', STR_PAD_LEFT);
				\Log::error('PROCESS TRANSACTION ERROR '.$error_number.' :: User tried to post using subscription token without active subscription. User: '.$user->id.' Item:'.$item_.id);
				$flash_message= 'Uh oh! You don\'t have an active subscription. If you believe this is a mistake, we\'ll do everything we can to make it right. Please contact info@unclebucks.co with this error message:<br /><br /><b>ERROR '.$error_number.' : No active subscription.</b>';
			
				Session::flash('message', $flash_message);
				Session::flash('alert-class', 'bg-ub-error');
			
				return( redirect(url('home')));
			}
			

			//try to renew the item, if one was set
			if(!empty($item)){
				//calculate and set the item's new expiration date
				$item->expires_at = $item->calculateExpiration();
					
				$item->save();
				
				\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' The user is subscribed, and item has been renewed to: '.$item->expires_at->format('F d, Y'));
			}
			
			
		}
		else if($type == "charge"){
			
			if( $plan == "one" ){
				$credits= 1;
			}
			
			$stripe_data= [
				'customer' => $customer,
				'description' => $request->input('title'),
				'metadata'=> array(
					"credits" => $credits,
					"email" => $item->user->email,
					"item_id" => $item->id,
				),
			];
			
			$amount= $request->input('amount');
			
			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' Try a Charge of $'.$amount.' for item #'.$item->id);
			
			$response= $item->user->charge($amount, $stripe_data);
				
			if($response == FALSE ){
				\Log::error('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' One Time Charge FAILED!');
				abort('500');
				//return redirect('payment/charge/'.$request->input('credits'));
			}
			
			
			//try to renew the item, if one was set
			if(!empty($item)){
				//calculate and set the item's new expiration date
				$item->expires_at = $item->calculateExpiration();
					
				$item->save();
			}
				

			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' One Time Charge $'.$amount.' for item:'.$item->id.' expires_at:'.$item->expires_at->format('M d, Y').' charge_id:'.$response->id);
			
			
		}
		else if ($type == "subscription"){
			
			$current_subscription= "";
			
			if($user->hasSubscription() ){
				if($user->stripe_plan == $plan) {
				
					\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' Type is Subscription, but user already has an active subscription.');
					
					// flash the user to let them know they are already subscribed
					$flash_message='You are already subscribed to an unlimited '.$user->stripe_plan.' plan. Your card hasn\'t been charged.';
					Session::flash('message', $flash_message);
					Session::flash('alert-class', 'bg-ub-inverse');
		
					return( redirect(url('home')));
				}
				
				$current_subscription= $user->stripe_subscription;
			}
			
			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' Try a create a new subscription of plan '.$plan);
				
			//try to create the subscription
			$user->subscription( $plan )->create(null, [], $user->customer() );
            $subscription= $user->getSubscription();
			$plan= $subscription->plan;
			
			if( ! $user->hasSubscription() || $current_subscription == $user->stripe_subscription){
				\Log::error('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' Create Subscription FAILED');
				abort('500');
			}
			
			\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.' New Subscription is SUCCESSFUL #'.$user->stripe_subscription);
			
			
			//subscription has been created - now renew any expired items
			$expired_items= $user->items("expired");
			foreach($expired_items as $i){
				$i->expires_at= $i->calculateExpiration();
				$i->save();
				\Log::info('PROCESS TRANSACTION User ['.Auth::user()->id.'] '.Auth::user()->email.'________ renewing item: '.$i->id.' '.$i->expires_at->format('M d, Y'));
			}
			
			\Log::info('Subscription complete for user:'.$user->email.' subscription_id:'.$user->stripe_subscription);
				
			
			
			
		}

		/**
		 ************************************************************************************************
		 */
		
		
		// flash a confirmation message to the user
		$flash_message="";
		
		switch($type) {
			case "subscribed":
				$flash_message= 'Yeeehaw! Your '.$item->title.' has been posted to Uncle Bucks. Make sure your contact preferences and location are up to date.';
				break;
			
			case "charge": 
				if( Session::get('new_item', FALSE) ){ //this is a new item, so display the confirmation message and go to account
					$flash_message= 'Well thank ya kindly! We received your payment, and your '.$item->title.' has been posted to Uncle Bucks. Make sure your contact preferences and location are up to date.';
					Session::forget('new_item');
				}
				else {
					$flash_message='Well thank ya kindly! We received your payment, and your '.$item->title.' will be posted on Uncle Bucks until '.$item->expires_at->toFormattedDateString().'.';
				}	
                
                $subject="Uncle Bucks Payment Confirmation";
                
                try{ 
                    Mail::send('emails.inlined.payment-charge', ['item'=>$item, 'amount'=>($amount/100), 'confirmation_id'=>$response->id], function($message) use ($user, $subject)
                    {
                        $message->to($user->email)->subject($subject);
                    });
                }
                catch(GuzzleHttp\Exception\ClientException $e){
                    \Log::error('Exception while attempting to send email to confirm that payment has been received for user: '.$user->id);
                }   
				
				break;
				
			case "subscription": 
				$flash_message='YEEHAW! You are now subscribed to the '.$plan->name.' plan, so start posting items right away!';
                
                $subject="Uncle Bucks Subscription Confirmation";
                
                try{
                    Mail::send('emails.inlined.payment-subscription', ['subscription' => $subscription, 'plan'=>$plan->name, 'amount' => ($plan->amount / 100), 'confirmation_id'=>$user->stripe_subscription], function($message) use ($user, $subject)
                    {
                        $message->to($user->email)->subject($subject);
                    });
                }
                catch(GuzzleHttp\Exception\ClientException $e){
                    \Log::error('Exception while attempting to send email to confirm that subscription has been purchased for user: '.$user->id.' and confirmation id: '.$user->stripe_subscription);
                }
                
				break;
		}
		
		Session::flash('message', $flash_message);
		Session::flash('alert-class', 'bg-ub-inverse');
		
		
		
		return( redirect(url('home')));
		
	}
	
	
	
	public function manage_subscription(){
		
		if( Auth::guest() ){
			return redirect('home');
		}
		
		\Log::info('Manage Subscription User ['.Auth::user()->id.'] '.Auth::user()->email);
		
		$user= Auth::user();
		
		$subscription= $user->getSubscription();
		$plan= $subscription->plan;
	
		$amount = $plan->amount / 100;
		
		
		
		//get the saved card, if the user has one saved
		//if the user does not want to use a card that has been saved, savedCard() also returns FALSE
		$saved_card= Auth::user()->savedCard();
		
		//usr has an active subscription
		if($user->hasSubscription() && $user->cancelled() == FALSE){
				
			\Log::info('Manage Subscription User ['.Auth::user()->id.'] '.Auth::user()->email).' User has an active subscription';
			
			return view('payment.active_subscription', compact('item', 'user', 'subscription', 'plan', 'amount', 'saved_card'));
			
		}
		
		//user has a Cancelled subscription
		else if($user->hasSubscription() && $user->cancelled()){
			
			\Log::info('Manage Subscription User ['.Auth::user()->id.'] '.Auth::user()->email).' User has a cancelled subscription';

			//$subscription->cancelled_at= \Carbon\Carbon::today();
			return view('payment.cancelled_subscription', compact('item', 'user', 'subscription', 'plan', 'amount', 'saved_card'));
				
		}
		
	}
	
	
	public function cancel_subscription($id){
	
		$user= Auth::user();
	
		if( empty($user) || ( ($user->id != $id) && ($user->hasRole('admin') == FALSE) ) ){
			return redirect('home');
		}
	
		\Log::info('Cancel Subscription User ['.Auth::user()->id.'] '.Auth::user()->email);
        
		$subscription_ends_at= $user->getSubscription()->current_period_end;
        
        
		//cancel the current plan
		$user->subscription( )->cancel();
		
		$plan= $user->getStripePlan();
	
		if($user->cancelled() == FALSE){ //something went wrong...
			\Log::error('Cancel Subscription: Unable to cancel subscription for user '.$user->id.' sub#'.$user->stripe_subscription);
				
			// flash a confirmation message to the user
			$flash_message='Hmmmm, something went wrong. We weren\'t able to cancel your subscription. Why don\'t you try again?';
			Session::flash('message', $flash_message);
			Session::flash('alert-class', 'bg-ub-inverse');
				
			return redirect(url('home'));
		}
	
		\Log::info('Cancel Subscription: Subscription cancelled for user '.$user->id);
		
		// flash a confirmation message to the user
		$flash_message='Your '.$plan->name.' subscription has been cancelled. We\'re sorry to see you go, but you\'re always welcome back!';
		Session::flash('message', $flash_message);
		Session::flash('alert-class', 'bg-ub-inverse');
        
        
        $subject="Uncle Bucks Subscription Has Been Cancelled";
        
        try{
            Mail::send('emails.inlined.cancel-subscription', ['subscription_ends_at' => $subscription_ends_at, 'plan'=>$plan ], function($message) use ($user, $subject)
            {
                $message->to($user->email)->subject($subject);
            });
        
        }
        catch(GuzzleHttp\Exception\ClientException $e){
            \Log::error('Exception while attempting to send email to confirm that subscription has been cancelled for user: '.$user->id);
        }
        
		return redirect(url('home'));
	}
	
	
	
	public function reactivate_subscription($id){
		
		$user= Auth::user();
		
		if( empty($user) || ( ($user->id != $id) && ($user->hasRole('admin') == FALSE) ) ){
			return redirect('home');
		}
		
		\Log::info('Reactivate Subscription User ['.Auth::user()->id.'] '.Auth::user()->email);
		
        $subscription_ends_at= $user->subscription_ends_at;
        if( $subscription_ends_at->lt(\Carbon\Carbon::today() ) ){
            return redirect(url('payment/subscription/monthly'));
        }
        
        
        
		//resume the current plan with the saved token
		$plan= $user->getSubscription()->plan;
		$user->subscription( $plan )->resume();
		
		if($user->cancelled() == TRUE){ //something went wrong...
			\Log::error('Reactivate Subscription: Unable to reactivate subscription for user '.$user->id);
			
			// flash a confirmation message to the user
			$flash_message='Hmmmm, something went wrong. We weren\'t able to reactivate your subscription. Why don\'t you try again?';
			Session::flash('message', $flash_message);
			Session::flash('alert-class', 'bg-ub-inverse');
			
			return redirect(url('home'));
		}
		
		\Log::info('Reactivate Subscription: Subscription reactivated for user '.$user->id);
		
		// flash a confirmation message to the user
		$flash_message='YEEHAW! Your '.$plan->name.' subscription has been reactivated, so start posting items right away!';
		Session::flash('message', $flash_message);
		Session::flash('alert-class', 'bg-ub-inverse');
		
        $subject="Uncle Bucks Subscription Has Been Re-Activated";
        try{
            Mail::send('emails.inlined.reactivate-subscription', ['subscription_ends_at' => $subscription_ends_at, 'plan'=>$plan, 'amount' => ($plan->amount / 100) ], function($message) use ($user, $subject)
            {
                $message->to($user->email)->subject($subject);
            });
        }
        catch(GuzzleHttp\Exception\ClientException $e){
            \Log::error('Exception while attempting to send email to confirm that subscription has been re-activated for user: '.$user->id);
        }
        
		return redirect(url('home'));
	}
	
	
	
	
	public function manage_card(){
		
		if( Auth::guest() ){
			return redirect('home');
		}
		
		$user= Auth::user();
		//get the saved card, if the user has one saved
		//if the user does not want to use a card that has been saved, savedCard() also returns FALSE
		$saved_card= $user->savedCard();
		
		
		return view('payment.manage-card', compact('saved_card'));
	
	}
	
	
	public function update_card(Request $request){
		
		if( Auth::guest() ){
			return redirect('home');
		}
		$user= Auth::user();
		
		\Log::info('Update Credit Card Details: user '.$user->id);
		
		if($request->has('stripeToken') == FALSE ){
			$flash_message='Uh oh! We weren\'t able to update your payment details. Please try again.';
			Log::error('Update Card failed. No stripe token returned. user:'.$user->id);
			Session::flash('message', $flash_message);
			Session::flash('alert-class', 'bg-danger');
			return redirect('home');
		}
		
		$customer= $user->customer();
		$token= $request->input('stripeToken');
		$customer->source= $token;					//attach the card entered by the user to the cutomer account 
		$customer->save();							
		
		//managing a subscription, so ALWAYS save the card to the user account
		$user->save_card = 1;
		$user->save();
		
		\Log::info('Update Credit Card Details: SUCCESS for user '.$user->id);
		
		
		$flash_message='Your payment details have been updated.';
		Session::flash('message', $flash_message);
		Session::flash('alert-class', 'bg-ub-inverse');
		return redirect('home');
	}

}
