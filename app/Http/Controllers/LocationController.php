<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use Illuminate\Http\Request;
use App\Location;
use Illuminate\Support\Facades\Auth;


class LocationController extends Controller {

	
	public function geocode(Request $request)
	{
		
		if(\Request::isMethod('post') && \Request::ajax())
		{
			
			$id= $request->input('id', 0);
			$type= $request->input('type', '');
			

			$data['lat']= $request->input('lat');
			$data['lng']= $request->input('lng');
			$data['address']= $request->input('address');
			
		
			// check which Model we are connecting this Location to 
			switch($type){
				case "item":
					$resource= \App\Item::find($id);
					break;
					
				case "user":
					$resource= \App\User::find($id);
					break;
					
				default:
					abort(404);
					break;
			}
			
			if( ! $resource ){
				abort(404);
			}
			
			
			if( $loc= $resource->location ) { 
				 
				$loc->update($data);
				$loc->save();
				
				$msg= "the location attached to the ".$type." was updated with new geolocation data";
			}
			else {
		
				$loc= Location::create($data);
				
				//follow this format to save the relationshiip!   $item->location()->save($loc);
				$resource->location()->save($loc);
				
				$msg= "a new location was created and attached to the ".$type;
			}
			
			
			
			
			
			if( ! $loc ) {
				return response()->json(['error'=>true, 'msg'=>'couldnt make location']);
			}
			
			return response()->json(['success' => true, 'location_id'=>$loc->id, 'msg' => $msg ]);
			
		}
	}	
	
	
	
	
	
	
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return view('location.google');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($resource="user", $id="")
	{
		//
		$user= Auth::user();
		
		if( ! $user ){
			abort(404);
		}
		
		$redirect_url= "home";
		if($resource == "item"){
			$redirect_url= "item/".$id."/edit";
		}
		
		return view('location.create', compact('user', 'redirect_url'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		//if(\Request::isMethod('post') && \Request::ajax())
		//{
			
			$id= $request->input('id', 0);
			$type= $request->input('type', '');
			

			$data['lat']= $request->input('new-lat');
			$data['lng']= $request->input('new-lng');
			$data['address']= $request->input('new-address');
			$redirect_url= $request->input('redirect_url');
			
		
			// check which Model we are connecting this Location to 
			switch($type){
				case "item":
					$resource= \App\Item::find($id);
					break;
					
				case "user":
					$resource= \App\User::find($id);
					break;
					
				default:
					abort(404);
					break;
			}
			
			if( ! $resource ){
				abort(404);
			}
			
		
			
			if( $loc = $resource->location ) { 
				 
				$loc->update($data);
				$loc->save();
				
				if ($type == "user"){
					$msg= "Alright, we updated your location to the new spot.";
				}
				else if($type == "item"){
					$msg= "Ok, we updated your item to the new location.";
				}
			}
			else {
		
				$loc= Location::create($data);
				
				//follow this format to save the relationship!   $item->location()->save($loc);
				$resource->location()->save($loc);
				
				if ($type == "user"){
					$msg= "We've saved this location for you. We'll use this as your default location whenever you are signed in to Uncle Bucks.";
				}
				else if($type == "item"){
					$msg= "Great, we saved your item's location.";
				}
			}
			
			
				
			Session::flash('message', $msg);
			Session::flash('alert-class', 'bg-ub-inverse');
			
			
			return redirect($redirect_url); 	
			
		/*}

		Session::flash('message', 'Sorry! Something went wrong while we tried to set your location. You\'ll need to try again.');
		Session::flash('alert-class', 'bg-ub-danger');
			
		return redirect('home');
		*/
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
