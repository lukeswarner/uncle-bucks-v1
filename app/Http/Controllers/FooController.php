<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FooController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//return view('foo.index');
		
		$categories= \App\Category::lists('name', 'id');  	//lists gets all() the categories and formats them as [ 'id' => 'name', ...] for dropdowns!
		$categories[" "]="Category";
		$default_category_id= " ";
		
		
		$items= \App\Item::withKeyword('artisan')->latest()->paginate(40);
		$items->setPath( url('isotope') );
		
		
		return view('item.isotope', compact('categories', 'default_category_id', 'items'));
	}

	
	public function filter(Request $request)
	{
		//
		//dd('post filter');
		
		$categories= \App\Category::lists('name', 'id');  	//lists gets all() the categories and formats them as [ 'id' => 'name', ...] for dropdowns!
		$categories[" "]="Category";
		$default_category_id= " ";
		
		
		$category_id= false;
		
		
		
		if ($request->isMethod('post'))
		{
			$input= $request->all();
			if( !empty($input['category_id']) ){
				$category_id= $input['category_id'];
				$default_category_id= $input['category_id'];
					
		
			}
		}
		if ($request->isMethod('get'))
		{
			//dd('get');
			// ... this is GET method
			$items= \App\Item::withCategory()->latest()->paginate(40);
		}
		
		$items= \App\Item::withCategory($category_id)->latest()->paginate(40);
		
		
		
		
		
		
		return view('static.unclebucks', compact('categories', 'default_category_id', 'items'));
	}
	
	
	public function menu()
	{
		
		
		return view('static.menu');
	}
	

	public function glyphicons()
	{
	
	
		return view('foo.glyphicons');
	}

	
	
	
	

	public function ajax(Request $request)
	{
		if ($request->isMethod('post')) {
			$c_id= $request->input('category_id');
			$filter= $request->input('filter');
	
			$category= \App\Category::find($c_id);
			
			
			$keyword= $request->input('kw');
			
		}
		else if ($request->isMethod('get')) {
			
			$category= "";
			$keyword= "";
			$filter="recent";
		}
		
	
		return view('category.ajax', compact('category','keyword', 'filter'));
	}
	
	
	public function ajaxshow()
	{
	
	
		return view('category.ajax', compact('category'));
	}
}
