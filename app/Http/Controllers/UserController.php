<?php namespace App\Http\Controllers;

/**
 * @issues
 * UB-7 Update User Account (sub-table of UB-1 User CRUD) [2015-06-13]
 */

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $username
	 * @return Response
	 */
	public function show($id)
	{
		//UB-7 Update User Account
		//$user= User::where("username", $username)->first();
		
		$user= User::find($id);		//UB-139 Remove username
		
		if( ! $user ){
			abort(404);
		}
		
		return view('user.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $username
	 * @return Response
	 */
	public function edit($id)						/* UB-7 Update User Account (sub-table of UB-1 User CRUD) [2015-06-13] */	
	{
		//UB-7 Update User Account
		//$user= User::where("username", $username)->first();
		$user= User::find($id);		//UB-139 Remove username
		
		if( ! $user ){
			abort(404);
		}
		
		$contact_set= TRUE;
		if( $user->contact_email == FALSE && $user->contact_phone == FALSE && $user->contact_sms == FALSE ){
			$contact_set= FALSE;
		}
		return view('user.edit', compact('user', 'contact_set'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)		/* UB-7 Update User Account (sub-table of UB-1 User CRUD) [2015-06-13] */	
	{
		// Do User validation differently for each method, since Create, Update, and UpdatePassword 
		// all need to validate uniquely 
		$this->validate($request, [	
				//'username' => 'required|max:255|unique:users,username,'.$id,	 	//UB-139 Remove username
				'email' => 'required|email|max:255|unique:users,email,'.$id,
				'phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
		]);
		
		
		
		
		$user= User::find($id);
		
		$data= $request->all();
	
		$data['contact_email'] = ($request->has('contact_email')) ? 1 : 0;
		$data['contact_phone'] = ($request->has('contact_phone')) ? 1 : 0;
		$data['contact_sms'] = ($request->has('contact_sms')) ? 1 : 0;
		$data['contact_notification'] = ($request->has('contact_notification')) ? 1 : 0;
		
		
		if( empty( $data['phone'] ) ){							//UB-133 Phone Optional for Register
			$data['contact_phone']= FALSE;
			$data['contact_sms']= FALSE;
		}
		
		
		$user->update($data);
		
		Session::flash('message', 'Your contact information was updated.');
		Session::flash('alert-class', 'bg-ub-inverse');
		
		//return redirect('user/'.$user->username.'/edit');		//UB-139 Remove username
		return redirect('home'); 								//UB-141 Redirect to Account after saving Contact Preferences
	}
	

	
	/**
	 * Show the form for editing the users password.
	 *
	 * @param  int  $username
	 * @return Response
	 */
	public function edit_password()						/* UB-125 User can change their password [2015-11-16] */
	{
		$user= \Auth::user();
	
		if( ! $user ){
			abort(404);
		}
	
		return view('user.password', compact('user'));
	}
	
	
	public function update_password(Request $request)		/* UB-125 User can change their password [2015-11-16] */
	{
		$this->validate($request, ['password' => 'required|confirmed',]);
	
		$user= \Auth::user();
	
		if( ! $user ){
			abort(404);
		}
	
		$user->password = bcrypt($request->password);
		$user->save();
	
		Session::flash('message', 'Your password was successfully changed.');
		Session::flash('alert-class', 'bg-ub-inverse'); 
		
		return redirect('home');
	}
	
	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
