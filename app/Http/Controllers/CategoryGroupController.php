<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\CategoryGroup;

class CategoryGroupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//we should never get here- but if we do, redirect to the CATEGORY 
		//controller's index to list all Categorys and CategoryGroups
		return redirect(url('category'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('categorygroup.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * 
	 * @param  Requests\CategoryGroupRequest request
	 * @return Response
	 */
	public function store(Requests\CategoryGroupRequest $request)
	{
		//validation is run first, before this method is called.
		$input= $request->all();

		CategoryGroup::create($input);
		
		return redirect(url('category'));   //use the Category's index to list all Categories and groups
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$category_group= CategoryGroup::where('slug', $slug)->first();
		
		if(! $category_group){
			//abort(404);
			return redirect(url('category'));
		}
		
		return view('categorygroup.edit', compact('category_group'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param  Requests\CategoryGroupRequest $request
	 * @return Response
	 */
	public function update($id, Requests\CategoryGroupRequest $request)
	{
		$category_group= CategoryGroup::find($id);
		
		$category_group->update($request->all());
		
		return redirect(url('category'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		CategoryGroup::destroy($id);
		
		return redirect(url('category'));
	}

}
