<?php namespace App\Http\Controllers;


/**
 * @issues
 * UB-19 Item Model [2015-06-22]
 *
 *
 * */

use Auth;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CreateItemRequest; //used to run input through validation, and replaces Request
use App\Item;
use Illuminate\Support\Facades\Redirect;
use Image;
use Illuminate\Support\Facades\Cache;
use App\Repositories\ItemRepositoryInterface;
use Mail;
use Stripe\Plan;


class ItemController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(ItemRepositoryInterface $itemRepository)
	{
		$this->itemRepository= $itemRepository;
	}
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$items= Item::withCategory()->latest()->paginate(40);
		$items->setPath('/ub/items'); 	//pagination path
		return view('item.grid', compact('items'));
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function info()
	{
	
		return view('item.info');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category_groups= \App\CategoryGroup::lists('name','id');  	//lists gets all() the category_groups and formats them as [ 'id' => 'name', ...] for dropdowns!
		$category_groups[" "]=" ";
		
		Session::put('new_item', TRUE);
		
		return view('item.create', compact('category_groups'));
	}
	
	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateItemRequest $request){
		
		//validation is run first, before this method is called.
		
		$input= $request->all();


		$input['expires_at']= null; //leave expiration date blank for now - it is set once a user activates, pays, and publishes an Item!
		
		$input['user_id']= Auth::user()->id;
		
		$item= Item::create($input);
	
		
		return redirect(url('item/'.$item->id.'/edit'));		
		//return( redirect(url('item/'.$item->id.'/creating')));			/* UB-36 Streamline Create Item Workflow - 2015-08-28 */
		
	}
	
	
	
		

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		/*
		$item = Cache::remember('item.'.$id, 4, function() use ($id) {
			return Item::find($id);
		});
		*/
		$item= Item::find($id);
		
		if( !$item ) {
			abort('404');
		}
		
        if( $item->is_expired() ) {
			abort('405');
		} 
        
         if( $item->is_flagged() ) {
			abort('406');
		}
        
		/* UB- 133 Item owner always sees Edit view */
		//if the signed-in user is the owner, send them straight to the edit page
		$user= Auth::user();
		if( !empty($user) && $item->user == $user){
			return( redirect(url('item/'.$item->id.'/edit')));
		}
		
		/* UB- 133 Item owner always sees Edit view */
		//item owner will never get here, BUT - still check if editable for admins
		$editable= $item->checkEditable( Auth::user() );

		
		return view('item.show', compact('item', 'editable'));
	}

	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		\Log::info('editing item '.$id);
		$item= Item::find($id);
		
		if( !$item) {
			abort('404');
		}
		
		
		if( ! $item->category ){
			\Log::info('editing item '.$id.' Set Category!');
			return( redirect(url('item/'.$item->id.'/edit/category')));
		}
		else if( !$item->photo  ){
			\Log::info('editing item '.$id.' Set Photo!');
			return( redirect(url('item/'.$item->id.'/edit/photo')));
		}
		else if( !$item->user->location  ){
			\Log::info('editing item '.$id.' Set Location!');
			return( redirect(url('location/item/'.$item->id)));
		}
		else if( is_null( $item->expires_at )  ){
			\Log::info('editing item '.$id.' Publish!');
			return( redirect(url('item/'.$item->id.'/publish')));
		}
		else {
			//this is a previously existing item, so edit it
			\Log::info('editing item '.$id.' nothing missing. go to edit page.');
			
			return view('item.edit', compact('item'));
			
		}
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, CreateItemRequest $request)
	{
	
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}
	
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		
		\Log::info($request->all());
	
		$item->update($request->all());
	
		\Log::info('period: '.$item->rental_period);
		
		//return view('item.edit', compact('item'));
		return( redirect(url('item/'.$id.'/edit')));
	}
	
	
	
	
	
	
	/*** ======== Category ======== ***
	 * 
	 */
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function edit_category($id)
	{
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}
		
		/*
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		*/
		$category_groups= \App\CategoryGroup::all();  	//lists gets all() the category_groups and formats them as [ 'id' => 'name', ...] for dropdowns!

		return view('item.category', compact('item', 'category_groups'));
	}
	
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update_category($id, Request $request)
	{
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}
	
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		
		$this->validate($request, [
				'category' => 'required'
		]);
	
		$new_category= \App\Category::find($request->input('category'));
		if( ! $new_category ){
			abort('404');
		}
	
		$item->category_id= $new_category->id;
		$item->save();
	
	
		return( redirect(url('item/'.$id.'/edit')));
	}
	
	/* 
	 *
	 *** ======== Category ======== ****/
	
	
	
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	/*
	 * Items no longer have a location - this is connected to the user. 	UB-131 User Can Save Location  && UB-149 Finalize Item Creation Process
	 * 
	public function edit_location($id)
	{
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}
		

		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		
		
		return view('item.location', compact('item'));
	}
	*/
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function edit_photo($id)
	{
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}	
			
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		
		return view('item.photos', compact('item'));
	}
	
	
	
	
	

	
	
	public function upload_photo($id, Request $request){
		$item= Item::find($id);
		
		
		if( ! $item ){
			abort('404');
		}
		
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		
		$messages = [
			'photo.max' => 'Please choose a photo that is less than 5 MB.',
		];
		
		$this->validate($request, [
				'photo' => 'image|max:5000|required'		/* 5 MB */
				]);
		
		if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
			
			$uploaded_file= $request->file('photo');
			

			$upload_folder= "uploads/item/";
			$destinationPath= public_path($upload_folder);
			$fileName = str_random(20);
			$ext= '.'.$uploaded_file->getClientOriginalExtension();
			//$fileName = str_random(20);
			
			
		if( \App::isLocal() == FALSE ):
			\Log::info('upload photo: make image on dev');
			$image= Image::make($uploaded_file)->orientate();		// added orientate() method to auto-rotate photos  UB-121 Photos not rotating properly 
			$image_tb= Image::make($uploaded_file)->orientate();	// added orientate() method to auto-rotate photos  UB-121 Photos not rotating properly 
			$image_xs= Image::make($uploaded_file)->orientate();	// added orientate() method to auto-rotate photos  UB-121 Photos not rotating properly 
		else :
			\Log::info('upload photo: make image on local ');
			$image= Image::make($uploaded_file);		// local windows PHP can't hande exif data!!!
			$image_tb= Image::make($uploaded_file);
			$image_xs= Image::make($uploaded_file);
		endif;
		
		
		\Log::info('upload photo: prepare to resize');
			$aspect= $image->width() / $image->height();
			
			if( $aspect > 1.15 ){  // this is a wide photo
				
				\Log::info('upload photo: resize to widescreen');
				//$image->fit(324, 243);
				$image->fit(468, 350);
				$image_tb->fit(240, 180);
				$image_xs->fit(100, 75);
				
			}
			else if( $aspect < 0.85  ){
				\Log::info('upload photo: resize to tall');
				//$imaged->fit(324, 432);
				$image->fit(468, 623);
				$image_tb->fit(240, 320);
				$image_xs->fit(100, 133);
			}
			else {
				\Log::info('upload photo: resize to square');
				//$image->fit(324, 324);
				$image->fit(468, 468);
				$image_tb->fit(240, 240);
				$image_xs->fit(100, 100);
			}
			
			\Log::info('upload photo: saving photo and thumbnail to fileserver');
			$image->save($destinationPath.$fileName.$ext, 90);
			$image_tb->save($destinationPath.$fileName.'_tb'.$ext, 60);
			$image_xs->save($destinationPath.$fileName.'_xs'.$ext, 60);
			
			
			$item->photo= $upload_folder.$fileName.$ext;
			$item->thumbnail= $upload_folder.$fileName.'_tb'.$ext;
			$item->photo_xs= $upload_folder.$fileName.'_xs'.$ext;
			$item->save();
			
		}
		
		return( redirect(url('item/'.$id.'/edit')));

	}
	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}

		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}
		
		Item::destroy($id);
		
		return redirect(url('user/'.Auth::user()->username));
	}
	
	

	/**
	 * begin publishing process by loading the apprpriate publishing view for an item. 
	 * @param int $id of App\Item
	 * @return 
	 */
	public function publish($id){
	
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}
	
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			\Log::info('Unauthorized user ['.Auth::user()->id.'] '.Auth::user()->email.' tried to publish item '.$item->id);
			return redirect(url('item', $id));
		}
		\Log::info('PUBLISH ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' is beginning publishing process');
		
		/*   UB-130 User can pay */
		$user= $item->user;
		
		
		if($user->test){
			//check if this is the first item the user has EVER posted
			if( $user->customer() == FALSE ) {	//do they have a stripe customer id?
				
				\Log::info('PUBLISH ITEM #'.$item->id.': ['.Auth::user()->id.'] '.Auth::user()->email.' is first item, so post for free');
				return view('item.publish.first', compact('item'));
			}
			
			//check if the user has an active subscription 
			else if( $user->hasSubscription() ){
				Session::put('publish_item_id', $item->id);
				
				$subscription= $user->getSubscription();
				$plan= $subscription->plan;
				
				\Log::info('PUBLISH ITEM #'.$item->id.': ['.Auth::user()->id.'] '.Auth::user()->email.' has an active subscription ');
				return view('item.publish.subscription', compact('item', 'user', 'subscription', 'plan'));
			}
			
			//all other options- offer the choice of payment options
			else {
				Session::put('publish_item_id', $item->id);
				
				\Log::info('PUBLISH ITEM #'.$item->id.': ['.Auth::user()->id.'] '.Auth::user()->email.' has to PAY');
				return view('item.publish.publish-feb', compact('item'));
			}
		}
		else {
			\Log::info('PUBLISH ITEM #'.$item->id.': ['.Auth::user()->id.'] '.Auth::user()->email.' is BETA tester, so posting for free');
			return view('item.publish.publish-feb', compact('item'));
		}
		//this is used to publish free items during BETA
		//return view('item.publish.publish', compact('item'));
	}
	
	
	/**
	 * Complete the Publishing of an Item.
	 * By this point, all payment processing has been completed by Stripe
	 * 		and the transation information will be POSTed here
	 * 
	 * @param int $id of App\Item
	 * @return 
	 */
	public function publish_item($id, Request $request){
		$item= Item::find($id);
		
		if( ! $item ){
			abort('404');
		}
		
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			\Log::info('Publish Item: Auth User is not the Item Owner '.$item->user->id);
			return redirect(url('item', $id));
		}
		
		\Log::info('PUBLISH ITEM #'.$item->id.': ['.Auth::user()->id.'] '.Auth::user()->email.' has completed payment ');
		
		
		
			
		
		$flash_message="";
		$payment= $request->input('payment');
		
		switch($payment){
			
			//this is the user's first posted item
			case "first":
				
				\Log::info('PUBLISH ITEM #'.$item->id.': First Item for User '.$item->user->id);
				//assign this user a Stripe Customer ID
				$item->user->createStripeCustomer();
				
				
				$flash_message= 'Yeeehaw! Your '.$item->title.' has been posted to Uncle Bucks. Make sure your contact preferences and location are up to date.';
				Session::forget('new_item');
				
				
				break;
				
			case "subscribed":
				\Log::info('PUBLISH ITEM #'.$item->id.': Active Subscription for User '.$item->user->id);
				
				break;
				/////////////////////////////////////////////////////////////////////////////////////////
			
			//the user processed a one-time charge to post or renew an iem
			case "charge":
				\Log::info('PUBLISH ITEM #'.$item->id.': CHARGE SUCCESSFUL for User '.$item->user->id);
				
				if( Session::get('new_item', FALSE) ){ //this is a new item, so display the confirmation message and go to account
					$flash_message= 'Well thank ya kindly! We received your payment, and your '.$item->title.' has been posted to Uncle Bucks. Make sure your contact preferences and location are up to date.';
					Session::forget('new_item');
				}
				else {
					Session::flash('message', 'Well thank ya kindly! We received your payment, and your '.$item->title.' will be posted on Uncle Bucks until '.$item->expires_at->toFormattedDateString().'.');
				}
			
				
				break;
				/////////////////////////////////////////////////////////////////////////////////////////
				
			case "subscription":
				
				\Log::info('PUBLISH ITEM #'.$item->id.': NEW SUBSCRIPTION SUCCESSFUL for User '.$item->user->id);
				
				if( Session::get('new_item', FALSE) ){ //this is a new item, s display the confirmation message and go to account
					$flash_message= 'Ain\'t that fantastic! Your '.$item->title.' has been posted. With your new "Unlimited Monthly" subscription, you can post as many items as you want on Uncle Bucks without any additional charge. Even better, your items will automatically renew every month!';
					Session::forget('new_item');
				}
				else {
					$flash_message= 'Ain\'t that fantastic! Your '.$item->title.' has been renewed. With your new "Unlimited Monthly" subscription, you can post as many items as you want on Uncle Bucks without any additional charge. Even better, your items will automatically renew every month!';
				}
	
				
				break;
				/////////////////////////////////////////////////////////////////////////////////////////
				
			default:
				
				//abort(500, 'Invalid payment type sent to publish_item: "'.$payment.'" for item "'.$item->id.'"');
				break;
		}
		
		
		//calculate and set the item's new expiration date
		$item->expires_at = $item->calculateExpiration();
		
		$item->save();			
		
		\Log::info('PUBLISH ITEM #'.$item->id.': Expiration updated to '.$item->expires_at->format('F m, Y'));
		
		// flash a confirmation message to the user
		Session::flash('message', $flash_message);
		Session::flash('alert-class', 'bg-ub-inverse');
		
		
		
		return( redirect(url('home')));
		
	}
	
	
	/**
	 * Renew an existing item
	 * @param int $id of App\Item
	 * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>|Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
	 */
	public function renew($id){
		
		$item= Item::find($id);
		
		if( ! $item ){
			abort('404');
		}
		\Log::info('RENEW ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' is beginning renewal process');
		
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			\Log::info('RENEW ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' does not have access to renew');
			return redirect(url('item', $id));
		}
		
		if( $item->user->hasSubscription() ){
			\Log::info('RENEW ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' has an active subscription already...');
			return redirect(action('PaymentController@manage_subscription'));
		}
		
		
		if($item->user->test){
			\Log::info('RENEW ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' is BETA TESTER, so publish for free');
			Session::put('publish_item_id', $item->id);
			return view('item.publish.renew', compact('item'));
		}
		else {
			\Log::info('RENEW ITEM #'.$item->id.': User ['.Auth::user()->id.'] '.Auth::user()->email.' needs to pay');
			return view('item.publish.publish-feb', compact('item'));
		}
	}
	
	

	/**
	 * 		REMOVED - NOT USED IN NEW PUBLISHING SYSTEM
	 * 
	 * Process the PUBLISH or RENEW request - 
	 * this is called by both the publish form and the renew form
	 * 
	 * @param unknown $id
	 * @param Request $request
	 * @return Ambigous <\Illuminate\Routing\Redirector, \Illuminate\Http\RedirectResponse, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
	 */
	public function update_renew($id, Request $request){
		
		$item= Item::find($id);
	
		if( ! $item ){
			abort('404');
		}
	
		if ( $item->checkEditable( Auth::user() ) == FALSE ){
			return redirect(url('item', $id));
		}


		\Log::info('Item->update_renew()\r\n'.
				print_r($request->all(), TRUE).'\r\n'.
				'auto_renew: ['.$item->auto_renew.'] '
		);
				
		
		
		
		
		if( $request->has('manual_renew') ){
			
			if( is_null( $item->expires_at ) ){  //the first time this item has been published
				$item->expires_at= $item->calculateExpiration();
			}
			else {
				// display checkout form here
				$item->expires_at= $item->calculateExpiration();
			}
			\Log::info('manual_renew was clicked');
		}
		else if( $request->has('auto_renew') ){
			
			//enable or disable auto_renew
			switch ( $request->input('auto_renew') ){
				case "enable":
					$item->auto_renew= TRUE;
					break;
				
				case "disable":
					$item->auto_renew= FALSE;
					break;
					
				default:
					break;
			}
			
			//only add time to the expiration date if the item has already expired
			if( $item->is_expired() ){
				$item->expires_at= $item->calculateExpiration();
				\Log::info('auto_renew was enabled for an expired item, so we updated the expiration date');
			}
			
		}

		
		$item->save();
	
		return( redirect(url('item/'.$id.'/edit')));
	}
	//*/
	
    
    
    
    
    public function publish_flagged($id){
       
        \Log::info('re-publishing an item which has been flagged:'.$id);
        
        $item= Item::find($id);
        if(! $item ){
            abort(404);
        }
        
        $flags= $item->getFlags();
        
        //check if the item has been updated more recently than the last flag 
        if($item->updated_at->gt( $flags->last()->updated_at ) ){
            \Log::info('item '.$id.'has been updated since last being flagged, so deactivate flags and republish');
            //set the item's flag counter to 0
            $item->flags = 0;
            $item->save();
            
            //deactivate this group of flags, since (theoretically) they apply to an abuse which has been corrected.
            foreach($flags as $flag){
                $flag->active = FALSE;
                $flag->save();
            }
            
        }
        else {
            \Log::info('item '.$id.'has NOT been updated since last being flagged, so send the user back to the edit page.');
            
            Session::flash('message', 'You must update your item with the appropriate changes before your item can be republished. ');
            Session::flash('alert-class', 'bg-danger');
        }
        
        
            return redirect(url('item/'.$id.'/edit'));
        
    }
    
    
    
    
    
    
	
	public function set_favorite(Request $request){

		if(\Request::isMethod('post') && \Request::ajax()) {
			$item_id= $request->input('item');
			$user_id= $request->input('user');
			
			$item= \App\Item::find($item_id);
			$user= \App\User::find($user_id);
			
			if( ! $user || ! $item ){
				
				Log::error('error in Item->set_favorite() : user: '.$user_id.' and item '.$item_id);
				return response()->json(['error'=>true, 'msg'=>'error in Item->set_favorite() : user: '.$user_id.' and item '.$item_id]);
			}
			
			
			$user->favorite_items()->attach($item);
			
			$this->itemRepository->refreshFavoriteItems("", "", $user);		//UB-140 Invalidate Cache when changes made
			
			
			return response()->json(['success' => true, 'msg' => 'success in Item->set_favorite() : user: '.$user_id.' and item '.$item_id]);
		
		}
		else {
			return response()->json(['error'=>true, 'msg'=>'error in Item->set_favorite() : method is not POST or not AJAX']);
		}
	}
	
	
	
	public function remove_favorite(Request $request){
	
		if(\Request::isMethod('post') && \Request::ajax()) {
			$item_id= $request->input('item');
			$user_id= $request->input('user');
				
			$item= \App\Item::find($item_id);
			$user= \App\User::find($user_id);
				
			if( ! $user || ! $item ){
	
				Log::error('error in Item->remove_favorite() : user: '.$user_id.' and item '.$item_id);
				return response()->json(['error'=>true, 'msg'=>'error in Item->remove_favorite() : user: '.$user_id.' and item '.$item_id]);
			}
				
				
			$user->favorite_items()->detach($item);
				

			$this->itemRepository->refreshFavoriteItems("", "", $user);		//UB-140 Invalidate Cache when changes made
			
			
			return response()->json(['success' => true, 'msg' => 'success in Item->remove_favorite() : user: '.$user_id.' and item '.$item_id]);
	
		}
		else {
			return response()->json(['error'=>true, 'msg'=>'error in Item->remove_favorite() : method is not POST or not AJAX']);
		}
	}




	
	
	
	public function email_item_owners(){
		
		
		
		
		
		
		
		
		
		/*
		$all_items= \App\Item::all();
		
		$expired_date= \Carbon\Carbon::today();
		$expiring_date= $expired_date->copy();
		$expiring_date->addDays(5);
		
		$expiring_users = collect([]);
		$expired_users = collect([]);
		
		echo "Expired date: ".$expired_date.'<br />';
		echo "Expiring date: ".$expiring_date.'<br />';
		
		
		$expiring_items= \App\Item::expiresAt($expiring_date)->get()->sortBy('user_id');
		$expired_items= \App\Item::expiresAt($expired_date)->get()->sortBy('user_id');
		
		//echo $expiring_items."<br /><br />";
		
		if($expiring_items->count() > 0){
			$user_items= collect([]);
			$current_user= $expiring_items->first()->user;
			
			foreach ($expiring_items as $item){
				//echo "~~~ item ".$item->id.'<br />';
				if( $current_user->id != $item->user->id ){
					//echo "current: ".$current_user->id.' and item: '.$item->user->id."<br />";
					
					echo $current_user->email.' has '.$user_items->count().' expiring items: <br />';
					foreach($user_items as $i){
						echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;['.$i->id.'] '.$i->title.' <br />';
					}
					echo '<br />';
					
					$current_user= $item->user;
					//echo "++++ NEW USER: ".$current_user->id.' same as '.$item->user->id."+++++++++++<br />";
					$user_items= collect( [] );
				}
				
				$user_items->push($item);
				//echo "______pushing item ".$item->id." to user ".$current_user->email."<br />";
					
					
			}
			
			echo $current_user->email.' has '.$user_items->count().' expiring items: <br />';
			foreach($user_items as $item){
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;['.$item->id.'] '.$item->title.' <br />';
			}
			echo '<br />';
		}
		
		
		*/
		
		
		
		
		/*
		foreach ($all_items as $item){
			
			if( empty($item->expires_at) ){
				echo 'empty item : ['.$item->id.'] '.$item->title.' '.$item->user->email.'<br />';
			}
			
			else if( $expired_date->eq($item->expires_at) ){
				
				
				if( ! $expiring_users->has($item->user) ){
					$expiring_users->push($item->user);
				}
				
				
				echo 'EXPIRED : ['.$item->id.'] '.$item->title.' '.$to_email.'<br />';


				$to_email= $item->user->email;
				Mail::send('emails.expired_item', ['item'=>$item], function($message) use ($to_email)
				{
					$message->to($to_email)->subject('Your Uncle Bucks Item Has Expired Soon');
				});
				
				
			}
			
			else if( $expiring_date->eq($item->expires_at) ){
				

				if( ! $expired_users->has($item->user) ){
					$expiring_users->push($item->user);
				}
				
				
				$to_email= $item->user->email;
				
				echo 'expiring soon : ['.$item->id.'] '.$item->title.' '.$to_email.'<br />';
				
				Mail::send('emails.expiring_item', ['item'=>$item], function($message) use ($to_email)
				{
					$message->to($to_email)->subject('Your Uncle Bucks Item Is Expiring Soon');
				});
				
			}
			
		}
		*/
		
		
		
	}
}
