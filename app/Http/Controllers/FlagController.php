<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\User;
use App\Flag;
use Auth;
use Validator;
use Session;
use Mail;

use Illuminate\Http\Request;

class FlagController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($item_id, Request $request)
	{
		
        $item= Item::find($item_id);
	
		if( ! $item ){
			abort('404');
		}
        
        
        $flagger_id= "";
        if(Auth::check()){
            $flagger_id= Auth::user()->id; 
        }
        
        $ip= $request->ip();
   
        if( Flag::isDuplicateFlag($item->id, $ip, $flagger_id)) {
            Session::flash('message', 'Hold up! It looks like you already flagged this item. Don\'t be spammin\' now, alright?');
            Session::flash('alert-class', 'bg-ub-inverse');
            return redirect(url('item/'.$item->id));
        }
        
        return view('flag.create', compact('item'));
    
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($item_id, Request $request)
	{
		$messages = [
            'explanation.required' => 'We really need you to complete the more details section!',
        ];
        $rules= [
				'reason' => 'required',
                'explanation' =>'required'
		];

        $validator = Validator::make($request->input(), $rules, $messages);
		if ($validator->fails()) {
            return redirect('flag/'.$item_id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $item= Item::find($item_id);
		if( ! $item ){
			abort('404');
		}
        
        $ip= $request->ip();
        $flagger_id= "";
        if(Auth::check()){
            $flagger_id= Auth::user()->id; 
        }
        
        if( Flag::isDuplicateFlag($item->id, $ip, $flagger_id) ){
            Session::flash('message', 'Hold up! It looks like you already flagged this item. Don\'t be spammin\' now, alright?');
            Session::flash('alert-class', 'bg-ub-inverse');
            return redirect(url('item/'.$item->id));
       }
        
        
        $data= $request->all(); //gets us reason and explanation
        $data['item_id']= $item->id;
        $data['owner_id']= $item->user->id;
        $data['flagger_id']= $flagger_id;
        $data['flagger_ip']= $ip;
        
        Flag::create($data);
        $item->flags+= 1;
        $item->save();
        
        
        if($item->flags == Flag::MAX_FLAGS){
            $to_email= $item->user->email;
		
            $flags= $item->getFlags();
            
            $header_color= "#D50000";
            $header_text= "Heads up. Your item has been removed from Uncle Bucks.";
            
            try{ 
                Mail::send('emails.inlined.flagged_item', ['item'=>$item,  'header_text' => $header_text, 'header_color' => $header_color], function($message) use ($to_email)
                {
                     $message->to($to_email)->subject('Your Uncle Bucks Item Was Removed');
                });
            }
            catch(GuzzleHttp\Exception\ClientException $e){
                \Log::error('Exception while attempting to send inappropriate item email to user: '.$user->id);
            }
            
            
             
             $message= "";
             foreach($flags as $flag){
                 $message .= '<b>'.$flag->reason.':</b>&nbsp;'.$flag->explanation.'\r\n';
             }
             \Log::info('Sent email to '.$to_email.' that item has been flagged and removed.\r\n'.$message);
        }
		
        Session::flash('message', 'Thanks for letting us know. We appreciate your help in keeping Uncle Bucks clean and honest!');
        Session::flash('alert-class', 'bg-ub-inverse');
		return redirect(url(''));
        
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
