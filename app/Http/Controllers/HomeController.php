<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Monolog\Logger;
use Auth;
use DB;
use Cache;
use Session;
use Mail;

use App\Repositories\ItemRepositoryInterface;
use Illuminate\Http\JsonResponse;
use App\Item;


class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(ItemRepositoryInterface $itemRepository)
	{
		//$this->middleware('auth');
		
		$this->itemRepository= $itemRepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		
		$category_id= 0; //
		$keyword= "";
		$filter= "recent";
		
		
		//filter items based on user's POST
		if ($request->isMethod('post')) {
			
			$input= $request->all();
			
			$category_id= $request->input('category_id', 0);
			$keyword= $request->input('keyword', "");
			$filter= $request->input('filter', 'recent');
			
			\Log::info("item home: c:".$category_id.", k:".$keyword.", f:".$filter);
		}
		else if($request->isMethod('get')) {
			
			$input= $request->all();
				
			$category_id= $request->session()->get('category_id', 0);
			$keyword= $request->session()->get('keyword', "");
			$filter= $request->session()->get('filter', 'recent');
			
			$page= $request->input('page', 0);
			
			\Log::info("getting more items matching c:".$category_id.", k:".$keyword.", f:".$filter." for PAGE ".$page);
		}
		
		
		$request->session()->put('category_id', $category_id);
		$request->session()->put('keyword', $keyword);
		$request->session()->put('filter', $filter);
		
		
		
		
		switch( $filter ){
		
			case "user":
				
				$items= $this->itemRepository->getUserItems($category_id, $keyword, $filter, Auth::user() );
				break;
				
				
				
			case "favorite":
				
				
				
				$items= $this->itemRepository->getFavoriteItems($category_id, $keyword, Auth::user() );


				//\Log::info('favorite items in Controller : '.$items->count() );
				break;
			
				
				
				
			case "recent":
			
				$items= $this->itemRepository->getRecentItems($category_id, $keyword);
				/*
				$items = Cache::remember('items.c-'.$category_id.'.k-'.$keyword, 4, function() use ($category_id, $keyword) {
					\Log::info('query not cached - most recent with category: '.$category_id.' and keyword: "'.$keyword.'"');
					return \App\Item::withCategory($category_id)->withKeyword($keyword)->active()->latest('expires_at')->get();
				});
				
				*/
				//$items= \App\Item::withCategory($category_id)->withKeyword($keyword)->active()->latest('expires_at')->get();
				break;
				
				/*
			case "location":
						
					$items= $this->itemRepository->getNearestItems($category_id, $keyword);
					break;
				*/
				
			case "expired":
				
				$items= $this->itemRepository->getExpiredItems($category_id, $keyword);
				break;
				
				
			default:
				$items= $this->itemRepository->getRecentItems($category_id, $keyword);
				
				break;
				
		}
		
		$total_items=0;
		$limit= 0;
		
		if( $filter == "favorite" ){
			$limit=0;
			$total_items=0;
		}
		else if( !empty($items) ) {
			
			$limit= $this->itemRepository->limit();
			$total_items= $items->total();
		}
		
		
		return view('static.unclebucks', compact( 'category_id', 'filter', 'keyword', 'items', 'limit', 'total_items'));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function more(Request $request)
	{
	
		\Log::info('more items requested!');
		
		
		$category_id= 0; //
		$keyword= "";
		$filter= "recent";
		$page= 0;
	
	
		//filter items based on user's POST
		if ($request->isMethod('post')) {
				
			$input= $request->all();
				
			$category_id= $request->input('category_id', 0);
			$keyword= $request->input('keyword', "");
			$filter= $request->input('filter', 'recent');
			$page= $request->input('page', 0);
		}
	
		//\Log::info('filter after input: '.$filter.' & '.$request->input('filter','empty'));
		
		$request->session()->put('category_id', $category_id);
		$request->session()->put('keyword', $keyword);
		$request->session()->put('filter', $filter);
		$request->session()->put('page', $page);
	
		//\Log::info('filter after session: '.$filter);
	
		
		switch( $filter ){
	
			case "user":
				\Log::info('user items requested!');
	
				$items= $this->itemRepository->getUserItems($category_id, $keyword, $filter, Auth::user() );
				break;
	
	
	
			case "favorite":
				\Log::info('favorie items requested!');
	
				$items= $this->itemRepository->getFavoriteItems($category_id, $keyword, Auth::user() );
				break;
					
	
	
	
			case "recent":
				\Log::info('recent items requested!');
				
				$items= $this->itemRepository->getRecentItems($category_id, $keyword, 1);
				break;
	
	
			case "expired":
				\Log::info('expired items requested!');
				
				$items= $this->itemRepository->getExpiredItems($category_id, $keyword);
				break;
	
			default:
				\Log::info('default items requested!');
				
				$items= \App\Item::withCategory($category_id)->withKeyword($keyword)->active()->get();
				break;
	
		}
		
	
		//\Log::info('last query: '.print_r(DB::getQueryLog(), TRUE));
		
		$packed_items= collect([]);
		foreach ($items as $i){
			$packed_items->push( $i->presentGridElement($filter) );
		}
		
		\Log::info('packed json: '.$packed_items->toJson());
		
		return response()->json($packed_items);
		
		
		//return response()->json($items);
	}

	
	
	
	
	public function home()
	{
		$user= Auth::user();
		
		$subscription= null;
		$plan= null;
		if( $subscription= $user->getSubscription() ){
			$plan= $subscription->plan;
		}
		return view('static.user-dashboard', compact('user', 'subscription', 'plan'));
	}
	
	public function dashboard()
	{
		return view('static.admin-dashboard');
	}
	
	
	public function old_browser()
	{
		return view('static.old-browser');
	}
	
    
    public function resources($page)
	{
		return view('static.resources.'.$page);
	}
    
    
	public function about()
	{
		return view('static.about');
	}
	
	public function givesback()
	{
		return view('static.givesback');
	}
	
	public function business()
	{
		return view('static.business_partnerships');
	}
	
	public function help()
	{
		$user= Auth::user();
		return view('static.help', compact('user'));
	}
	
	public function send_message(Request $request)
	{
		
		$this->validate($request, [
			'email' => 'required|email',
			'msg' => 'required'
		]);
		
		$to_email= "info@unclebucks.co";
		
        $type= $request->input('message-type');
        switch ($type){
            case "help":
                $subject= "Uncle Bucks Help Request";
                $view= 'emails.slack';
                $redirect= "help";
                break;
            
            case "question":
                $subject= "Uncle Bucks Question";
                $view= 'emails.slack';
                $redirect= "resources/faq";
                break;
                
            default:
                $subject= "Uncle Bucks Help Request";
                $view= 'emails.slack';
                $redirect= "help";
                break;
        }
        
        
		//\Log::info('sending help message to '.$input['email'].' : '.$input['message']);
        try{
            Mail::send($view, ['message-type' => $type, 'subject' => $subject, 'msg' => $request->input('msg'), 'from_email' => $request->input('email')], function($message) use ($to_email, $subject)
            {
                 $message->to($to_email)->subject($subject);
            });
        }
        catch(GuzzleHttp\Exception\ClientException $e){
            \Log::error('Exception while attempting to '.$type.' email to: '.$user->id);
            
            return view('emails.error');
        }
		
		Session::flash('message', 'Thanks. Your message is on its way. We\'ll get back to ya real soon.');
		Session::flash('alert-class', 'bg-ub-inverse');
		
		return redirect(url($redirect));
	}
	
	
	public function email_template($template_name="welcome")
	{
		switch($template_name){
			case "welcome":
                $header_color= "#000000";
                $header_text= "Welcome to Uncle Bucks: Helping Neighbors Rent From Neighbors";
				return view('emails.inlined.welcome', compact('header_color', 'header_text'));
				break;
                
            case "password":
                $header_color= "#000000";
                $header_text= "Did you forget your password?";
                $token = "LKASD8FALDKG948DKZzkfdjkd2lfh";
				return view('emails.password', compact( 'token'));
				break;
				
			case "help":
				$from_email= "luke@unclebucks.co";
				$msg= "Hello, this is a test message.";
				return view('emails.help', compact('from_email', 'msg'));
				break;
                
            case "error":
				return view('emails.error');
				break;
                
            case "flagged":
				$from_email= "luke@unclebucks.co";
                $header_color= "#D50000";
                $header_text= "Heads up. Your item has been removed from Uncle Bucks.";
                
                $item = \App\Item::first();
				return view('emails.inlined.flagged_item', compact('header_color', 'header_text', 'item'));
				break;
			

            case "payment":
                //return view('emails.litmus');
                
                $item = \App\Item::first();
                $amount= 1;
				$to_email= "lukeswarner@gmail.com";
                
				$subject="Uncle Bucks Payment Confirmation";
                Mail::send('emails.inlined.payment-charge', ['item'=>$item, 'amount'=>$amount, 'confirmation_id'=>'CH_JFKDLIW123KJLA'], function($message) use ($to_email, $subject)
                {
                    $message->to($to_email)->subject($subject);
                });
                
                break;
                
                
            case "subscription":
                //return view('emails.litmus');
                
                $amount= 10;
				$to_email= "lsw@lukewarner.net";
                $subscription= (object)[];
                $subscription->current_period_end= \Carbon\Carbon::tomorrow();
                $subject="Uncle Bucks Subscription Confirmation";
                Mail::send('emails.inlined.payment-subscription', ['subscription' =>$subscription, 'plan'=>'Unlimitted Monthly', 'amount' => $amount, 'confirmation_id'=>'SUB_JKLFDLKWJR'], function($message) use ($to_email, $subject)
                {
                    $message->to($to_email)->subject($subject);
                });
                
                break;
				

	
			case "expiring":
				$item= \App\Item::first();
				return view('emails.expiring_item', compact('item'));
				break;
                
			case "expired":
				$expiring_items= collect([]);
				$expired_items= collect(\App\Item::find('5208'));
				return view('emails.inlined.items_expiration', compact('expired_items', 'expiring_items'));
				break;
				
			case "cron":
				\App\Item::process_items_nightly();
				//\App\Item::email_expired_items();
				break;	
				
			case "expires":
				$user= \App\User::first();
				$expiring_items= collect([]);
				$expiring_items->push( \App\Item::find('2'));
				

				$expired_items= collect([]);
                $expired_items->push( \App\Item::find('3'));
				
				$to_email= "lukeswarner-xg3i@litmustest.com";
                $to_email= "lukeswarner@gmail.com";
               
                
				//\Log::info('sending help message to '.$input['email'].' : '.$input['message']);
                
               //Prepare Email
               if($expired_items->count() == 1){
                    $header_text= "WELL SHOOT! One of your Uncle Bucks items has EXPIRED!";
                    $subject= 'Your Uncle Bucks Items Have Expired';
                }
                else if($expired_items->count() > 1){
                    $header_text= "WELL SHOOT! A few of your Uncle Bucks items have EXPIRED!";
                    $subject= 'Your Uncle Bucks Items Have Expired';
                }
                else if($expiring_items->count() == 1){
                    $header_text= "WELL SHOOT!  One of your Uncle Bucks items is EXPIRING SOON!";
                    $subject= 'Your Uncle Bucks Items Are Expiring Soon';
                }
                else if($expiring_items->count() > 1){
                    $header_text= "WELL SHOOT! A few of your Uncle Bucks items are EXPIRING SOON!";
                    $subject= 'Your Uncle Bucks Items Are Expiring Soon';
                }
               
                $header_color= "#000000";
                $email_view="emails.inlined.expiration_notification";
                
               
                Mail::send($email_view, ['expired_items'=>$expired_items, 'expiring_items'=>$expiring_items, 'header_text'=>$header_text, 'header_color'=>$header_color], function($message) use ($to_email, $subject)
                {
                    $message->to(explode(', ', $to_email))->subject($subject);
                });
                
                
				return view($email_view, compact('expired_items', 'expiring_items', 'header_text', 'header_color'));
				break;
				
			default:
				return view('emails.welcome');
				break;
				
		}
		
	}

}
