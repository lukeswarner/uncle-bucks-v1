<?php namespace App\Http\Controllers;


/**
 * @issues
 * UB-12 Category CRUD
 * 
 */


use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use App\Category;
use App\CategoryGroup;

use Cache;

class CategoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$category_groups= CategoryGroup::all();
		
		return view('category.list', compact('category_groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category_groups= \App\CategoryGroup::lists('name','id');  	//lists gets all() the category_groups and formats them as [ 'id' => 'name', ...] for dropdowns!
		$category_groups[" "]=" ";
		
		
		return view('category.create', compact('category_groups'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Requests\CategoryRequest $request
	 * @return Response
	 */
	public function store(Requests\CategoryRequest $request)
	{
		//validation is run first, before this method is called.
		$input= $request->all();

		Category::create($input);
		
		Cache::forget('categories.all');		//UB-140 Invalidate Cache when changes made
		
		return redirect(url('category'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
		//
		$category= Category::where('slug', $slug)->first();
		
		if(! $category){
			//abort(404);
			return redirect(url('category'));
		}
		
		return view('category.show', compact('category'));
		//return $category->toArray();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$category_groups= \App\CategoryGroup::lists('name','id');  	//lists gets all() the category_groups and formats them as [ 'id' => 'name', ...] for dropdowns!
		
		$category= Category::where('slug', $slug)->first();
		
		if(! $category){
			//abort(404);
			return redirect(url('category'));
		}
		
		return view('category.edit', compact('category', 'category_groups'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param  Requests\CategoryRequest $request
	 * @return Response
	 */
	public function update($id, Requests\CategoryRequest $request)		//use the ID to find the category, so we can update the slug
	{
		
		//
		$category= Category::find($id);
		
		$category->update($request->all());
		
		Cache::forget('categories.all');		//UB-140 Invalidate Cache when changes made
		
		return redirect(url('category'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Category::destroy($id);

		Cache::forget('categories.all');		//UB-140 Invalidate Cache when changes made
		
		return redirect(url('category'));
		
		
	}

}
