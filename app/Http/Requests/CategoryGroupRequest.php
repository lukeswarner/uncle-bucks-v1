<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoryGroupRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',	 
			'slug' => 'required|max:24|unique:category_groups,slug,'.$this->id,
		];
	}

}
