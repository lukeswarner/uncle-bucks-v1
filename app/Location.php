<?php namespace App;

/**
 * @issues
 * //UB-25 Item can have a Location [2015-07-02]
 */
use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;		// UB-114 Cache Search Results

class Location extends Model {

	use Rememberable;		// UB-114 Cache Search Results
	
	//
	protected $fillable= [
		'lat', 
		'lng', 
		'address'
	];

	
	/**
	 * This Polymorphically returns an instance of the object marked by this location- either an Item or a User
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\MorphTo
	 */
	public function locatable(){						//UB-25 Item can have a Location [2015-07-02]
		return $this->morphTo();
	}
	
	
	public function distanceBetween(\App\Location $loc2){
		
		$lat1= $this->lat;
		$lon1= $this->lng;
		
		$lat2= $loc2->lat;
		$lon2= $loc2->lng;
		
		
		$pi80 = M_PI / 180;
		$lat1 *= $pi80;
		$lon1 *= $pi80;
		$lat2 *= $pi80;
		$lon2 *= $pi80;
		
		$r= 3959.872;		// mean radius of Earth in m
		//$r = 6372.797; 	// mean radius of Earth in km
		$dlat = $lat2 - $lat1;
		$dlon = $lon2 - $lon1;
		$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$mi = $r * $c;
	
		//echo '<br/>'.$km;
		return $mi;
	
	}
	
	
}
