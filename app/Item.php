<?php namespace App;

/** 
 * @issues
 * UB-19 Item Model [2015-06-22] 
 * 
 * 
 * */

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Flag;
use Cache;
use Auth;
use Mail;

use Watson\Rememberable\Rememberable;		// UB-114 Cache Search Results

class Item extends Model {
	
	use Rememberable;						// UB-114 Cache Search Results

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [		//UB-19 Item model [2015-06-22]
			'title', 
			'rental_period',		//UB-134 Item can be rented hourly
			'price',
			'description', 
			'category_id', 
			'user_id', 
			'photo',
			'thumbnail',
			'photo_xs',
			'status',
			'flags',				//UB-150 Items can be flagged as inappropriate
			'expires_at',
			'auto_renew',
			'created_at',
			'updated_at',
	]; 
	
	/*
	 * add 'expires_at' to list of fields to be represented as Carbon dates
	 */
	protected $dates= ['expires_at'];
	
	
	
	/**
	 * return the rental information for an item, with price and rental_period
	 * This can includ ea link to the Item, and can force the rental_period to uppercase, 
	 * 		both of which are the default behaviors
	 * 
	 * @param string $link = TRUE
	 * @param string $uppercase = TRUE
	 * @return string
	 */
	public function displayRentalDetails($link= TRUE, $uppercase= TRUE){
		$details= '$'.$this->price.'/';
		
		$details.= ($uppercase) ? strtoupper($this->rental_period) : $this->rental_period;
		
		if($link){
			$details= '<a href="'.action('ItemController@show', $this->id).'">'.$details.'</a>';
		}
		
		return $details;
	}
	

	/**
	 * Return the formateted string displaying the Items Category
	 *
	 * @param string $tag
	 * @return string
	 */
	public function print_category($tag= "div"){
	
		if($this->category && $this->category_group()) {
			//return $this->category_group()->name."&nbsp;>&nbsp;".$this->category->name;
			return $this->category->name;
		}
		else {
			return "";
		}
	
	}
	
	
	/**
	 * determine if the User has access to edit this Item. The user
	 * 		must be either the Item's owner or an Admin
	 * @param \App\User $user
	 * @return boolean
	 */
	public function checkEditable(\App\User $user= null){
	
		if( empty($user) ) {
			return FALSE;
		}
			
		
		
		if ( $user->id == $this->user->id ){
			\Log::info('item route filter #'.$this->id.' - EDITABLE. User is the owner ');
			return TRUE;
		}
		else if ( $user->hasRole('admin')) {
			\Log::info('item route filter #'.$this->id.' - EDITABLE. User is ADMIN ');
			return TRUE;
		}
	
		return FALSE;
	
	}
	
	
	
	/**
	 * Set the photo atribute for an item- and delete the old photo (if neccessary) from the filesystem
	 * 
	 * @param string $value - URI of the item's new photo
	 */
	public function setPhotoAttribute($value){
		
		if( !empty( $this->attributes['photo'] ) ){
		
			\File::delete( public_path( $this->attributes['photo'] ) );
		
		}
		
		$this->attributes['photo']= $value;
		
	}
	
	
	public function getPhotoAttribute($value){
		
		if( $value ){
			
			return url($value);
		}
		
	
		return "";
	}
	
	
	
	/**
	 * Set the thumbail atribute for an item- and delete the old thumbnail (if neccessary) from the filesystem
	 * 
	 * @param string $value - URI of the item's new photo
	 */
	public function setThumbnailAttribute($value){
		
		if( !empty( $this->attributes['thumbnail'] ) ){
		
			\File::delete( public_path( $this->attributes['thumbnail'] ) );
		
		}
		
		$this->attributes['thumbnail']= $value;
		
	}
	
	
	public function getThumbnailAttribute($value){
		
		if( $value ){
			
			return url($value);
		}
		else if ($this->photo) {
			return $this->photo;
		}
		
	
		return "";
	}
	
	
	
	
	/**
	 * Set the thumbail atribute for an item- and delete the old thumbnail (if neccessary) from the filesystem
	 *
	 * @param string $value - URI of the item's new photo
	 */
	public function setPhotoXsAttribute($value){
	
		if( !empty( $this->attributes['photo_xs'] ) ){
	
			\File::delete( public_path( $this->attributes['photo_xs'] ) );
	
		}
	
		$this->attributes['photo_xs']= $value;
	
	}
	
	
	public function getPhotoXsAttribute($value){
	
		if( $value ){
				
			return url($value);
		}
		else if ($this->photo) {
			return $this->photo;
		}
	
	
		return "";
	}
	

	
	/**
	 * Allows $item->user() to return the User who posted the item
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){														//UB-19 Items Model	[2015.06.22]
		
		//return $this->belongsTo('App\User');
		//return Cache::remember( 'user'.$this->user_id, $this->cache_time, function()  {
			//\Log::info('caching query. c: '.$category_id.' k:'.$keyword);
		
			return $this->belongsTo('App\User')->remember(4);
		//});
		
	}

	
	
	/**
	 * Allows $item->category() to return the Category the Item belongsTo
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category(){													//UB-19 Items Model	[2015.06.22]
	
		return $this->belongsTo('App\Category')->remember(20);
	}
	
	
	/**
	 * Allows $item->category_group to return the Category_group the Item belongsTo
	 *
	 * @return \App\CategoryGroup
	 */
	public function category_group(){											//UB-3 User can create Item [2015.06.23]
	
		if($this->category) {
			return $this->category->category_group;
		}
		else {
			return \App\CategoryGroup::find(0);
		}
	}
	
	
	
	/**
	 * Allows $item->location to return the location of this Item
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function location(){													//UB-25 Item can have a Location [2015-07-02]
		return $this->morphOne('App\Location', 'locatable')->remember(60);
	}
	
	
	/**
	 * calculate an expiration date for an Item
	 * If the expiration date hasn't been set yet (for brand new iems)
	 *   or if the expiration date is in the past, it sets the new expiration
	 *   to 1 month from today.
	 * If the expiration date is in the future, it adds 1 month to the current 
	 *   expiration date
	 * 
	 * @return Carbon $expiration_date - the new expiration date for this Item
	 */		
	public function calculateExpiration(){								//UB-19 Items Model	[2015.06.22]
																		//UB-124 Users Can Publish Items	[2015.10.21]
		
		if( is_null( $this->expires_at ) || $this->is_expired()  ){
			return Carbon::today()->addMonths(1);
		}
		else {
			return $this->expires_at->addMonths(1);
		}
	}
	
	
	/**
	 * return a boolean on whether the item is past its expiration date
	 *     if an item expires TODAY, it is still valid
	 */
	public function is_expired(){
		
		if( is_null($this->expires_at) ){
			//\Log::info('no expiration date set, yet');
			return TRUE;
		}
		
		return $this->expires_at->lt( \Carbon\Carbon::today() );
		
	}
	
	
	/**
	 * return a boolean on whether the item is expiring within X days
	 *     default number of days is 5
	 */
	public function is_expiring($days= 5){
	
		if( is_null($this->expires_at) ){
			//\Log::info('no expiration date set, yet');
			return TRUE;
		}
	
		$cutoff_day= \Carbon\Carbon::today()->addDays($days);
		
		return $this->expires_at->lt( $cutoff_day );
	}
	
	
	/**
	 * return a boolean on whether the item will be renewed before the subscription is cancelled
	 */
	public function is_renewing_before_cancellation(){
	
		if( is_null($this->expires_at) ){
			//\Log::info('no expiration date set, yet');
			return FALSE;
		}
		
		if($this->is_expired()){
			return FALSE;
		}
	
		//the subscription must be LATER than the expiration date - if they are the same, item should expire
		if($this->user->cancelled()){
			return $this->user->subscription_ends_at->format('Ymd') > $this->expires_at->format('Ymd');
		}
		else{
			return FALSE;
		}
	
	}
	
	/**
	 * return a string describing the expiration date in human readable terms
	 * @return string
	 * 
	 * UB-122 Display Expiration Details for My Items 
	 */
	
	public function print_expiration(){
		
		if( $this->user->hasSubscription() && !$this->user->cancelled() ) {
			return 'AutoRenews '.$this->expires_at->toFormattedDateString();
		}
		elseif( $this->expires_at->gte(\Carbon\Carbon::today()) ) {
			return 'Expires '.$this->expires_at->toFormattedDateString();
		}
		else {
			return 'Expired '.\Carbon\Carbon::now()->diffForHumans( $this->expires_at, TRUE ).' ago!';
		}
	}
	
	
	
     /**
      * @brief a Collection of \App\Flags attached to this item
      * @param string $status : "active" : "inactive" : "all"
      * @return  
      */
	public function getFlags($status= "active"){													//UB-150 Items can be flagged inappropriate	[2016.02.29]

        switch($status){
            case "active":
                $active= 1;
                break;
                    
            case "inactive":
                $active= 0;
                break;
                    
            case "all":
                return Flag::where('item_id', '=', $this->id)->get();
                break;
                    
            default:
                $active= 1;
                break;
        }
        
		return Flag::where('item_id', '=', $this->id)->where('active', '=', $active)->get();
	}
	
	
	
	
	/**
	 * return a boolean on whether the item has been flagged MAX_FLAGS times
	 * @return bool
	 * UB-150 Item can be flagged as inappropriate
	 */
	public function is_flagged(){
	
		if( $this->flags >= Flag::MAX_FLAGS ){
			
			return TRUE;
		}
	
		return FALSE;
	
	}
	
	
	
	
	/**
	 * Scope a query to only items that have not reached the flagged limit
	 *
	 * 
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeNotFlagged($query)
	{
	
		return $query->where('flags', '<', Flag::MAX_FLAGS);
	}
	
	
	public function scopePopular($query)
	{
		return $query->where('flags', '>', 100);
	}
	
	
	/**
	 * Scope a query to only specified category
	 * 
	 * @param \App\Category | int  of category to filter on
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWithCategory($query, $category= null)
	{

		if( empty($category) ){
			return $query;
		}
		
		if( $category instanceof \App\Category ){
			$category_id= $category->id;
		}
		else {
			$category_id= $category;
			
		}
		
		return $query->where('category_id', '=', $category_id);
	}
	
	
	/**
	 * Scope a query to match a specified keyword
	 * 
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param string $keyword
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWithKeyword($query, $keyword= null){
		
		
		if( empty($keyword) ){
			return $query;
		}
		
		$keyword.= "*";
		return $query->whereRaw(
	        	"MATCH(title,description) AGAINST(? IN BOOLEAN MODE)", 
	        	array($keyword)
		);
	}
	
	
	/**
	 * Scope a query to only specified category
	 *
	 * @param \App\Category | int  of category to filter on
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWithFilter($query, $filter= "", $id= null)
	{
		//\Log::info('in Item->scopeWithFilter('.$filter.', '.$id.') ');
	
		switch( $filter ){
			case "user":
				if( empty($id)) {
					\Log::error('No user_id provided in Item->scopeWithFilter() to match users items.');
					return $query;
				}
				
				return $query->where('user_id', '=', $id);
				break;
				
			case "favorite":
				return $query->where('favorite_items.user_id', '=', $id)
							->join('favorite_items', 'items.id', '=', 'favorite_items.item_id');
				break;
						
			default:
				return $query;
				
		}
		
	}
	
	/**
	 * Scope a query to only active (not-expired) Items
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeActive($query){
		return $query->where('expires_at', '>=', \Carbon\Carbon::today());
	}
	
	
	/**
	 * Scope a query to only expired Items
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeExpired($query){
		return $query->where('expires_at', '<', \Carbon\Carbon::today());
	}
	
	
	
	/**
	 * Scope a query to only expired Items
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeExpiresAt($query, $expiration_date){
		return $query->where('expires_at', '=', $expiration_date);
	}
	
	
	public function presentGridElement($filter){
	
		\Log::info('packing item for preentation '.$this->id);
	
		$user= $this->user;
	
		ob_start();
		?><div class="grid-item">
					
				
			<div class="well well-masonry">
				
				<?php  
				if ( $this->thumbnail ){
				?>
				<div class="photo-container img">
					 
					<img src="<?= $this->thumbnail ?>" class="photo-masonry">
				
				
					<!--  UB-122 Display Expiration Details for My Items -->
					<?php if($filter == "user" && $this->auto_renew){ ?>
						<div class="expiration_overlay bg-success">
		      				<h4 class="expiration-hdr"> <?= $this->print_expiration() ?></h4>
				      	</div>
				      						 
				    <?php 
					}  else if($filter == "user" && $this->is_expired()) { ?>
				      	<div class="expiration_overlay bg-danger">
		      				<h4 class="expiration-hdr"><?= $this->print_expiration() ?></h4>
				      	</div>
				      						 
				    <?php 
					} else if ($filter == "user" && $this->is_expiring()) { ?>
					    <div class="expiration_overlay bg-warning">
			      			<h4 class="expiration-hdr"><?= $this->print_expiration() ?></h4>
					    </div>
				      						 
				    <?php 
					} else if ($filter == "user") { ?>														<!-- /* UB-132 My Item Design Changes [2015-11-09] */ -->
				    	<div class="expiration_overlay bg-white">
				      		<h4 class="expiration-hdr"><?= $this->print_expiration() ?></h4>
		      			</div>
		      		<?php 
					}
					?>
		
		
		
		
			      	<div class="overlay">
			      		<div class="overlay-top">
			      			<a class="close-overlay hidden pull-right">x</a>
			      			<h3 id="item-title<?= $this->id?>" class="item-title"><?= str_limit($this->title, 40) ?></h3>
			      		</div>
			      								 
			      		<div id="overlay-content<?= $this->id?>" class="overlay-content hidden-sm hidden-xs">
			      			<?= str_limit($this->description, 128) ?>
			      		</div>
		        
			      		<div id="overlay-contact<?= $this->id?>" class="overlay-contact" ></div>
			      								 
			      		<div class="overlay-bottom">
			      			<?php // overlay for owners
		      				if ( $filter == "user" ) {												// UB-132 My Item Design Changes [2015-11-09] ?>
		      								 
			      				<div class="contact-icon ">
			      					<a href="<?= url('item/'.$this->id.'/edit') ?>" class="glyphicon glyphicon-edit glyphicon-contact" aria-hidden="true"></a><br />
			      					Edit
			      				</div>
		
		      				<?php 
		      				}
		      				else { //overlay for everyone else ?>
		
			      				<div class="contact-icon">
			      					<a href="<?= url('item', $this->id) ?>" class="glyphicon glyphicon-th-list glyphicon-contact" aria-hidden="true"></a><br />
			      					Details
			      				</div>
		
		      				
		      				<?php 
		      					if( $user->contact_email) { ?>
		      						<div class="contact-icon ">
		      							<i onclick="showContact('<?= $this->id ?>', '<?= $user->split_email() ?>', 'email')" class="glyphicon glyphicon-envelope glyphicon-contact" aria-hidden="true"></i><br />
		      							Email
		      						</div> <?php 
								}
		      					else { ?>
		      						<div class="contact-icon contact-disabled">
		      							<i class="glyphicon glyphicon-envelope glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
		      							Email
		      						</div> <?php 
		      					}
		      					
		      					
		      					if( $user->contact_phone) { ?>
		      						<div class="contact-icon ">
		      							<i onclick="showContact('<?= $this->id ?>', '<?= $user->phone ?>', 'phone')" class="glyphicon glyphicon-earphone glyphicon-contact" aria-hidden="true"></i><br />
		      							Phone
		      						</div> <?php 
								}
		      					else { ?>
		      						<div class="contact-icon contact-disabled">
		      							<i class="glyphicon glyphicon-earphone glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
		      							Phone
		      						</div> <?php 
		      					}
		      					
		      					
		      					
		      					
		
		      					if( $user->contact_sms) { ?>
		      						<div class="contact-icon ">
		      							<i onclick="showContact('<?= $this->id ?>', '<?= $user->phone ?>', 'sms')" class="glyphicon glyphicon-comment glyphicon-contact" aria-hidden="true"></i><br />
		      							Text
		      						</div> <?php 
								}
		      					else { ?>
		      						<div class="contact-icon contact-disabled">
		      							<i class="glyphicon glyphicon-comment glyphicon-contact contact-disabled" aria-hidden="true"></i><br />
		      							Text
		      						</div> <?php 
		      					}
		      					
		      				} // end overlay bottom for user	
		      					
		      				?>
			          	</div> <!-- end overlay bottom -->
					</div><!-- end overlay -->	 
			    </div><!-- end photo container --> <?php 
			                					 
				} // end if thumbnail
		
																								/* UB-132 My Item Design Changes [2015-11-09] */
				if ( $filter != "user" && Auth::check() ){
					$session_user= Auth::user();
					if( $session_user->favorite_items->contains( $this ) ){
						$star_class= "glyphicon-star favorite";
					}
					else {
						//$star_class= "glyphicon-star-empty";									/** removed per UB-111 "Dull" non-favorited star **/
						$star_class= "glyphicon-star";
					}
					?>					
					<h3 class="itemfav"><i id="itemfav<?= $this->id ?>" class="glyphicon <?= $star_class ?>" onclick="toggleFavorite('<?= $this->id ?>', '<?= $session_user->id ?>' )"></i></h3>
					<?php 
				}	
				?>
					<h3>'<?= $this->displayRentalDetails() ?></h3>
			</div>
		</div><?php 	
								
		return $this->trim_all( ob_get_clean());						
			
	}
	
	private function trim_all( $str , $what = NULL , $with = ' ' )
	{
		if( $what === NULL )
		{
			//  Character      Decimal      Use
			//  "\0"            0           Null Character
			//  "\t"            9           Tab
			//  "\n"           10           New line
			//  "\x0B"         11           Vertical Tab
			//  "\r"           13           New Line in Mac
			//  " "            32           Space
			 
			$what   = "\\x00-\\x20";    //all white-spaces and control chars
		}
		 
		return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
	}

	
	
	

	
	/**
	 * Called via Laravel's Task Scheduling (in console/kernel.php)
	 * This process does several things:
	 * 1) Loop through all users and their items to find items that are expiring soon or expired today
	 * 2) If the user has an active subscription, renew the items which would have expired today.
	 * 3) If the subscription has been cancelled and is ending today, clear the subscription data
	 * 4) If the user does not have an active subscription, AND their contact preferences allow it,
	 * 		email them a notification with the items expiring today and soon.
	 * 
	 * This is set up to only send ONE email to a given user in a day. Rather than sending an email for each
	 * 		item, all items are packed together into a single email.
	 */
	public static function process_items_nightly(){
	
		$all_users= \App\User::all();
	
		$expired_date= \Carbon\Carbon::yesterday();
		$expiring_date= $expired_date->copy();
		$expiring_date->addDays(5);
	
	
		echo 'Running for Date: '.$expired_date->format('F d, Y h:i:s').'<br /<br /><br />';
	
		foreach ($all_users as $user){
			
				
			$allow_email = FALSE;
			$active_subscription= FALSE;
			$contact_allowed = $user->contact_notification;
			
			
			$message= "-------------------------------<br />".$user->email."<br />";
            
			//decide if the subscription is active 
			// the subscription has to be in the future - If it expires today, remove the subscription info.
			
			if(  $user->hasSubscription() && !$user->cancelled() ){		//subscription is active and not being cancelled
				$active_subscription= TRUE;
				$message.= "subscription: ACTIVE <br />";
			}
			else if(  $user->hasSubscription() && $user->cancelled() ){	//subscription is cancelled
				if( $user->subscription_ends_at->format('Ymd') ==  $expired_date->format('Ymd') ) {	//... and it ends TODAY, so end it
					$user->clearSubscription();
					$message.= "subscription: CLEARED<br />";
				}
				else { //the end dat is in the future
					$active_subscription= TRUE;
					$message.= "subscription: CANCELLED<br />";
				}
			}
			else {
				$message.= "subscription: none <br />";
			}
			
			
			
			//decide if we'll send an email notification
			if( $contact_allowed && $active_subscription== FALSE){
				$allow_email = TRUE;
				$message.= "contact: YES<br />";
			}
			else {
				$message.= "contact: NO<br />";
			}
			
			
			
			
			
			$expiring_items= collect([]);
			$expired_items= collect([]);
	
			foreach ($user->items as $item){
	
				if( empty($item->expires_at) ){
					$message.= 'empty expiration date item : ['.$item->id.'] '.$item->title.' <br />';
					continue;
				}
					
				else if( $expired_date->format('Ymd') == $item->expires_at->format('Ymd') ){
					//these are items which expire TODAY
					
					$message.= 'EXPIRED : ['.$item->id.'] '.$item->title.' - '.$item->expires_at->format('F d, Y').'<br />';
					
					
					if($active_subscription){	//the subscription is active, so renew the item!
						$old_exp= $item->expires_at->copy();
						$item->expires_at= $item->calculateExpiration();
						$item->save();
						$message.= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - New Expiration Date: '.$item->expires_at->format('F d, Y').'<br />';
					}
					
					
					$expired_items->push($item);
	
					
	
				}
					
				else if( $expiring_date->format('Ymd') == $item->expires_at->format('Ymd') ){
					
					$message.= 'expiring soon : ['.$item->id.'] '.$item->title.' - '.$item->expires_at->format('F d, Y').'<br />';

					$expiring_items->push($item);

				}
	
			}
	
            echo $message;
			
			//if the user has no expired and no expiring, don't email them
			if( $expired_items->count() == 0 && $expiring_items->count() == 0 ){
				//echo "      no items<br />";
				continue;
			}
	
		
			
			
			
			//if the user doesn't want to be contacted OR has an active subscription, don't email them
			if( $allow_email== FALSE ){
				

				echo $message."<br/><br />";				
				\Log::notice( preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, $message) );
				
				continue;
			}
			
			
			$message.= "+ email sent !<br />";
			echo $message."<br/><br />";
			\Log::notice( preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, $message) );
			
			
			//Prepare Email
            $header_color= "#000000";
            
			if( $expired_items->count() > 0 ){
				$subject= 'Your Uncle Bucks Items Have Expired';
                $email_view="emails.inlined.expiration_notification";
                
                $header_text= "WELL SHOOT! Your Uncle Bucks Items Have Expired!";

			}
	
			else {
				$subject= 'Your Uncle Bucks Items Are Expiring Soon';
                $email_view="emails.inlined.expiration_notification";
                
                $header_text= "WELL SHOOT! Your Uncle Bucks Items Are Expiring Soon!";
			}
	
			$to_email= $user->email;
	
			
			/*
			echo $user->email."<br />";
			echo "Expired: <br />";
			foreach ($expired_items as $i){
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;['.$i->id.'] '.$i->title.' - '.$i->expires_at->format('F d, Y').'<br />';
			}
				
			echo "expiring: <br />";
			foreach ($expiring_items as $i){
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;['.$i->id.'] '.$i->title.' - '.$i->expires_at->format('F d, Y').'<br />';
			}
			echo "<br /><br />";
			*/
			
			try{
                Mail::send($email_view, ['expired_items'=>$expired_items, 'expiring_items'=>$expiring_items, 'header_text' => $header_text, 'header_color' => $header_color], function($message) use ($to_email, $subject)
                {
                    $message->to($to_email)->subject($subject);
                });
            }
            catch(GuzzleHttp\Exception\ClientException $e){
                \Log::error('Exception while attempting to send expiration notification email to: '.$user->id);
            }
    			
		}
	
	
	}
}
