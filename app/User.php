<?php namespace App;

/**
 * @issues
 * UB-20 User Migration (sub-table of UB-1 User CRUD) [2015-06-13]
 * UB-11 User Roles [2015-06-18]
 * UB-19 Items Model	[2015.06.22]
 */

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Zizaco\Entrust\Traits\EntrustUserTrait; /* UB-11 User Roles [2015-06-18] */
use Watson\Rememberable\Rememberable;		// UB-114 Cache Search Results
use Laravel\Cashier\Billable;				/* UB-130 Users can Pay to Renew Items [2016-01-13] */
use Laravel\Cashier\Contracts\Billable as BillableContract;			/* UB-130 Users can Pay to Renew Items [2016-01-13] */	

use Carbon;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract, BillableContract {

	use Authenticatable, CanResetPassword;
	use EntrustUserTrait; 	/* UB-11 User Roles [2015-06-18] */
	use Rememberable;		// UB-114 Cache Search Results
	use Billable;			/* UB-130 Users can Pay to Renew Items [2016-01-13] */


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [	//'name', 
							//'username', 			//UB-139 Remove username
							'email', 				
							'password', 
							'phone', 				//UB-20 User Migration: Add username, phone fields to User
							'contact_email', 
							'contact_phone', 
							'contact_sms', 
							'contact_notification'
						]; 

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	
	
	/**
	 * The date attributes added for Cashier 		UB-130 Users can Pay to Renew Items [2016-01-13]
	 * @var unknown
	 */
	protected $dates = ['trial_ends_at', 'subscription_ends_at'];
	
	
	
	/**
	 * a User has many Items matching the $filter param
	 * 
	 * @param  string $filter = "active" | "expired" | "expiring" | "auto_renew"
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items($filter= ""){												//UB-132 Home Dashboard	[2015.06.22]
		$items= $this->hasMany('App\Item');
		
		switch ($filter){
			case "active":
				
				return $this->items->filter( function($item){
					return $item->is_expired() == FALSE;
				});
				break;
				
				
			case "auto_renew":
				return $this->items->filter( function($item){
					return $item->auto_renew == TRUE;
				});
				break;
				
				
			case "expired":
				return $this->items->filter( function($item){
					return $item->is_expired() == TRUE;
				});
				break;
				
			case "expiring":
				return $this->items("active")->filter( function($item){
					return $item->expires_at->lt( \Carbon\Carbon::today()->addDays(5) );
				});
				break;
				
			case "renews_before_cancellation":
				return $this->items->filter( function($item){
					return $item->is_renewing_before_cancellation() == TRUE;
				});
				break;
			
			default:
				return $items;
		}
		
		
	}
	
	
	
	/**
	 * a User has many favorite items (and items can be favorited by many users)
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function favorite_items(){
		return $this->belongsToMany('\App\Item', 'favorite_items');
	}
	
	/**
	 * Allows $user->location to return the location of this User
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function location(){													//UB-25 Item can have a Location [2015-07-02]
		return $this->morphOne('App\Location', 'locatable');
	}
	
	public function split_email(){
		$pieces= preg_split('/@/', $this->email);
		return $pieces[0].' @'.$pieces[1];
	}

	
	/**	
	 * Conect this user to Stripe by making them a customer id, stored as stripe_id				
	 */
	public function createStripeCustomer(){													//UB-130 User Can Pay... [2016.01.20]
		//pass the user object to the gateway; it must implement BillableContract
		$gateway = new \Laravel\Cashier\StripeGateway($this);
		
		
		$customer_data= ['email'=>$this->email, 
						 'metadata'=>[
						 		'user_id'=> $this->id,
						 		'phone'=> $this->phone,
						 		'region'=>'Bend, OR'
						 ]						
		];
		//manually create a new Customer instance with Stripe
		//$customer = $gateway->createStripeCustomer($request->get('stripe_token'));
		
		//manually create a new Customer instance with Stripe WITHOUT a credit card token
		$customer = $gateway->createStripeCustomer(null, $customer_data);
		
		//update the model's info
		$gateway->updateLocalStripeData($customer);
		
		return TRUE;
	}
	
	
	/**
	 * return the Stripe Customer object, if the user is a Stripe Customer
	 */
	public function customer(){																//UB-130 User Can Pay... [2016.01.20]
		//pass the user object to the gateway; it must implement BillableContract
		$gateway = new \Laravel\Cashier\StripeGateway($this);
		
		if( $this->hasStripeId() ){
			//get the Stripe Customer
			$customer = $gateway->getStripeCustomer($this->getStripeId());
			
			return $customer;
		}
		
		return FALSE;
	}
	
	
	public function getSubscription(){
		
		if($this->hasSubscription()){
			$sub= $this->customer()->subscriptions->retrieve( $this->getStripeSubscription() );
			
			//the date the subscription started on
			$sub->start= Carbon\Carbon::createFromTimestampUTC($sub->start);

			//the date the current subscription period ends - aka, the date the subscription is renewed and card is charged again
			$sub->current_period_end= Carbon\Carbon::createFromTimestampUTC($sub->current_period_end);
			
			//the date the current period started
			$sub->current_period_start= Carbon\Carbon::createFromTimestampUTC($sub->current_period_start);
			
			//the date the subscription was cancelled on - this will set $user->subscription_ends_at field to Subscription->current_period_end
			if( !empty($sub->canceled_at) ){
				$sub->canceled_at= Carbon\Carbon::createFromTimestampUTC($sub->canceled_at);
			}
			
			
			return $sub;
		}
		else {
			return FALSE;
		}
	}
	
	public function hasSubscription(){														//UB-130 User Can Pay... [2016.01.20]
		
		if ( empty( $this->stripe_subscription) ){
			return FALSE;
		}
		return TRUE;
	}
	
	public function clearSubscription(){
		$this->stripe_subscription = null;
		$this->stripe_plan= null;
		$this->subscription_ends_at= null;
		$this->stripe_active== 0;
		$this->save();
	}
	
	/**
	 * get the user's Stripe subscription plan, if tehy have a subscription
	 * 
	 * 		- amount (integer in cents)
	 * 		- name	(string name of the plan)
	 * 		- interval	(year or month)
	 * 		- created (timestamp)
	 * 
	 * @return Stripe Plan object
	 */
	public function getPlan(){
		if ( empty( $this->stripe_subscription) ){
			return FALSE;
		}
		
		$sub= $this->getSubscription();
		
		if( empty($sub) ){
			return FALSE;
		}
		
		$plan= $sub->plan;
		return $plan;
		
	}
	
	
	public function savedCard(){
		
		
		if($this->save_card == FALSE || ! $customer= $this->customer() ){
			return FALSE;
		}
			
		
		$card= $customer->sources->retrieve( $customer->default_source );
		
		switch($card->brand){
			case "Visa":
				$card->brand_logo= '<img src="https://www.merchantequip.com/image/?logos=vd&height=64" alt="Merchant Equipment Store Credit Card Logos" />';
				break;
				
			case "MasterCard":
				$card->brand_logo= '<img src="https://www.merchantequip.com/image/?logos=m&height=64" alt="Merchant Equipment Store Credit Card Logos" />';
				break;
				
			case "American Express":
				$card->brand_logo= '<img src="https://www.merchantequip.com/image/?logos=a&height=64" alt="Merchant Equipment Store Credit Card Logos" />';
				break;
				
			case "Discover":
				$card->brand_logo= '<img src="https://www.merchantequip.com/image/?logos=d&height=64" alt="Merchant Equipment Store Credit Card Logos" />';
				break;
				
		}
		return $card;
	}
}
