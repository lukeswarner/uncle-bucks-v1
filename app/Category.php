<?php namespace App;

/**
 *
 * @author Luke
 * @issues
 * UB-16 Categories Model
 * UB-19 Items Model	[2015.06.22]
 *
 */

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;		// UB-114 Cache Search Results
use Cache;


/*
 * model: Category
 * table: categories
 */
class Category extends Model {

	use Rememberable;		// UB-114 Cache Search Results
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'slug', 'description', 'category_group_id']; //UB-12 Category CRUD - 2015.06.17
	
	
	public static function all($columns= []){
		return Cache::remember( 'categories.all', 20, function(){	
			return parent::all();	
		});
	}
	
	
	
	/**
	 * a Category belongs to a CategoryGroup
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category_group(){						//UB-16 - a Category belongsTo one CategoryGroup
		
		return $this->belongsTo('App\CategoryGroup')->remember(20);
	}

	
	/**
	 * there are many Items which belong to a Category
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items(){								//UB-19 Items Model	[2015.06.22]
		return $this->hasMany('App\Category');
	}
	
	
}
