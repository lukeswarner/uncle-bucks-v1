<?php namespace App\Repositories;

use App\Models\Users;
use \Cache;

class UserEloquentRepository implements UserRepositoryInterface{
	
	private $cache_time= 60;
	
	public function getUser($id){
		\Debugbar::startMeasure('user.id','Measure UserRepository->getUser('.$id.')');
		
		return Cache::remember( $this->generateKey('user', $id ), $this->cache_time, function() use ($id) {
			//\Log::info('caching query. c: '.$category_id.' k:'.$keyword);
		
			return $this->belongsTo('App\User')->first();
		});
		
		\Debugbar::stopMeasure('user.id');
	}
	
	private function generateKey($q="", $c="", $id=""){
	
		return md5("$q.id$id");
	}
}