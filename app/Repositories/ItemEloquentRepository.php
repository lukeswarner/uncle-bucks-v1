<?php namespace  App\Repositories;

use App\Models\Items;
use \Cache;
use \DB;
use \Collection;

class ItemEloquentRepository implements ItemRepositoryInterface
{
	private $cache_time= 4;
	private $limit= 75;
	
	
	
	public function limit(){
		return $this->limit;
	}
	
	/**
	 * return an Eloquent Collection of recent Items matching the specified criteria
	 * 
	 * @param unknown $category_id
	 * @param unknown $keyword
	 */
	public function getRecentItems($category_id, $keyword, $page=0){
		\Log::info('get Recent Items for c: '.$category_id.' k:'.$keyword.' p:'.$page);
		
		//\Debugbar::startMeasure('items.recent','Measure ItemRepository->getRecentItems()');
		
		//return Cache::remember( $this->generateKey('items.recent', $category_id, $keyword), $this->cache_time, 	function() use ($category_id, $keyword) {
				//
				
			//'owner' => function ($q) {$q->remember(10);}
			return $items= \App\Item::with( ['user' => function($q){$q->remember( $this->cache_time);}] )
				//return $items= \App\Item::with('user')		
					->withCategory($category_id)
					->withKeyword($keyword)
					->active()
					->notFlagged()
					->latest('expires_at')
					//->skip($page * $this->limit)
					//->take($this->limit)
					->paginate($this->limit);		
		//});

		//\Debugbar::stopMeasure('items.recent');
		
	}
	
	
	
	
	
	public function getNearestItems($category_id, $keyword, $page=0){
		
		$bend_lat= 44.0581753;
		$bend_lng= -121.3166054;
		
		//$locations= DB::table('locations')->whereRaw("`id`, ( 3959 * acos( cos( radians(44.0581753) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-121.3166054) ) + sin( radians(44.0581753) ) * sin( radians( lat ) ) ) ) AS distance from locations HAVING distance < 50 ORDER BY distance")->get();
		//DB::select('select * from users where id = ?', [1]);
		$items= DB::select("SELECT id, ( 3959 * acos( cos( radians(44.0581753) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-121.3166054) ) + sin( radians(44.0581753) ) * sin( radians( lat ) ) ) ) AS distance FROM locations HAVING distance < 25 ORDER BY distance");
		
		
		dd($locations);
		
		//"SELECT id, ( 3959 * acos( cos( radians(44.0581753) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-121.3166054) ) + sin( radians(44.0581753) ) * sin( radians( lat ) ) ) ) AS distance FROM locations HAVING distance < 25 ORDER BY distance"";
	}
	
	
	
	/**
	 * return an Eloquent Collection of items belonging to the specified user
	 * @param id $category_id
	 * @param string $keyword
	 * @param string $filter
	 * @param App\User $user
	 */
	public function getUserItems($category_id, $keyword, $filter, $user){
		
		if( empty( $user ) ) {
			//abort(404);
			return collect([]);
		}
			
		
		//return Cache::remember( $this->generateKey('items.user', $category_id, $keyword, $user->id), $this->cache_time, 	function() use ($category_id, $keyword, $filter, $user) {
			
			return $items= $user->items()->with('user')
				->withCategory($category_id)
				->withKeyword($keyword)
				->withFilter($filter, $user->id)
				->orderBy('expires_at', 'desc')
				
				->paginate($this->limit);	
		//});
		
		
	}
	
	/**
	 * refresh User Items cache by first forgeting the items saved in the Cache, then eager loading the query
	 * @param id $category_id
	 * @param string $keyword
	 * @param App\User $user
	 */
	
	/*public function refreshUserItems($category_id, $keyword, $filter, $user){
	
		if( empty( $user ) ) {
			return FALSE;
		}
	
		\Log::info('refreshing User Items for user: '.$user->id.' by category: '.$category_id.' and keyword "'.$keyword.'"');
	
	
		$key= $this->generateKey('items.user', $category_id, $keyword, $user->id);
	
		if( Cache::has( $key ) ){
	
			Cache::forget( $key );
	
			return $this->getUserItems($category_id, $keyword, $filter, $user);
		}
	
		return FALSE;
	}
	*/
	
	
	
	
	
	
	
	
	/**
	 * return an Eloquent Collection of items favorited by the specified user
	 * @param id $category_id
	 * @param string $keyword
	 * @param App\User $user
	 * */
	public function getFavoriteItems($category_id, $keyword, $user){
		
		if( empty( $user ) ) {
			abort(404);
		}
		
		
		
			
		//\Debugbar::startMeasure('items.favorite','Measure ItemRepository->getFavoriteItems()');
		//$items= Cache::remember( $this->generateKey('items.favorite', "", "", $user->id), $this->cache_time, 	function() use ($category_id, $keyword, $user) {
				
		//		\Log::info('caching favorite items for user: '.$user->id);
		
		
				$items= $user->favorite_items;	

				
				/** TRY THIS ONE: 
				$items= DB::select("select `items`.*, `favorite_items`.`user_id` as `pivot_user_id`, `favorite_items`.`item_id` as `pivot_item_id` from `items` inner join `favorite_items` on `items`.`id` = `favorite_items`.`item_id` where `favorite_items`.`user_id` = '?'", [2]);
				*/
		//});
		
		/*
		select `items`.*,
		`favorite_items`.`user_id` as `pivot_user_id`, `favorite_items`.`item_id`
		as `pivot_item_id` from `items` inner join `favorite_items`
		on `items`.`id` = `favorite_items`.`item_id`
		where `favorite_items`.`user_id` = '2' from `items` order by `expires_at` desc limit 30 offset 0)
	
		
		select `items`.*, 
		`favorite_items`.`user_id` as `pivot_user_id`, `favorite_items`.`item_id` 
		as `pivot_item_id` from `items` inner join `favorite_items` 
		on `items`.`id` = `favorite_items`.`item_id` 
		where `favorite_items`.`user_id` = '2'


		
		$items= \App\Item::select(
				DB::raw("`items`.*,
                        `favorite_items`.`user_id` as `pivot_user_id`, `favorite_items`.`item_id`
						as `pivot_item_id` from `items` inner join `favorite_items`
						on `items`.`id` = `favorite_items`.`item_id`
						where `favorite_items`.`user_id` = '?'"))
								->setBindings([$user->id])
								->orderBy('expires_at', 'desc')
								->paginate($this->limit);
		
		\Log::info('filtering '.$items->count().' favorite items by category '.$category_id);
			*/
		
		//$items= DB::statement("select `items`.*, `favorite_items`.`user_id` as `pivot_user_id`, `favorite_items`.`item_id` as `pivot_item_id` from `items` inner join `favorite_items` on `items`.`id` = `favorite_items`.`item_id` where `favorite_items`.`user_id` = '?'", [2]);
		//\Log::info('filtering '.$items->count().' favorite items by category '.$category_id);
		
		
		//filter out items that aren't in the requested category
		if($category_id != 0){
			$items= $items->filter(function ($item) use ($category_id) {
				return $category_id == $item->category->id;
			});
		}
		
		//\Log::info('filtering '.$items->count().' favorite items by keyword "'.$keyword.'"');
		
		//filter out items that don't contain the keyword
		if( !empty($keyword) ){
			$items= $items->filter(function ($item) use ($keyword) {
				return str_contains($item->title, $keyword) || str_contains($item->description, $keyword);
			});
		}
		
		
		//filter out items that have been flagged as inapropriate
		$items= $items->filter(function ($item) {
			return ($item->is_flagged() == FALSE);
		});
		
		
		//\Debugbar::stopMeasure('items.favorite');
		
		//\Log::info('favorite items left: '.$items->count() );
		
		
		
		/*
	
		 // Define pagination defaults if not given
		$limit = $this->limit;
		$page = \Input::get('page', 1);
		
		// Force page to 1, if something funny is given as parameter
		if ($page > count($items) || $page < 1) {
			$page = 1;
		}
		
		// Calculate offset and slice array
		$offset = ($page * $limit) - $limit;
		$items_slice = array_slice($items->all(), $offset, $limit, $page);
		
		// Create paginator manually
		
		// (mixed $items, int $total, int $perPage, int|null $currentPage = null, array $options = array())
		$paginator = new \Illuminate\Pagination\LengthAwarePaginator($items_slice, count($items), $limit);
	
		
		return $paginator;
		*/
		return $items;
	}
	
	/**
	 * refresh Favorite Items cache by first forgeting the items saved in the Cache, then eager loading the query
	 * @param id $category_id
	 * @param string $keyword
	 * @param App\User $user
	 */
	public function refreshFavoriteItems($category_id, $keyword, $user){
		
		if( empty( $user ) ) {
			return FALSE;
		}
		
		\Log::info('refreshing Favorite Items for user: '.$user->id.' by category: '.$category_id.' and keyword "'.$keyword.'"');
		
		
		$key= $this->generateKey('items.favorite', "", "", $user->id);
	
		if( Cache::has( $key ) ){
				
			Cache::forget( $key );
				
			return $this->getFavoriteItems($category_id, $keyword, $user);
		}
	
		return FALSE;
	}
	
	
	
	
	
	
	
	
	public function getExpiredItems($category_id, $keyword){
		
		return Cache::remember( $this->generateKey('items.expired', $category_id, $keyword), $this->cache_time, 	function() use ($category_id, $keyword) {
			return App\Item::withCategory($category_id)
					->withKeyword($keyword)
					->expired()
					->get();
		});
	}
	
	/**
	 * refresh teh Expired Items cache by first forgeting the items saved in the Cache, then eager loading the query 
	 * @param id $category_id
	 * @param string $keyword
	 */
	public function refreshExpiredItems($category_id, $keyword){
		$key= $this->generateKey('items.expired', $category_id, $keyword);
		
		if( Cache::has( $key ) ){
			
			Cache::forget( $key );
			
			return $this->getExpiredItems($category_id, $keyword);
		}
		
		return FALSE;
	}
	
	public function forget($filter, $category_id, $keyword, $user){
		
		Cache::forget( $this->generateKey('items.'.$filter, $category_id, $keyword, $user->id) );
	}
	
	
	private function generateKey($q="", $c="", $k="", $u=""){
		
		return md5("$q.c$c.k$k.u$u");
	}
}

