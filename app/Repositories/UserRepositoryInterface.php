<?php namespace App\Repositories;


interface UserRepositoryInterface{
	
	public function getUser($id);
	
}