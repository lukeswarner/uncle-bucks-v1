<?php namespace App\Repositories;

interface ItemRepositoryInterface {
	
	public function getRecentItems($category_id, $keyword);
	
	public function getUserItems($category_id, $keyword, $filter, $user);
	
	public function getFavoriteItems($category_id, $keyword, $user);
	
	public function refreshFavoriteItems($category_id, $keyword, $user);
	
	public function getExpiredItems($category_id, $keyword);
	
	public function refreshExpiredItems($category_id, $keyword);
	
	public function forget($filter, $category_id, $keyword, $user);
	
}
